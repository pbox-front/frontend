const proxy = require("http-proxy-middleware");

module.exports = function (app) {
    app.use(
        proxy("/www/api/keywordapi", {
            target: "http://dev.protectbox.com",
            secure: false,
            changeOrigin: true
        })
    );

    app.use(
        proxy("/www/api/blogapi", {
            target: "http://dev.protectbox.com",
            secure: false,
            changeOrigin: true
        })
    );

    app.use(
      proxy("/www/api/blogapi/blogdetail", {
        target: "http://dev.protectbox.com",
        secure: false,
        changeOrigin: true
      })
    );

    app.use(
        proxy("/www/api/blogapi/newsdetail", {
          target: "http://dev.protectbox.com",
          secure: false,
          changeOrigin: true
        })
      );

    app.use(
        proxy("/www/api/loginapi", {
            target: "http://dev.protectbox.com",
            secure: false,
            changeOrigin: true
        })
    );

    app.use(
        proxy("/www/api/registerapi", {
            target: "http://dev.protectbox.com",
            secure: false,
            changeOrigin: true
        })
    );

    app.use(
        proxy("/www/api/forgotpassapi", {
            target: "http://dev.protectbox.com",
            secure: false,
            changeOrigin: true
        })
    );

    app.use(
        proxy("/www/api/faqapi/getheadings", {
          target: "http://dev.protectbox.com",
          secure: false,
          changeOrigin: true
        })

      );
};
