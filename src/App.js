import React from "react";
import { BrowserRouter as Router, Switch, Route, Redirect } from "react-router-dom";
import Login from "./routes/outerRoutes/Login";
import Register from "./routes/outerRoutes/Register";
import ForgotPassword from "./routes/outerRoutes/ForgotPassword";
import Blog from "./routes/outerRoutes/Blog";
import ContactUS from "./routes/outerRoutes/ContactUs";
import EditSolution from "./routes/outerRoutes/EditSolution";
import Sales from "./routes/outerRoutes/Sales";
import Coupons from "./routes/innerRoutes/Coupons";
import Questionaire from "./routes/innerRoutes/Questionaire";
import BeDeligate from "./routes/innerRoutes/BeDeligate";
import ManageDelegates from "./routes/innerRoutes/ManageDelegates";
import Payment from "./routes/innerRoutes/Payment";
import Profile from "./routes/innerRoutes/Profile";
import About from "./routes/innerRoutes/About";
import Home from "./routes/innerRoutes/Home";
import Smp_subscription from "./routes/innerRoutes/Smp_Subscription";
import Faqs from "./routes/outerRoutes/Faq";
import Terms from "./routes/outerRoutes/Terms";
import SupplierPolicy from "./routes/outerRoutes/SupplierPolicy";
import Privacy from "./routes/outerRoutes/Privacy";
import Invoice from "./routes/innerRoutes/Invoice";
import Supplier_partner from "./routes/outerRoutes/Supplier_Partner";
import BlogDetail from "./routes/outerRoutes/BlogDetail";
import BlogNewsDetail from "./routes/outerRoutes/BlogNewsDetail";

const App = () => {
  return (
    <div className="App">
      <Router>
        <Switch>
          <Route exact path="/" component={Home} />
          <Route exact path="/Home" component={Home} />
          <Route exact path="/Login" component={Login} />
          <Route exact path="/Registration" component={Register} />
          <Route exact path="/forgetpass" component={ForgotPassword} />
          <Route exact path="/Blog" component={Blog} />
          <Route exact path="/Blog/Detail/:title" component={BlogDetail} />
          <Route exact path="/News/Detail/:title" component={BlogNewsDetail} />
          <Route exact path="/ContactUs" component={ContactUS} />
          <Route exact path="/edit_solution" component={EditSolution} />
          <Route exact path="/Sales" component={Sales} />
          <Route exact path="/Coupons" component={Coupons} />
          <Route exact path="/Questionaire" component={Questionaire} />
          <Route exact path="/BeDeligate" component={BeDeligate} />
          <Route exact path="/Payment" component={Payment} />
          <Route exact path="/Profile" component={Profile} />
          <Route exact path="/About" component={About} />
          <Route exact path="/ManageDelegates" component={ManageDelegates} />
          <Route exact path="/smb_subscription" component={Smp_subscription} />
          <Route exact path="/FAQs" component={Faqs} />
          <Route exact path="/Termsandconditions" component={Terms} />
          <Route exact path="/SupplierPolicy" component={SupplierPolicy} />
          <Route exact path="/Privacypolicy" component={Privacy} />
          <Route exact path="/Invoice" component={Invoice} />
          <Route exact path="/SupplierAndPartners" component={Supplier_partner} />
        </Switch>
      </Router>
    </div>
  );
};

export default App;
