import React, { useEffect } from "react";
import Header from "./../../components/Header";
import Footer from "./../../components/Footer";

const ContactUs = () => {
  useEffect(() => {
    document.title = "ProtectBox | Contact Us"
  }, [])

  return (
    <div>
      <div id="load" />

      <section className="pb-section slider-main">
        <Header />
        <div className="banner-section-main contact-block">
          <div className="black-overlay">
            <div className="banner-text">
              <h1 className="pbh-2 white-color">Contact Us</h1>
            </div>
          </div>
        </div>
      </section>
      <main className="contactus">
        <div className="container margin_60">
          <div className="row">
            <h1 style={{ marginBottom: 20 }} className="col-md-12 text-center">
              Get In Touch
            </h1>
            <div
              className="col-md-7 col-md-offset-3 rounded_div"
              style={{
                padding: "10px 25px 10px",
                border: "1px solid #CCC",
                boxShadow: "0px 0px 3px #bfbfbf",
                borderRadius: 5,
                background: "#f5f5f5",
                margin: "auto",
              }}
            >
              <form name="contactzzzz" method="POST">
                <div className="row">
                  <div className="col-md-6 col-sm-6 px-1">
                    <div className="form-group">
                      <label>First name</label>
                      <input
                        type="text"
                        className="form-control"
                        name="first_name"
                      />
                    </div>
                  </div>
                  <div className="col-md-6 col-sm-6 px-1">
                    <div className="form-group">
                      <label>Last name</label>
                      <input
                        type="text"
                        className="form-control"
                        name="last_name"
                      />
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-md-6 col-sm-6 px-1">
                    <div className="form-group">
                      <label>Email</label>
                      <input
                        type="email"
                        name="email_id"
                        className="form-control"
                      />
                    </div>
                  </div>
                  <div className="col-md-6 col-sm-6 px-1">
                    <div className="form-group">
                      <label>Phone number</label>
                      <input
                        type="number"
                        name="phone_number"
                        className="form-control"
                      />
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-md-12 px-1">
                    <div className="form-group">
                      <label>Your message</label>
                      <textarea
                        rows={5}
                        name="message_info"
                        className="form-control"
                        style={{ height: 100 }}
                        defaultValue={""}
                      />
                    </div>
                  </div>
                </div>
                {/* <p>&nbsp;</p>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-6" style="margin-top:-30px;">
                                    <img src="<?php echo base_url();?>Contact/captcha/" style="height:50px;width:auto;text-align:center;"/><br/>
                                    <label>Enter the text in the box</label>
                                </div>
                                <div class="col-md-6">
                                     <div class="form-group">
                                        <input type="text" name="captcha" class="form-control" >
                                     </div>
                               </div>
                            </div>
                        </div>
                     </div>
                 </div>
            </div> */}
                <div className="row">
                  <div className="col-md-12">
                    <div className="form-group text-center">
                      <button
                        type="submit"
                        className="btn btn-success btn-lg col-md-offset-5 btn-contact-submit"
                      >
                        {" "}
                        Submit{" "}
                      </button>
                    </div>
                  </div>
                </div>
              </form>
            </div>
            {/* End col-md-9 */}
          </div>
          {/* End row */}
        </div>
        {/* End container */}
      </main>
      <Footer />
    </div>
  );
};

export default ContactUs;
