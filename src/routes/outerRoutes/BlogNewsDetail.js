import React, { Component } from "react";
import Header from "./../../components/Header";
import Footer from "./../../components/Footer";
import Moment from "moment";
import Axios from "axios";
import { Link } from "react-router-dom";


class BlogNewsDetail extends Component {
  
  constructor(props) {
    super(props);

    this.state = {
      blogData: "",
      suggestList: []
    }
  }

  componentDidMount() {
    const title = this.props.match.params.title;
    document.title = title;

    this._getSuggestBlogList(title);
  }

  _getSuggestBlogList(blog_slug) {
    const url = "/www/api/blogapi/newsdetail";
    let formData = new FormData();
    const slug = blog_slug;

    formData.append("slug", slug);

    Axios.post(url, formData, {
      headers: {
        "Content-Type": `multi-part/form-data`,
        "Access-Control-Allow-Origin": "*"
      },
    }).then((res) => {
      if (res.data.message == "Success") {
        console.log(res.data);
        this.setState({blogData: res.data.data});
        this.setState({suggestList: res.data.data_suggested});
      }
    });
  }

  render() {
    const blogData = this.state.blogData;
    const suggestList = this.state.suggestList;
    const imageUrl = "https://protectbox.com/"+blogData.post_image;

    return (
      <>
        <div id="load" />
        <section className="pb-section slider-main">
          <Header />
          <div id="blog_section1">
            <div className="container-fluid blog-detail-banner" style={{ backgroundImage: `url(${imageUrl})`}}>

            </div>
          </div>
        </section>

        <article className="pt-4 pb-4">
          <div className="container">
            <div className="row">
              <div className="col-10 offset-1">
                <div className="row">
                  <div className="col-9">
                    <div className="media">
                      <div className="media-body pt-2">
                        <h6 className="mt-0 mb-0">ProtectBox</h6>
                        <span className="ceotext">
                          {Moment(`${blogData.year}-${blogData.month}-${blogData.day}`).format("MMM Do YYYY")}
                        </span>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="row pt-4">
                  <div className="col-12 blog-detail-section" style={{wordWrap: "break-word"}}>
                    <div dangerouslySetInnerHTML={{__html: blogData.post_body}} />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </article>

        <section id="posts" className="pt-5 pb-5">
        <div className="container">
            <div className="row">
              <div className="col-10 offset-1">
                <div className="row">
                  <div className="col-12 text-left">
                    <h2 className="pbh-2" style={{textAlign: "left"}}>Suggested reading</h2>
                  </div>
                </div>
                <div className="row blog-suggested-list">
                  {suggestList.map(blog => (
                    <div className="mt-3 blog-suggested-item">
                      <div className="card">
                        <img className="card-img-top" style={{maxHeight: "130px"}} src={"https://protectbox.com/" + blog.post_image} />
                        <div className="card-body text-center" style={{padding: "15px", minHeight: "80px"}}>
                          <Link to={{pathname: "/Blog/Detail/"+blog.post_slug}} target="_blank" style={{fontWeight: "bold", color: "#000"}} >{blog.post_title}</Link>
                        </div>
                      </div>
                    </div>
                  ))}
                </div>
              </div>
            </div>
          </div>
        </section>

        {/*footer section*/}
        <Footer/>
      </>
    )
  }
}

export default BlogNewsDetail;
