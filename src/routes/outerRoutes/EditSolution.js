import React, { Component } from "react";
import Header2 from "./../../components/Header2";
import Footer2 from "./../../components/Footer2";
import { Tabs, Tab } from "react-bootstrap";
import { FaInfoCircle } from "react-icons/fa";
import { FaUsers } from "react-icons/fa";
import { AiOutlineUnorderedList, AiFillLinkedin } from "react-icons/ai";

export default class EditSolution extends Component {
  render() {
    const stepIconStyle = {
      color: '#fff',
      position: 'absolute',
      marginLeft: -50,
      marginTop: 5
    }
    return (
      <div>
        <div id="load" />
        
        <section className="pb-section slider-main">
        <Header2 />
      </section>
        <section
          id="sub_header"
          style={{
            background: "#f5f5f5",
            boxShadow: "inset 0 1px 10px 1px rgba(0,0,0,.41)",
          }}
        >
          <div className="container">
            <div
              className="main_title"
              style={{
                background: "none",
                textAlign: "center",
                fontSize: 40,
                color: "#000",
                bottom: 30,
              }}
            >
              Let's add your protection..
            </div>
          </div>
        </section>
        <main>
          <div className="container margin_60">
            <div className="row ">
              <div className="col-md-12">
                <div className="tab-content edit-solution">
                  <div className="tab-pane active" id="home">
                    <div className="account_info">
                      <div className="row">
                        <div className="col-md-12">
                          <p style={{ fontSize: 14, fontWeight: "normal" }}>
                            <div class="form-check">
                              <input
                                class="form-check-input"
                                type="radio"
                                name="exampleRadios"
                                id="exampleRadios2"
                                value="option2"
                              />
                              <label
                                class="form-check-label"
                                for="exampleRadios2"
                              >
                                Click "Account" button in top right corner to
                                change your details (Settings), preferred
                                payment (Payments), to re-access this page
                                (Solutions) or monthly subscriptions for
                                analysis on all your sales (Sales).{" "}
                              </label>
                            </div>
                          </p>
                        </div>
                        <div className="col-md-12">
                          <p style={{ fontSize: 14, fontWeight: "normal" }}>
                            <div class="form-check">
                              <input
                                class="form-check-input"
                                type="radio"
                                name="exampleRadios"
                                id="exampleRadios2"
                                value="option2"
                              />
                              <label
                                class="form-check-label"
                                for="exampleRadios2"
                              >
                                We send you email alerts of each sale as part of
                                our affiliate / re-seller fee that you select in
                                "Services" and that you only pay when sale made,
                                which we take automatically.
                              </label>
                            </div>
                          </p>
                        </div>
                      </div>
                    </div>
                    <div style={{}} className="another">
                      <div className="row">
                        <div className="col-md-12">
                          <p
                            style={{
                              color: "#ec8b0d",
                              fontSize: 14,
                              fontWeight: "normal",
                            }}
                          >
                            <div class="form-check">
                              <input
                                class="form-check-input"
                                type="radio"
                                name="exampleRadios"
                                id="exampleRadios2"
                                value="option2"
                              />
                              <label
                                class="form-check-label"
                                for="exampleRadios2"
                              >
                                All questions with an asterisk{" "}
                                <span
                                  style={{
                                    color: "#ec8b0d",
                                    fontSize: 22,
                                    marginTop: 10,
                                  }}
                                >
                                  *
                                </span>{" "}
                                must be answered.
                              </label>
                            </div>
                          </p>
                        </div>
                        <div
                          className="col-md-12"
                          style={{ padding: 0, margin: 0 }}
                        >
                          <div className="col-md-4">
                            <p
                              style={{
                                color: "#83C72A",
                                fontSize: 14,
                                fontWeight: "normal",
                              }}
                            >
                              <div class="form-check">
                                <input
                                  class="form-check-input"
                                  type="radio"
                                  name="exampleRadios"
                                  id="exampleRadios2"
                                  value="option2"
                                />
                                <label
                                  style={{ cursor: "pointer" }}
                                  class="form-check-label"
                                  for="exampleRadios2"
                                >
                                  Click on
                                  <a
                                    className="info-circle"
                                    style={{ display: "inline" }}
                                  >
                                    <span
                                      style={{
                                        color: "#83C72A",
                                      }}
                                    >
                                      {" "}
                                      <FaInfoCircle />
                                    </span>{" "}
                                    for more info.
                                  </a>{" "}
                                </label>
                              </div>
                            </p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <ul className="nav " />
                <div className="tab-content  edit-solution ">
                  <div className="tab-pane active" id="home">
                    <div className="form_title">
                      <h3>
                        <strong>
                          <i className="icon-users" />
                        </strong>
                        <span style={stepIconStyle}>
                          <FaUsers />
                        </span>{" "}
                        Overview{" "}
                        <span
                          style={{
                            color: "#83C72A",
                          }}
                        >
                          {" "}
                          <FaInfoCircle />
                        </span>
                      </h3>
                    </div>
                    <div className="step">
                      <form
                        name="supplier_basics_information"
                        action="<?php echo base_url();?>edit_solution/basic_info"
                        encType="multipart/form-data"
                        method="POST"
                      >
                        <div className="row">
                          <div className="col-lg-6 col-md-6 col-sm-12">
                            <div className="form-group">
                              <label>
                                Supplier name?&nbsp;
                                <span
                                  style={{ color: "#ec8b0d", fontSize: 22 }}
                                >
                                  *
                                </span>
                              </label>
                              <input
                                type="text"
                                className="form-control"
                                name="supplier_name"
                                defaultValue=""
                              />
                            </div>
                          </div>
                          <div className="col-lg-6 col-md-6 col-sm-12">
                            <div className="form-group">
                              <label>
                                What price will your solution be?&nbsp;
                                <span
                                  style={{ color: "#ec8b0d", fontSize: 22 }}
                                >
                                  *
                                </span>
                              </label>{" "}
                              <a
                                data-container="body"
                                title="This can be single number or range. You will add exact prices for each product/service in 'Services' below. This is to give ProtectBox an approximation to help match you better.  <br><br>
<i>If you have more questions, please ask in 'Search' in top right hand corner of website or see our FAQs page. Or then ask our team at supplier@protectbox.com or as shown in the footer via chat/telephone. Supplier@protectbox.com is also checked outside of the working hours for chat/telephone enquiries.</i> "
                                href="javascript:void(0);"
                                className="tooltiplink"
                                data-toggle="tooltip"
                                style={{
                                  display: "inline",
                                  margin: "0",
                                  padding: "0",
                                }}
                                data-placement="right"
                                data-html="true"
                              >
                                <span
                                  style={{
                                    color: "#83C72A",
                                  }}
                                >
                                  {" "}
                                  <FaInfoCircle />
                                </span>
                              </a>
                              <input
                                type="text"
                                className="form-control"
                                name="price_solution"
                                defaultValue=""
                              />
                            </div>
                          </div>
                        </div>
                        <div className="row">
                          <div className="col-lg-6 col-md-6 col-sm-12">
                            <div className="form-group">
                              <label>
                                Which of these categories does your solution
                                fall into ? &nbsp;
                                <span
                                  style={{ color: "#ec8b0d", fontSize: 22 }}
                                >
                                  *
                                </span>
                              </label>{" "}
                              <a
                                data-container="body"
                                style={{
                                  display: "inline",
                                  margin: "0",
                                  padding: "0",
                                }}
                                title="All the products that you select in 'Product Category' in 'Services' need to be selected here. In 'Services' you show one by one, in this section you are showing them all together. Categories are based on how the business customer would think of your product/service not how you would define it. This helps ProtectBox help you find the best way to sell your product/service.  <br><br>
<i>If you have more questions, please ask in 'Search' in top right hand corner of website. Or then ask our team at supplier@protectbox.com or as shown in the footer via chat/telephone. Supplier@protectbox.com is also checked outside of the working hours for chat/telephone enquiries"
                                className="tooltiplink</i> "
                                href="javascript:void(0);"
                                data-toggle="tooltip"
                                data-placement="right"
                                data-html="true"
                              >
                                <span
                                  style={{
                                    color: "#83C72A",
                                  }}
                                >
                                  {" "}
                                  <FaInfoCircle />
                                </span>
                              </a>
                              <select
                                className="form-control"
                                name="solution_category[]"
                                multiple="multiple"
                                style={{ height: 140 }}
                              >
                                <option></option>
                              </select>
                            </div>
                          </div>
                          <div className="col-lg-6 col-md-6 col-sm-12">
                            <div className="form-group">
                              <label>
                                Which suppliers do you NOT want to be bundled
                                with?
                              </label>{" "}
                              <a
                                data-container="body"
                                style={{
                                  display: "inline",
                                  margin: "0",
                                  padding: "0",
                                }}
                                title="All names need to be separated by commas. <br><br>
<i>If you have more questions, please ask in 'Search' in top right hand corner of website. Or then ask our team at supplier@protectbox.com or as shown in the footer via chat/telephone. Supplier@protectbox.com is also checked outside of the working hours for chat/telephone enquiries"
                                className="tooltiplink</i> "
                                href="javascript:void(0);"
                                data-toggle="tooltip"
                                data-placement="right"
                                data-html="true"
                              >
                                <span
                                  style={{
                                    color: "#83C72A",
                                  }}
                                >
                                  {" "}
                                  <FaInfoCircle />
                                </span>
                              </a>
                              <input
                                type="text"
                                className="form-control"
                                name="supplier_bundle_with"
                                defaultValue=""
                              />
                            </div>
                            <div className="form-group">
                              <label>Receive email alert on sale</label>{" "}
                              <a
                                data-container="body"
                                style={{
                                  display: "inline",
                                  margin: "0",
                                  padding: "0",
                                }}
                                title="if you select 'Yes' below, we will by default, alert you by email each time we sell one of your products/services. If you select 'No', we will not save any of your Sales details. You can instead sign up to our Subscription service, by clicking on link at end of this page, or on 'Sales' in 'Accounts' in top right hand corner. Our subscription saves all of your Sales details, gives Sales analytics & market news. With lots more features to come, including global Supply maps.<br><br><i>If you have more questions, please ask in 'Search' in top right hand corner of website or see our FAQs page. Or then ask our team at supplier@protectbox.com or as shown in the footer via chat/ telephone. Supplier@protectbox.com is also checked outside of the working hours for chat/telephone enquiries.</i>"
                                className="tooltiplink</i> "
                                href="javascript:void(0);"
                                data-toggle="tooltip"
                                data-placement="right"
                                data-html="true"
                              >
                                <span
                                  style={{
                                    color: "#83C72A",
                                  }}
                                >
                                  {" "}
                                  <FaInfoCircle />
                                </span>
                              </a>
                              <select
                                className="form-control"
                                name="email_receive"
                              ></select>
                            </div>
                          </div>
                        </div>
                        <div className=" text-right" style={{ marginTop: 50 }}>
                          <button
                            type="submit"
                            className="btn btn-success btn-medium btn-login-submit"
                          >
                            Save Data
                          </button>
                        </div>
                      </form>
                    </div>
                    <div className="form_title">
                      <h3>
                        <strong>
                          <i className="icon-users" />
                        </strong>
                        <span style={stepIconStyle}>
                          <AiOutlineUnorderedList />
                        </span>{" "}
                        Servies{" "}
                        <span
                          style={{
                            color: "#83C72A",
                          }}
                        >
                          {" "}
                          <FaInfoCircle />
                        </span>
                      </h3>
                    </div>
                    <div className=" step">
                      <Tabs
                        defaultActiveKey="add"
                        id="uncontrolled-tab-example"
                      >
                        <Tab eventKey="view" title="View Services">
                          <div>
                            <div className="row">
                              <div className=" account_info col-md-12">
                                <div className="col-md-12">
                                  <p
                                    style={{
                                      fontSize: 14,
                                      fontWeight: "normal",
                                    }}
                                  >
                                    {" "}
                                    "In the below table &amp; in the Excel
                                    download, you can see if we are already
                                    selling any of your services through a
                                    reseller/distributor. If you do not want us
                                    to sell your services through the
                                    reseller/distributor shown, please change
                                    the ‘Service Status’ to ‘Inactive’ in the
                                    Excel download. You can only add
                                    products/services that you will sell
                                    Directly through ProtectBox on this page. We
                                    encourage you to add your services as
                                    individual services &amp; as bundled
                                    services, to see what sells best. Also, to
                                    add the same service across more than one
                                    'Product Category', as customers may not
                                    define your product/service in the same way
                                    that you do. You can also add your services
                                    in 'Add Services' by selecting dropdown
                                    answers or typing in text, then pressing
                                    'Save' for each service.
                                    <br />
                                    <br />
                                    <i>
                                      If you have more questions, please ask in
                                      "Search" in top right hand corner of
                                      website or see our FAQs page. Or then ask
                                      our team at supplier@protectbox.com or as
                                      shown in the footer via chat/telephone.
                                      Supplier@protectbox.com is also checked
                                      outside of the working hours for
                                      chat/telephone enquiries."
                                    </i>
                                  </p>
                                </div>
                              </div>
                            </div>
                            <div className="row " style={{ paddingBottom: 20 }}>
                              <a href="" download></a>
                              <div className=" text-center col-md-12">
                                <a href="" download>
                                  <div className="col-md-8 col-md-offset-2"
                                  style={{ marginTop: 10, marginLeft:'16.6%' }}>
                                    <button
                                      type="button"
                                      className="btn badge-solution"
                                      style={{
                                        width: "100%",
                                        background: "#cccccc",
                                        border: "1px solid #cccccc",
                                        borderRadius: "5px",
                                      }}
                                    >
                                      <span
                                        style={{
                                          fontSize: 10,
                                          fontWeight: "bold",
                                          color: "#333",
                                        }}
                                      >
                                        Download excel template spreadsheet for
                                        addition of new services.
                                      </span>
                                    </button>
                                  </div>
                                </a>
                                <a href="<?php echo base_url('edit_solution/down_service');?>">
                                  <div
                                  className="col-md-8 col-md-offset-2"
                                  style={{ marginTop: 10, marginLeft:'16.6%' }}
                                  >
                                    <button
                                      type="button"
                                      className="btn badge-solution"
                                      style={{
                                        width: "100%",
                                        background: "#cccccc",
                                        border: "1px solid #cccccc",
                                        borderRadius: "5px",
                                      }}
                                    >
                                      <span
                                        style={{
                                          fontSize: 10,
                                          fontWeight: "bold",
                                          color: "#333",
                                        }}
                                      >
                                        Download excel spreadsheet of current
                                        services.
                                      </span>
                                    </button>
                                  </div>
                                </a>
                                <div
                                  className="col-md-8 col-md-offset-2"
                                  style={{ marginTop: 10, marginLeft:'16.6%' }}
                                >
                                  <button
                                    type="button"
                                    className="btn badge-solution"
                                    data-toggle="modal"
                                    data-target="#uploadModal"
                                    style={{
                                      width: "100%",
                                      background: "#cccccc",
                                      border: "1px solid #cccccc",
                                      borderRadius: "5px",
                                    }}
                                  >
                                    <span
                                      style={{
                                        fontSize: 10,
                                        fontWeight: "bold",
                                        color: "#333",
                                      }}
                                    >
                                      Upload completed excel spreadsheet for
                                      addition of new services.
                                    </span>
                                  </button>
                                </div>
                              </div>
                            </div>

                            <div className="row">
                              <div className=" account_info col-md-12">
                                <div className="col-md-12">
                                  <p
                                    style={{
                                      fontSize: 14,
                                      fontWeight: "normal",
                                    }}
                                  >
                                    <i className="icon-circle-empty"> </i> We
                                    are already selling your newly added
                                    services, through the <b>"Distributor"</b>{" "}
                                  </p>
                                </div>
                                <div className="col-md-12">
                                  <p
                                    style={{
                                      fontSize: 14,
                                      fontWeight: "normal",
                                    }}
                                  >
                                    <i className="icon-circle-empty"> </i>If you
                                    would prefer us to sell your service
                                    directly through protectbox, please change
                                    btn to inactive and add your service again
                                    but changing distributor to direct..
                                  </p>
                                </div>
                                <div className="col-md-12">
                                  <p
                                    style={{
                                      fontSize: 14,
                                      fontWeight: "normal",
                                    }}
                                  >
                                    <i className="icon-circle-empty"> </i>You
                                    can add a new service by either clicking the
                                    buttons below or adding the data into each
                                    drop down cells.
                                  </p>
                                </div>
                              </div>
                            </div>

                            <div className="row">
                              <div
                                className="table-responsive rounded_div col-md-12 "
                                style={{ marginBottom: 20 }}
                              >
                                <div
                                  className=" table-responsive"
                                  style={{ marginTop: 15 }}
                                >
                                  <table
                                    id
                                    className="table table-striped table-bordered example"
                                  >
                                    <thead>
                                      <tr>
                                        <th width="10%">Service&nbsp;No.</th>
                                        <th>Service&nbsp;Provider</th>
                                        <th width="30%">Logo</th>
                                        <th width="15%">Service&nbsp;Name</th>
                                        <th width="15%">Customer&nbsp;Type</th>
                                        <th width="15%">
                                          Product&nbsp;Category
                                        </th>
                                        <th width="15%">Product&nbsp;Detail</th>
                                        <th width="15%">Currency</th>
                                        <th width="15%">Price&nbsp;Range</th>
                                        <th width="15%">Price&nbsp;Detail</th>
                                        <th width="15%">
                                          Operating&nbsp;System
                                        </th>
                                        <th width="15%">
                                          Specialist&nbsp;Hardware
                                        </th>
                                        <th width="15%">
                                          Third&nbsp;Party&nbsp;Supplier
                                        </th>
                                        <th width="15%">
                                          Ease&nbsp;of&nbsp;setup
                                        </th>
                                        <th width="15%">
                                          Protection&nbsp;level
                                        </th>
                                        <th width="15%">Product&nbsp;link</th>
                                        <th width="15%">
                                          Commission&nbsp;detail
                                        </th>
                                        <th width="15%">Payment&nbsp;option</th>
                                        <th width="15%">
                                          Government&nbsp;voucher
                                        </th>
                                        <th width="15%">Cashback</th>
                                        <th width="15%">Rating</th>
                                        <th width="15%">Location</th>
                                        <th width="15%">Regulation</th>
                                        <th width="15%">
                                          User&nbsp;instruction
                                        </th>
                                        <th>Status</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      <tr>
                                        <td></td>
                                        <td></td>
                                        <td>
                                          <img src="" style={{ height: 15 }} />
                                        </td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td>
                                          <a href=""></a>
                                        </td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td>
                                          <form
                                            id="myForm"
                                            action="#"
                                            method="post"
                                          >
                                            <div className="form-group options">
                                              <label className="switch-light switch-ios">
                                                <input
                                                  type="checkbox"
                                                  name="status"
                                                  id="service_option"
                                                  onchange=""
                                                />
                                                <span>
                                                  <span>Inactive</span>
                                                  <span>Active</span>
                                                </span>
                                                <a />
                                              </label>
                                            </div>
                                            <input
                                              type="hidden"
                                              id="serv_tgle_<?php echo $service_infom->supplier_service_id;?>"
                                              name="status"
                                              defaultValue="<?php echo $service_infom->status;?>"
                                            />
                                            <input
                                              type="hidden"
                                              name="matching_supplier_service_id"
                                              defaultValue="<?php echo $service_infom->supplier_service_id;?>"
                                            />
                                          </form>
                                        </td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </div>
                              </div>
                            </div>
                          </div>
                        </Tab>
                        <Tab eventKey="add" title="Add Services">
                          <div>
                            <div className=" account_info col-md-12">
                              <div className="col-md-12">
                                <p
                                  style={{
                                    fontSize: 14,
                                    fontWeight: "normal",
                                  }}
                                >
                                  {" "}
                                  For each service, select dropdown answers or
                                  type in text, then press 'Save'. We encourage
                                  you to add your services as individual
                                  services &amp; as bundled services, to see
                                  what sells best. Also, to add the same service
                                  across more than one 'Product Category', as
                                  customers may not define your product/service
                                  in the same way that you do. You can only add
                                  products/services that you will sell Directly
                                  through ProtectBox on this page. In 'View
                                  Services' you can see if we are already
                                  selling any of your services through a
                                  reseller/distributor. You can also add your
                                  services in 'View Services' by downloading an
                                  Excel template, adding it as a new row &amp;
                                  uploading the Excel spreadsheet.
                                  <br />
                                  <br />
                                  <i>
                                    If you have more questions, please ask in
                                    "Search" in top right hand corner of website
                                    or see our FAQs page. Or then ask our team
                                    at supplier@protectbox.com or as shown in
                                    the footer via chat/telephone.
                                    Supplier@protectbox.com is also checked
                                    outside of the working hours for
                                    chat/telephone enquiries.
                                  </i>
                                </p>
                              </div>
                            </div>
                            <form
                              name="supplier_information"
                              action="<?php echo base_url();?>edit_solution/add_supplier"
                              encType="multipart/form-data"
                              method="POST"
                            >
                              <div className="bbcc">
                                <div className="new_div">
                                  <div className="row">
                                    <div
                                      className="col-lg-3 col-md-6 col-sm-12"
                                      style={{ display: "none" }}
                                    >
                                      <div className="form-group">
                                        <input
                                          type="text"
                                          className="form-control keyzz_value"
                                          defaultValue="<?php echo $key;?>"
                                        />
                                        <input
                                          type="text"
                                          className="form-control keyzz"
                                          defaultValue="<?php echo $array_length;?>"
                                        />
                                      </div>
                                    </div>
                                    <div className="col-lg-3 col-md-6 col-sm-12">
                                      <div className="form-group">
                                        <label>
                                          Logo&nbsp;
                                          <span
                                            style={{
                                              color: "#ec8b0d",
                                              fontSize: 22,
                                            }}
                                          >
                                            *
                                          </span>
                                        </label>
                                        <input
                                          type="file"
                                          className="form-control"
                                          name="userFiles"
                                        />
                                      </div>
                                    </div>
                                    <div className="col-lg-3 col-md-6 col-sm-12">
                                      <div className="form-group">
                                        <label>
                                          Supplier Name&nbsp;
                                          <span
                                            style={{
                                              color: "#ec8b0d",
                                              fontSize: 22,
                                            }}
                                          >
                                            *
                                          </span>
                                        </label>
                                        <input
                                          type="text"
                                          className="form-control"
                                          name="new_supplier_name"
                                        />
                                      </div>
                                    </div>
                                    <div className="col-lg-3 col-md-6 col-sm-12">
                                      <div className="form-group">
                                        <label>
                                          Service Name&nbsp;
                                          <span
                                            style={{
                                              color: "#ec8b0d",
                                              fontSize: 22,
                                            }}
                                          >
                                            *
                                          </span>
                                        </label>
                                        <input
                                          type="text"
                                          className="form-control"
                                          name="new_service_name"
                                        />
                                      </div>
                                    </div>
                                    <div className="col-lg-3 col-md-6 col-sm-12">
                                      <div className="form-group">
                                        <label>
                                          Customer Type&nbsp;
                                          <span
                                            style={{
                                              color: "#ec8b0d",
                                              fontSize: 22,
                                            }}
                                          >
                                            *
                                          </span>
                                        </label>
                                        <a
                                          data-container="body"
                                          title=" For 'Enterprise', we are interested in finding out more about your offerings for Corporates, Governments & Infrastructure. Similarly, for 'Consumer' we are interested in your Consumer products. We are building iterations for these customers with several high-profile, established Partners. We can already link to your webpages, systems etc through apis to show these products/services. As well as new features including pay-as-you-use pricing, customised/bespoke pricing etc are coming soon. Please email supplier@protectbox.com to discuss how ProtectBox can best help you sell these offerings.   <br><br>
<i>If you have more questions, please ask in 'Search' in top right hand corner of website. Or then ask our team at supplier@protectbox.com or as shown in the footer via chat/telephone. Supplier@protectbox.com is also checked outside of the working hours for chat/telephone enquiries"
                                          className="tooltiplink</i>     

"
                                          style={{
                                            display: "inline",
                                            margin: "0",
                                            padding: "0",
                                          }}
                                          data-toggle="tooltip"
                                          data-placement="right"
                                          data-html="true"
                                        >
                                          <span style={{ color: "#83C72A" }}>
                                            <FaInfoCircle />
                                          </span>
                                        </a>
                                        <select
                                          className="form-control"
                                          name="customer_type"
                                        >
                                          <option selected disabled value>
                                            Please select
                                          </option>
                                        </select>
                                      </div>
                                    </div>
                                  </div>
                                  <div className="row">
                                    <div className="col-lg-3 col-md-6 col-sm-12">
                                      <div className="form-group">
                                        <label>
                                          Product Category&nbsp;
                                          <span
                                            style={{
                                              color: "#ec8b0d",
                                              fontSize: 22,
                                            }}
                                          >
                                            *
                                          </span>
                                        </label>
                                        <a
                                          data-container="body"
                                          style={{
                                            display: "inline",
                                            margin: "0",
                                            padding: "0",
                                          }}
                                          title="Here you are showing the category relevant to each of your services, one by one. In ‘Overview’ you showed them all together. Categories are based on how the business customer would think of your product/service not how you would define it. To help ProtectBox help you find the best way to sell your product/service, we encourage you to add your services as individual services & as bundled services. Also, to add the same service across more than one 'Product Category', as customers may not define your product/service in the same way that you do. Some of the categories, need you to select extra sub-categories in “Product Detail”. You need to select “Product Category” first, to auto-populate options in “Product Detail <br><br><i>If you have more questions, please ask in 'Search' in top right hand corner of website or see our FAQs page. Or then ask our team at supplier@protectbox.com or as shown in the footer via chat/ telephone. Supplier@protectbox.com is also checked outside of the working hours for chat/telephone enquiries.</i>"
                                          className="tooltiplink</i> "
                                          href="javascript:void(0);"
                                          data-toggle="tooltip"
                                          data-placement="right"
                                          data-html="true"
                                        >
                                          <span style={{ color: "#83C72A" }}>
                                            <FaInfoCircle />
                                          </span>
                                        </a>
                                        <select
                                          className="form-control"
                                          name="product_category"
                                          onchange="product_categoryzz(this,<?php echo $key;?>)"
                                        ></select>
                                      </div>
                                    </div>
                                    <div className="col-lg-3 col-md-6 col-sm-12">
                                      <div className="form-group">
                                        <label>
                                          Product Detail&nbsp;
                                          <span
                                            style={{
                                              color: "#ec8b0d",
                                              fontSize: 22,
                                            }}
                                          >
                                            *
                                          </span>
                                        </label>
                                        <a
                                          data-container="body"
                                          style={{
                                            display: "inline",
                                            margin: "0",
                                            padding: "0",
                                          }}
                                          title="You need to select “Product Category” first, to auto-populate options in “Product Detail” which show you sub-categories. Categories are based on how the business customer would think of your product/service not how you would define it. To help ProtectBox help you find the best way to sell your product/service, we encourage you to add your services as individual services & as bundled services. Also, to add the same service across more than one 'Product Category' & ‘Product Detail’, as customers may not define your product/service in the same way that you do. <br><br><i>If you have more questions, please ask in 'Search' in top right hand corner of website or see our FAQs page. Or then ask our team at supplier@protectbox.com or as shown in the footer via chat/ telephone. Supplier@protectbox.com is also checked outside of the working hours for chat/telephone enquiries.</i>"
                                          className="tooltiplink</i> "
                                          href="javascript:void(0);"
                                          data-toggle="tooltip"
                                          data-placement="right"
                                          data-html="true"
                                        >
                                          <span style={{ color: "#83C72A" }}>
                                            <FaInfoCircle />
                                          </span>
                                        </a>
                                        <select
                                          className="form-control felbovalue_<?php echo $key;?>"
                                          name="product_detailzz"
                                          id="product_detailzz"
                                        >
                                          <option value="No category to choose">
                                            No category to choose
                                          </option>
                                        </select>
                                      </div>
                                    </div>
                                    <div className="col-lg-3 col-md-6 col-sm-12">
                                      <div className="form-group">
                                        <label>
                                          Price Currency&nbsp;
                                          <span
                                            style={{
                                              color: "#ec8b0d",
                                              fontSize: 22,
                                            }}
                                          >
                                            *
                                          </span>
                                        </label>
                                        <select
                                          className="selectpicker form-control"
                                          data-live-search="true"
                                          name="price_currency"
                                          data-dropup-auto="false"
                                        ></select>
                                      </div>
                                    </div>
                                    <div className="col-lg-3 col-md-6 col-sm-12">
                                      <div className="form-group">
                                        <label>
                                          Price Range&nbsp;
                                          <span
                                            style={{
                                              color: "#ec8b0d",
                                              fontSize: 22,
                                            }}
                                          >
                                            *
                                          </span>
                                        </label>
                                        <select
                                          className="form-control"
                                          name="price_range"
                                        >
                                          <option selected disabled value>
                                            Please select
                                          </option>
                                          <option value="0-500">0 - 500</option>
                                          <option value="500-1000">
                                            500 - 1,000
                                          </option>
                                          <option value="1000-5000">
                                            1,000 - 5,000
                                          </option>
                                          <option value="5000-10000">
                                            5,000 - 10,000
                                          </option>
                                          <option value="10000+">
                                            10,000 +
                                          </option>
                                        </select>
                                      </div>
                                    </div>
                                  </div>
                                  <div className="row">
                                    <div className="col-lg-3 col-md-6 col-sm-12">
                                      <div className="form-group">
                                        <label>
                                          Price Detail&nbsp;
                                          <span
                                            style={{
                                              color: "#ec8b0d",
                                              fontSize: 22,
                                            }}
                                          >
                                            *
                                          </span>
                                        </label>
                                        <a
                                          data-container="body"
                                          style={{
                                            display: "inline",
                                            margin: "0",
                                            padding: "0",
                                          }}
                                          title="This is the exact price of your service, as a number, in the currency that you selected in ‘Price Currency <br><br><i>If you have more questions, please ask in 'Search' in top right hand corner of website or see our FAQs page. Or then ask our team at supplier@protectbox.com or as shown in the footer via chat/ telephone. Supplier@protectbox.com is also checked outside of the working hours for chat/telephone enquiries.</i>"
                                          className="tooltiplink</i> "
                                          href="javascript:void(0);"
                                          data-toggle="tooltip"
                                          data-placement="right"
                                          data-html="true"
                                        >
                                          <span style={{ color: "#83C72A" }}>
                                            <FaInfoCircle />
                                          </span>
                                        </a>
                                        <input
                                          type="number"
                                          className="form-control "
                                          name="price_details"
                                          onkeydown="javascript: return event.keyCode == 69 ? false : true"
                                        />
                                      </div>
                                    </div>
                                    <div className="col-lg-3 col-md-6 col-sm-12">
                                      <div className="form-group">
                                        <label>
                                          Operating System&nbsp;
                                          <span
                                            style={{
                                              color: "#ec8b0d",
                                              fontSize: 22,
                                            }}
                                          >
                                            *
                                          </span>
                                        </label>
                                        <select
                                          className="form-control"
                                          name="operating_system"
                                        >
                                          <option selected disabled value>
                                            Please select
                                          </option>
                                          <option value="Windows 7 or Below">
                                            Windows 7 or Below
                                          </option>
                                          <option value="Windows 8+">
                                            Windows 8 or Above
                                          </option>
                                          <option value="Linux">Linux</option>
                                          <option value="Mac">Mac</option>
                                        </select>
                                      </div>
                                    </div>
                                    <div className="col-lg-3 col-md-6 col-sm-12">
                                      <div className="form-group">
                                        <label>
                                          Specialist Hardware&nbsp;
                                          <span
                                            style={{
                                              color: "#ec8b0d",
                                              fontSize: 22,
                                            }}
                                          >
                                            *
                                          </span>
                                        </label>
                                        <select
                                          className="form-control"
                                          name="specialist_hardware"
                                        >
                                          <option selected disabled value>
                                            Please select
                                          </option>
                                          <option value="yes">Yes</option>
                                          <option value="no">No</option>
                                        </select>
                                      </div>
                                    </div>
                                    <div className="col-lg-3 col-md-6 col-sm-12">
                                      <div className="form-group">
                                        <label>
                                          3rd Party Software&nbsp;
                                          <span
                                            style={{
                                              color: "#ec8b0d",
                                              fontSize: 22,
                                            }}
                                          >
                                            *
                                          </span>
                                        </label>
                                        <select
                                          className="form-control"
                                          name="third_party_software"
                                        >
                                          <option selected disabled value>
                                            Please select
                                          </option>
                                          <option value="yes">Yes</option>
                                          <option value="no">No</option>
                                        </select>
                                      </div>
                                    </div>
                                  </div>
                                  <div className="row">
                                    <div className="col-lg-3 col-md-6 col-sm-12">
                                      <div className="form-group">
                                        <label>
                                          Ease of Setup&nbsp;
                                          <span
                                            style={{
                                              color: "#ec8b0d",
                                              fontSize: 22,
                                            }}
                                          >
                                            *
                                          </span>
                                        </label>
                                        <select
                                          className="form-control"
                                          name="ease_setup"
                                        >
                                          <option selected disabled value>
                                            Please select
                                          </option>
                                          <option value="specialist">
                                            Specialist
                                          </option>
                                          <option value="non-specialist">
                                            Non-Specialist
                                          </option>
                                        </select>
                                      </div>
                                    </div>
                                    <div className="col-lg-3 col-md-6 col-sm-12">
                                      <div className="form-group">
                                        <label>
                                          Protection Level&nbsp;
                                          <span
                                            style={{
                                              color: "#ec8b0d",
                                              fontSize: 22,
                                            }}
                                          >
                                            *
                                          </span>
                                        </label>
                                        <select
                                          className="form-control"
                                          name="protection_level"
                                        >
                                          <option selected disabled value>
                                            Please select
                                          </option>
                                          <option value="basic">Basic</option>
                                          <option value="advanced">
                                            Advanced
                                          </option>
                                          <option value="complete">
                                            Complete
                                          </option>
                                        </select>
                                      </div>
                                    </div>
                                    <div className="col-lg-3 col-md-6 col-sm-12">
                                      <div className="form-group">
                                        <label>Product Link</label>
                                        <span
                                          style={{
                                            color: "#ec8b0d",
                                            fontSize: 22,
                                          }}
                                        >
                                          *
                                        </span>
                                        <a
                                          data-container="body"
                                          style={{
                                            display: "inline",
                                            margin: "0",
                                            padding: "0",
                                          }}
                                          title="Please add pdf link here including product description, reviews as well as terms & conditions. <br><br>
<>If you have more questions, please ask in 'Search' in top right hand corner of website. Or then ask our team at supplier@protectbox.com or as shown in the footer via chat/telephone. Supplier@protectbox.com is also checked outside of the working hours for chat/telephone enquiries"
                                          className="pad-0"
                                          href="javascript:void(0);"
                                          data-toggle="tooltip"
                                          data-placement="right"
                                          data-html="true"
                                        >
                                          <span style={{ color: "#83C72A" }}>
                                            <FaInfoCircle />
                                          </span>
                                        </a>
                                        <input
                                          type="text"
                                          className="form-control"
                                          name="product_link"
                                        />
                                      </div>
                                    </div>
                                    <div className="col-lg-3 col-md-6 col-sm-12">
                                      <div className="form-group">
                                        <label>
                                          Commission Detail&nbsp;
                                          <span
                                            style={{
                                              color: "#ec8b0d",
                                              fontSize: 22,
                                            }}
                                          >
                                            *
                                          </span>
                                        </label>{" "}
                                        <a
                                          data-container="body"
                                          style={{
                                            display: "inline",
                                            margin: "0",
                                            padding: "0",
                                          }}
                                          title="You need to select “Product Category” first, to auto-populate our preferred option for “Commission Detail”. This is the Commission that ProtectBox takes for re-selling your services. Our commission is calculated as a percentage of the price of your service (which you input in “Price Detail”). Our fees are set & vary according to category based on ease of selling. They are value for money compared to the rest of the market. Also, our fees are fair, we only take a fee when we make an actual Sale for you. Many in the market are charging higher fees (some as much as 10x more) for an (unqualified) introduction or at best, a qualified lead but not an actual Sale, like ProtectBox does. <br><br>
<i>If you have more questions, please ask in 'Search' in top right hand corner of website. Or then ask our team at supplier@protectbox.com or as shown in the footer via chat/telephone. Supplier@protectbox.com is also checked outside of the working hours for chat/telephone enquiries"
                                          className="tooltiplink</i> "
                                          href="javascript:void(0);"
                                          data-toggle="tooltip"
                                          data-placement="left"
                                          data-html="true"
                                        >
                                          <span style={{ color: "#83C72A" }}>
                                            <FaInfoCircle />
                                          </span>
                                        </a>
                                        <select
                                          className="form-control"
                                          name="commission_detail"
                                        >
                                          <option selected disabled value>
                                            Please select
                                          </option>
                                          <option value={10}>10%</option>
                                          <option value={20}>20%</option>
                                          <option value={30}>30%</option>
                                        </select>
                                      </div>
                                    </div>
                                  </div>
                                  <div className="row">
                                    <div className="col-lg-3 col-md-6 col-sm-12">
                                      <div className="form-group">
                                        <label>
                                          Payment Option&nbsp;
                                          <span
                                            style={{
                                              color: "#ec8b0d",
                                              fontSize: 22,
                                            }}
                                          >
                                            *
                                          </span>
                                        </label>
                                        <select
                                          className="form-control"
                                          name="payment_option"
                                        >
                                          <option selected disabled value>
                                            Please select
                                          </option>
                                          <option value="Monthly">
                                            Monthly
                                          </option>
                                          <option value="Quarterly">
                                            Quarterly
                                          </option>
                                          <option value="Yearly">Yearly</option>
                                        </select>
                                      </div>
                                    </div>
                                    <div className="col-lg-3 col-md-6 col-sm-12">
                                      <div className="form-group">
                                        <label>Government Voucher</label>
                                        &nbsp;
                                        <span
                                          style={{
                                            color: "#ec8b0d",
                                            fontSize: 25,
                                          }}
                                        />
                                        <a
                                          data-container="body"
                                          style={{
                                            display: "inline",
                                            margin: "0",
                                            padding: "0",
                                          }}
                                          title="Amount (numbers) in same currency that price was given <br><br>
<i>If you have more questions, please ask in 'Search' in top right hand corner of website. Or then ask our team at supplier@protectbox.com or as shown in the footer via chat/telephone. Supplier@protectbox.com is also checked outside of the working hours for chat/telephone enquiries"
                                          className="tooltiplink</i> "
                                          href="javascript:void(0);"
                                          data-toggle="tooltip"
                                          data-placement="right"
                                          data-html="true"
                                        >
                                          <span style={{ color: "#83C72A" }}>
                                            <FaInfoCircle />
                                          </span>
                                        </a>
                                        <input
                                          type="nubmer"
                                          className="form-control"
                                          name="government_voucher"
                                        />
                                      </div>
                                    </div>
                                    <div className="col-lg-3 col-md-6 col-sm-12">
                                      <div className="form-group">
                                        <label>Cash Back</label>&nbsp;
                                        <span
                                          style={{
                                            color: "#ec8b0d",
                                            fontSize: 25,
                                          }}
                                        />
                                        <a
                                          style={{
                                            display: "inline",
                                            margin: "0",
                                            padding: "0",
                                          }}
                                          data-container="body"
                                          title="Amount (numbers) in same currency that price was given <br><br>
<i>If you have more questions, please ask in 'Search' in top right hand corner of website. Or then ask our team at supplier@protectbox.com or as shown in the footer via chat/telephone. Supplier@protectbox.com is also checked outside of the working hours for chat/telephone enquiries"
                                          className="tooltiplink</i> "
                                          href="javascript:void(0);"
                                          data-toggle="tooltip"
                                          data-placement="right"
                                          data-html="true"
                                        >
                                          <span style={{ color: "#83C72A" }}>
                                            <FaInfoCircle />
                                          </span>
                                        </a>
                                        <input
                                          type="text"
                                          className="form-control"
                                          name="cash_back"
                                        />
                                      </div>
                                    </div>
                                    <div className="col-lg-3 col-md-6 col-sm-12">
                                      <div className="form-group">
                                        <label>
                                          Rating/Ranking&nbsp;
                                          <span
                                            style={{
                                              color: "#ec8b0d",
                                              fontSize: 21,
                                            }}
                                          />
                                        </label>{" "}
                                        <a
                                          data-container="body"
                                          style={{
                                            display: "inline",
                                            margin: "0",
                                            padding: "0",
                                          }}
                                          title="Please add links to any Ratings & Reviews of your service here, separating them using commas. We also ask business customers for their feedback on your service when they buy through ProtectBox. We will be updating this page shortly to show you that information, so that you can respond to any feedback left about your service. <br><br><i>If you have more questions, please ask in 'Search' in top right hand corner of website or see our FAQs page. Or then ask our team at supplier@protectbox.com or as shown in the footer via chat/ telephone. Supplier@protectbox.com is also checked outside of the working hours for chat/telephone enquiries.</i>"
                                          className="tooltiplink</i> "
                                          href="javascript:void(0);"
                                          data-toggle="tooltip"
                                          data-placement="right"
                                          data-html="true"
                                        >
                                          <span style={{ color: "#83C72A" }}>
                                            <FaInfoCircle />
                                          </span>
                                        </a>
                                        <input
                                          type="text"
                                          className="form-control"
                                          name="rating_ranking"
                                        />
                                      </div>
                                    </div>
                                  </div>
                                  <div className="row">
                                    <div className="col-lg-3 col-md-6 col-sm-12">
                                      <div className="form-group">
                                        <label>
                                          Locations&nbsp;service&nbsp;is&nbsp;sold&nbsp;in&nbsp;
                                          <span
                                            style={{
                                              color: "#ec8b0d",
                                              fontSize: 22,
                                            }}
                                          >
                                            *
                                          </span>
                                        </label>
                                        <select
                                          className="selectpicker form-control"
                                          data-live-search="true"
                                          multiple
                                          name="location_service[<?php echo $key;?>]"
                                          data-dropup-auto="false"
                                        >
                                          <option value disabled selected>
                                            Please select
                                          </option>
                                          <option value="Northern Ireland">
                                            Northern Ireland (UK)
                                          </option>
                                          <option value="Ireland">
                                            Ireland (Europe)
                                          </option>
                                          <option value="Mainland UK">
                                            Mainland UK
                                          </option>
                                          <option value="Europe">
                                            Europe (Continental)
                                          </option>
                                          <option value="North America">
                                            North America
                                          </option>
                                          <option value="Central America">
                                            Central America
                                          </option>
                                          <option value="South America">
                                            South America
                                          </option>
                                          <option value="Africa">Africa</option>
                                          <option value="Middle East Qatar">
                                            Middle East (UAE, Qatar, Oman etc)
                                          </option>
                                          <option value="Middle East Israel">
                                            Middle East (Israel)
                                          </option>
                                          <option value="Russia">Russia</option>
                                          <option value="South Asia">
                                            South Asia (India, Pakistan,
                                            Bangladesh etc)
                                          </option>
                                          <option value="South East Asia">
                                            South East Asia (China, Japan etc)
                                          </option>
                                          <option value="South Pacific">
                                            South Pacific (Australia, New
                                            Zealand etc)
                                          </option>
                                        </select>
                                      </div>
                                    </div>
                                    <div className="col-lg-3 col-md-6 col-sm-12">
                                      <div className="form-group">
                                        <label>
                                          Regulation&nbsp;
                                          <span
                                            style={{
                                              color: "#ec8b0d",
                                              fontSize: 22,
                                            }}
                                          >
                                            *
                                          </span>
                                        </label>
                                        <select
                                          className="form-control"
                                          name="regulation"
                                        >
                                          <option selected disabled value>
                                            Please select
                                          </option>
                                          <option value="CyberEssentials">
                                            CyberEssentials
                                          </option>
                                          <option value="General Data Protection regulation (GDPR)">
                                            General Data Protection regulation
                                            (GDPR)
                                          </option>
                                          <option value="Control Objectives for Information and Related Technology (COBIT)">
                                            Control Objectives for Information
                                            and Related Technology (COBIT)
                                          </option>
                                          <option value="ISO/IEC 27000-series">
                                            ISO/IEC 27000-series
                                          </option>
                                          <option value="FSMA/NIST">
                                            FSMA/NIST
                                          </option>
                                          <option value="Payment Card Industry Data Security Standard (PCI DSS)">
                                            Payment Card Industry Data Security
                                            Standard (PCI DSS)
                                          </option>
                                        </select>
                                      </div>
                                    </div>
                                    <div className="col-lg-3 col-md-6 col-sm-12">
                                      <div className="form-group">
                                        <label>
                                          Refund possible?&nbsp;
                                          <span
                                            style={{
                                              color: "#ec8b0d",
                                              fontSize: 22,
                                            }}
                                          >
                                            *
                                          </span>
                                        </label>
                                        <select
                                          className="form-control valid"
                                          name="refund_possible"
                                        >
                                          <option value disabled selected>
                                            Please select
                                          </option>
                                          <option value="yes">Yes</option>
                                          <option value="no">No</option>
                                        </select>
                                      </div>
                                    </div>
                                    <div className="col-lg-3 col-md-6 col-sm-12">
                                      <div className="form-group">
                                        <label>Instructions to user</label>
                                        &nbsp;
                                        <span
                                          style={{
                                            color: "#ec8b0d",
                                            fontSize: 25,
                                          }}
                                        />
                                        <a
                                          data-container="body"
                                          style={{
                                            display: "inline",
                                            margin: "0",
                                            padding: "0",
                                          }}
                                          title="Please put download instructions or call back instructions here. As well as terms & conditions <br><br>
<i>If you have more questions, please ask in 'Search' in top right hand corner of website. Or then ask our team at supplier@protectbox.com or as shown in the footer via chat/telephone. Supplier@protectbox.com is also checked outside of the working hours for chat/telephone enquiries"
                                          className="tooltiplink</i> "
                                          href="javascript:void(0);"
                                          data-toggle="tooltip"
                                          data-placement="right"
                                          data-html="true"
                                        >
                                          <span style={{ color: "#83C72A" }}>
                                            <FaInfoCircle />
                                          </span>
                                        </a>
                                        <textarea
                                          className="form-control"
                                          name="instruction_details"
                                          defaultValue={""}
                                        />
                                      </div>
                                    </div>
                                    <div className="col-lg-3 col-md-6 col-sm-12">
                                      <div className="form-group">
                                        <label>Service Status</label>&nbsp;
                                        <span
                                          style={{
                                            color: "#ec8b0d",
                                            fontSize: 25,
                                          }}
                                        />
                                        <div className="options">
                                          <label className="switch-light switch-ios">
                                            <input
                                              type="checkbox"
                                              name="status"
                                              id="service_option"
                                            />
                                            <span className="mx-1">
                                              <span>Inactive</span>
                                              <span>Active</span>
                                            </span>
                                            <a />
                                          </label>
                                        </div>
                                      </div>
                                    </div>
                                    <div className="col-lg-3 col-md-6 col-sm-12">
                                      <div className="form-group">
                                        <label>Method Sold</label>&nbsp;
                                        <span
                                          style={{
                                            color: "#ec8b0d",
                                            fontSize: 25,
                                          }}
                                        />
                                        <select
                                          className="form-control valid"
                                          name="distributor"
                                        >
                                          <option value disabled selected>
                                            Please select
                                          </option>
                                          <option value="Direct">Direct</option>
                                        </select>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div className="col-md-12 pl-0">
                                  <div className="col-lg-3 col-md-6 col-sm-12 pl-0">
                                    <div className="form-group">
                                      <label>Terms And Conditions</label>
                                      &nbsp;
                                      <span
                                        style={{
                                          color: "#ec8b0d",
                                          fontSize: 25,
                                        }}
                                      />
                                      <textarea
                                        className="form-control"
                                        name="terms"
                                        style={{ height: 50 }}
                                      />
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div className="row">
                                <div className="col-md-8 col-sm-8">
                                  <div className="form-group">
                                    <div className="checkbox">
                                      <label style={{ fontWeight: "bold" }}>
                                        <input
                                          type="checkbox"
                                          name="check_payment"
                                          defaultValue={1}
                                          className="required"
                                          style={{ marginTop: 13 }}
                                        />{" "}
                                        <span
                                          style={{
                                            fontSize: 18,
                                            color: "#ec8b0d",
                                          }}
                                        >
                                          Yes, I have completed payment setup.
                                        </span>{" "}
                                        If you haven't,{" "}
                                        <a
                                          style={{
                                            display: "inline",
                                            margin: "0",
                                            padding: "0",
                                          }}
                                          href="<?php echo base_url('payments');?>"
                                          target="_blank"
                                        >
                                          Click here
                                        </a>{" "}
                                        to setup.&nbsp;
                                        <span
                                          style={{
                                            color: "#ec8b0d",
                                            fontSize: 22,
                                          }}
                                        >
                                          *
                                        </span>
                                        <span className="checkmark" />
                                      </label>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div className="row">
                                <div className="col-md-8 col-sm-8">
                                  <div className="form-group">
                                    <div className="checkbox">
                                      <label style={{ fontWeight: "bold" }}>
                                        <input
                                          type="checkbox"
                                          name="terms_condition"
                                          defaultValue={1}
                                          className="required"
                                          style={{ marginTop: 13 }}
                                        />{" "}
                                        I acknowledge the{" "}
                                        <a
                                          style={{ display: "inline" }}
                                          href="<?php echo base_url('terms')?>"
                                          target="_blank"
                                          className="pad-0"
                                        >
                                          Terms and Conditions
                                        </a>
                                        .&nbsp;
                                        <span
                                          style={{
                                            color: "#ec8b0d",
                                            fontSize: 22,
                                          }}
                                        >
                                          *
                                        </span>
                                        <span className="checkmark" />
                                      </label>
                                    </div>
                                  </div>
                                </div>
                                <div
                                  className=" text-right"
                                  style={{ marginTop: 10 }}
                                >
                                  <button
                                    type="submit"
                                    name="save_products"
                                    value="save"
                                    className="btn btn-warning btn-medium"
                                    style={{
                                      marginLeft: 10,
                                      marginBottom: "0.6rem",
                                      borderRadius: "5px",
                                    }}
                                  >
                                    Save
                                  </button>
                                  <button
                                    style={{
                                      marginLeft: 10,
                                      marginBottom: "0.6rem",
                                      borderRadius: "5px",
                                    }}
                                    type="submit"
                                    name="save_products"
                                    value="add_more"
                                    className="btn btn-success btn-medium "
                                  >
                                    Add More Products
                                  </button>
                                </div>
                              </div>
                            </form>
                          </div>
                        </Tab>
                      </Tabs>
                    </div>
                    <div className="step">
                      <div className="row">
                        <div className="col-md-12 col-sm-12">
                          <div className="form-group">
                            <label style={{ fontWeight: "bold", fontSize: 16 }}>
                              <a
                                style={{ display: "inline" }}
                                href=""
                                target="_blank"
                                className="pad-0"
                              >
                                Click here
                              </a>{" "}
                              to find out more about signing up to a
                              Subscription service that gives you analysis on
                              all your sales.
                              <span className="checkmark" />
                            </label>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              {/* End col-md-12*/}
            </div>
            {/* End row */}
          </div>
          {/* End container */}
          <div id="uploadModal" className="modal fade" role="dialog">
            <div className="modal-dialog">
              {/* Modal content*/}
              <div className="modal-content">
                <div className="modal-header">
                  <button
                    style={{ borderRadius: "5px" }}
                    type="button"
                    className="close"
                    data-dismiss="modal"
                  >
                    ×
                  </button>
                  <h4 className="modal-title">
                    Upload Completeted Excel Spreadsheeet
                  </h4>
                </div>
                <div className="modal-body">
                  {/* Form */}
                  <form method="post" action="">
                    <div className="form-group">
                      <label htmlFor="exampleFormControlFile1" />
                      <input
                        type="file"
                        className="form-control-file"
                        id="exampleFormControlFile1"
                        name="mapped"
                      />
                    </div>
                    <button
                      style={{ borderRadius: "5px" }}
                      type="submit"
                      className="btn btn-primary"
                    >
                      Submit
                    </button>
                  </form>
                  {/* Preview*/}
                  <div id="preview" />
                </div>
              </div>
            </div>
          </div>
        </main>
        <Footer2 />
      </div>
    );
  }
}
