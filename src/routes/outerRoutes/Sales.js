import React, { Component } from "react";
import Header2 from "./../../components/Header2";
import Footer2 from "../../components/Footer2";
import { Accordion, Card, Button } from "react-bootstrap";
import { Pie, Bar } from "react-chartjs-2/es";

import { Form } from "react-bootstrap";

import DataTable from "react-data-table-component";
import "../../styles/sales.css";
import { Link } from "react-router-dom";

const data = {
  labels: ["TechData UK", "TechData US"],
  datasets: [
    {
      label: "My First dataset",
      fill: false,
      lineTension: 0.1,
      backgroundColor: ["#eb6b0c", "#6ca71c"],
      borderColor: "rgba(75,192,192,1)",
      borderCapStyle: "butt",
      borderDash: [],
      borderDashOffset: 0.0,
      borderJoinStyle: "miter",
      pointBorderColor: "rgba(75,192,192,1)",
      pointBackgroundColor: "#fff",
      pointBorderWidth: 1,
      pointHoverRadius: 5,
      pointHoverBackgroundColor: "rgba(75,192,192,1)",
      pointHoverBorderColor: "rgba(220,220,220,1)",
      pointHoverBorderWidth: 2,
      pointRadius: 1,
      pointHitRadius: 10,
      data: [38, 61],
    },
  ],
};
const datas = [
  {
    id: 1,
    on: 1,
    cat: "Authentication",
    pro: "	Web Development",
    tp: "400",
    po: "Card",
    os: "1 Order delivered",
    vi: (
      <button
        style={{ width: "100%" }}
        className="btn btn-success  btn-login-submit"
      >
        Invoice
      </button>
    ),
    vd: (
      <button
        style={{ width: "100%" }}
        className="btn btn-success  btn-login-submit"
      >
        Invoice
      </button>
    ),
  },
  {
    id: 1,
    on: 2,
    cat: "Authentication",
    pro: "Systems Hardware",
    tp:
      "Axiom - DDR2 - 4 GB: 2 x 2 GB - SO-DIMM 200-pin - 667 MHz / PC2-5300 - unbuffered - non-ECC	",
    po: "$ 51.53 ",
    os: "Card",
    vi: (
      <button
        style={{ width: "100%" }}
        className="btn btn-success  btn-login-submit"
      >
        Invoice
      </button>
    ),
    vd: (
      <button
        style={{ width: "100%" }}
        className="btn btn-success  btn-login-submit"
      >
        Invoice
      </button>
    ),
  },
];
const columns = [
  {
    name: "Order number",
    selector: "on",
    sortable: true,
  },
  {
    name: "Category",
    selector: "cat",
    sortable: true,
    right: true,
  },
  {
    name: "Product",
    selector: "pro",
    sortable: true,
    right: true,
  },
  {
    name: "Total Price",
    selector: "tp",
    sortable: true,
    right: true,
  },
  {
    name: "Payment Option",
    selector: "po",
    sortable: true,
    right: true,
  },
  {
    name: "Order Status",
    selector: "os",
    sortable: true,
    right: true,
  },
  {
    name: "View Invoice ",
    selector: "vi",
    sortable: true,
    right: true,
  },
  {
    name: "View details",
    selector: "vd",
    sortable: true,
    right: true,
  },
];
export default class Sales extends Component {
  render() {
    return (
      <div id="load">
      <section className="pb-section slider-main">
        <Header2 />
      </section>
        <section
          id="sub_header"
          style={{
            background: "#f5f5f5",
            boxShadow: "inset 0 1px 10px 1px rgba(0,0,0,.41)",
          }}
        >
          <div className="container">
            <div
              className="main_title"
              style={{
                background: "none",
                textAlign: "center",
                fontSize: 40,
                color: "#000",
                bottom: 30,
              }}
            >
              Subsciption
            </div>
          </div>
        </section>

        <main className="sales">
          <div className="container margin_60">
            <Accordion>
              <div className="repeatrow">
                <div class="qheading" style={{ marginBottom: 10 }}>
                  <Accordion.Toggle as={Link} variant="link" eventKey="0">
                    <p style={{ marginBottom: 0 }}>
                      <a href="#collapse0" style={{ fontSize: 20, fontWeight: "bold", color: "#000" }}>
                        Sales Analysis
                        <span class="spanarrow rarrow" style={{ float: "right" }}>
                          <i class="fa fa-arrow-right" aria-hidden="true"></i></span>
                      </a>
                    </p>
                  </Accordion.Toggle>

                </div>
                <Accordion.Collapse eventKey="0">
                  <div>
                    {/* <div className="tab-content rounded_div"> */}
                    <div className=" table-responsive">
                      <h4 style={{ fontSize: 18, fontWeight: "normal" }}>
                        For GBP5 per month (+ VAT, if applicable) you can have
                        continuous access to the below dashboard of information
                        on all of your sales.
                      </h4>
                      {/* <div className="col-md-12">
                        <div className="row"></div>
                      </div>
                      <div className="col-md-12">
                        <div
                          id="donutchart"
                          style={{ width: 1000, height: 600 }}
                        />
                      </div>
                      <div className="col-md-12">
                        <div id="graph_solution" className="text-center" />
                      </div> */}
                      <div className="row">
                        <div className="col-md-6">
                          <Pie ref="chart" data={data} />
                        </div>
                      </div>

                      <Bar ref="chart" data={data} />
                      <Bar ref="chart" data={data} />
                    </div>
                  </div>
                </Accordion.Collapse>
              </div>
              <div className="repeatrow">
                <div class="qheading" style={{ marginBottom: 10 }}>
                  <Accordion.Toggle as={Link} variant="link" eventKey="1">

                    <p style={{ marginBottom: 0 }}>
                      <a href="#collapse1" style={{ fontSize: 20, fontWeight: "bold", color: "#000" }}>
                        Sales Details
                        <span class="spanarrow rarrow" style={{ float: "right" }}>
                          <i class="fa fa-arrow-right" aria-hidden="true"></i></span>
                      </a>
                    </p>
                  </Accordion.Toggle>
                </div>
                <Accordion.Collapse eventKey="1">
                  <div>
                    {/* <div className="tab-content rounded_div"> */}
                    <div className=" table-responsive">
                      <h4 style={{ fontSize: 18, fontWeight: "normal" }}>
                        For GBP5 per month (+ VAT, if applicable) you can have
                        continuous access to the below dashboard of information
                        on all of your sales.
                      </h4>
                      {/* <div className="col-md-12">
                        <div className="row"></div>
                      </div>
                      <div className="col-md-12">
                        <div
                          id="donutchart"
                          style={{ width: 1000, height: 600 }}
                        />
                      </div> */}
                      <ul className="cupon-btn">
                        <li>
                          <button>Copy</button>
                        </li>
                        <li>
                          <button>CSV</button>
                        </li>
                        <li>
                          <button>Excel</button>
                        </li>
                        <li>
                          <button>Print</button>
                        </li>
                      </ul>
                      <div className="col-md-12">
                        <DataTable
                          title="All Sales"
                          columns={columns}
                          data={datas}
                          subHeader={true}
                          subHeaderAlign={"left"}
                          pagination
                          compact={true}
                          wrap={true}
                          subHeaderComponent={
                            <div
                              style={{ display: "flex", alignItems: "center" }}
                            >
                              <Form.Group
                                style={{
                                  display: "flex",
                                  alignItems: "center",
                                }}
                                controlId="exampleForm.ControlSelect1"
                              >
                                <Form.Label>Show</Form.Label>
                                <Form.Control
                                  as="select"
                                  style={{ margin: "0 0.5rem" }}
                                >
                                  <option>1</option>
                                  <option>2</option>
                                  <option>3</option>
                                  <option>4</option>
                                  <option>5</option>
                                </Form.Control>
                                <Form.Label>Entries</Form.Label>
                              </Form.Group>
                            </div>
                          }
                        />
                      </div>
                    </div>
                    {/* </div> */}
                  </div>
                </Accordion.Collapse>
              </div>
              <div className="repeatrow">
                <div class="qheading" style={{ marginBottom: 10 }}>
                  <Accordion.Toggle as={Link} variant="link" eventKey="2">
                    <p style={{ marginBottom: 0 }}>
                      <a href="#collapse2" style={{ fontSize: 20, fontWeight: "bold", color: "#000" }}>
                        News
                        <span class="spanarrow rarrow" style={{ float: "right" }}>
                          <i class="fa fa-arrow-right" aria-hidden="true"></i></span>
                      </a>
                    </p>
                  </Accordion.Toggle>
                </div>
                <Accordion.Collapse eventKey="2">
                  <div className="repeatrow">
                    <div className=" table-responsive">
                      <div
                        align="center"
                        style={{
                          fontSize: 20,
                          color: "red",
                          fontWeight: "bold",
                        }}
                      >
                        No news yet
                        </div>
                    </div>
                  </div>
                </Accordion.Collapse>
              </div>
              <div className="repeatrow">
                <div class="qheading" style={{ marginBottom: 10 }}>
                  <Accordion.Toggle as={Link} variant="link" eventKey="3">
                    <p style={{ marginBottom: 0 }}>
                      <a href="#collapse2" style={{ fontSize: 20, fontWeight: "bold", color: "#000" }}>
                        Feedback
                        <span class="spanarrow rarrow" style={{ float: "right" }}>
                          <i class="fa fa-arrow-right" aria-hidden="true"></i></span>
                      </a>
                    </p>
                  </Accordion.Toggle>
                </div>
                <Accordion.Collapse eventKey="3">

                  <div className="repeatrow">
                    <div className=" table-responsive">
                      <table
                        id="example"
                        className="table table-striped table-bordered"
                        style={{ width: "100%" }}
                      >
                        <thead>
                          <tr>
                            <th width="15%">Customer&nbsp;Name</th>
                            <th width="15%">Product</th>
                            <th width="15%">Rating</th>
                            <th width="15%">Review</th>
                            <th width="15%">Date</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>Kiran </td>
                            <td>
                              Acer Total Protection Upgrade - Extended service
                              - shipment - for Acer H5360, H7530, X1260, X1261
                              </td>
                            <td>
                              <input
                                type="number"
                                id="rating-readonly"
                                className="rating read_only"
                                data-clearable="remove"
                                defaultValue="<?php echo $fetch_reviews->rating;?>"
                                data-readonly
                              />
                            </td>
                            <td> Range could be better.</td>
                            <td> 04/02/2019</td>
                          </tr>

                          {/* <tr>
                                <td
                                  colSpan={7}
                                  align="center"
                                  style={{ fontSize: 20, color: "red" }}
                                >
                                  <b>No payments yet</b>
                                </td>
                              </tr> */}
                        </tbody>
                      </table>
                    </div>
                  </div>
                </Accordion.Collapse>
              </div>
            </Accordion>
            <div className="row">
              <div className="col-md-12">
                <div className="mainsearchdiv2" id="accordion"></div>
              </div>
            </div>
            <div className="row">
              <div className="col-md-12 col-sm-12">
                <div className="form-group">
                  <form
                    action="<?php echo base_url('sales/stripe_unsubscribe')?>"
                    method="POST"
                  >
                    <input
                      type="hidden"
                      defaultValue="<?php echo ((!empty($subscribed_supplier))? $subscribed_supplier->subscription_id:'') ;?>"
                      name="subscriber_id"
                    />
                    <p />
                    <button
                      type="submit"
                      className="btn btn-success  btn-login-submit"
                    >
                      Unsubscribe
                              </button>{" "}
                              &nbsp;&nbsp;Click " Unsubscribe" to cancel your
                              Subscription.
                              <p />
                  </form>
                </div>
              </div>
            </div>
          </div>
        </main>
        <Footer2 />
      </div>
    );
  }
}
