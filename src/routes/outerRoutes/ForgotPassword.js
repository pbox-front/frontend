import React, { Fragment, useEffect, useState } from "react";
import Footer from "./../../components/Footer";
import Header from "./../../components/Header";
import axios from "axios";
import LoaderGif from '../../images2/favicon.gif';
import { Alert } from "react-bootstrap";

const ForgotPassword = () => {

  const [callingServer, setCallingServer] = useState(false);
  const [forgotErrorMessage, setForgotErrorMessage] = useState(null);
  const [fotgotSuccessMessage, setFotgotSuccessMessage] = useState(null);
  const [email, setEmail] = useState('');

  useEffect(() => {
    document.title = "ProtectBox | Forgotten Password"
  }, [])

  const onForgotPassword = () => {
    const formData = new FormData()
    formData.append('email', email);
    setCallingServer(true);
    axios.post('www/api/forgotpassapi', formData)
      .then((res) => {
        setCallingServer(false);
        setEmail('');
        setFotgotSuccessMessage(res.data.message);
      }).catch(error => {
        if (error && error.response && error.response.data) {
          setCallingServer(false);
          setForgotErrorMessage(error.response.data.message);
        }
      })
  }

  return (
    <Fragment>
      <div id="load" />
      <section className="pb-section slider-main">
        <Header />
      </section>
      <section
        id="sub_header"
        style={{
          background: "#f5f5f5",
          boxShadow: "inset 0 1px 10px 1px rgba(0,0,0,.41)",
        }}
      >
        <div className="container">
          <div
            className="main_title"
            style={{
              background: "none",
              textAlign: "center",
              fontSize: 40,
              color: "#000",
              bottom: 30,
            }}
          >
            Forgotten Password
        </div>
        </div>
      </section>

      <main>
        <div className="container margin_60">
          <div className="row forgot-password">
            {!callingServer ?
              <div
                className="col-md-6 col-md-offset-3"
                style={{
                  padding: "10px 25px 10px",
                  border: "1px solid #CCC",
                  boxShadow: "0px 0px 3px #bfbfbf",
                  borderRadius: 5,
                  background: "#f5f5f5",
                }}
              >
                {fotgotSuccessMessage && <Alert variant={'success'}>
                  <strong>{fotgotSuccessMessage}</strong>
                </Alert>
                }
                {forgotErrorMessage && <Alert variant={'danger'}>
                  <strong>{forgotErrorMessage}</strong>
                </Alert>
                }
                <div className="row" style={{ marginTop: 20 }}>
                  <div className="col-md-10 col-sm-10 col-md-offset-1">
                    <div className="form-group">
                      <label>Email Address</label>
                      <input
                        type="email"
                        className="form-control required"
                        id="email_quote"
                        name="email"
                        value={email}
                        onChange={(event) => setEmail(event.target.value)}
                      />
                    </div>
                  </div>
                </div>
                <div className="col-md-12 col-sm-12">
                  <div style={{ textAlign: "center" }} className="form-group ">
                    <button
                      type="submit"
                      className="btn btn-success  btn-login-submit"
                      onClick={onForgotPassword}
                    >
                      Submit
                  </button>
                  </div>
                </div>
              </div>
              :
              <div
                className="col-md-6 col-md-offset-3"
                style={{
                  padding: "10px 25px 10px",
                  boxShadow: "0px 0px 3px #bfbfbf",
                  borderRadius: 5,
                  textAlign: "center",
                  height: 400,
                  margin: "auto",
                }}
                id="loaderImg"
              >
                <img
                  src={LoaderGif}
                  style={{ height: 64, marginTop: 100 }}
                  alt="loader"
                />
                <h3>Please wait while we process your request.....</h3>
              </div>
            }
          </div>
        </div>
      </main>
      <Footer />
    </Fragment >
  );
};

export default ForgotPassword;
