import React, { useEffect } from "react";
import { Link } from 'react-router-dom';
import Footer from "./../../components/Footer";
import Header from "./../../components/Header";
import UseCases from "./../../components/UseCases";
import AwardWinning from "./../../components/AwardWinning";
import ExistingNews from "./../../components/ExistingNews";
import LastInsidesAndNews from "./../../components/LastInsidesAndNews";
import LatestInsides from "./../../components/LatestInsides";
import PartnerInvestor from "./../../components/PartnerInvestor";
import Slider from "react-slick";
import { FaEnvelope } from "react-icons/fa";


import { FaRegEnvelope } from "react-icons/fa";

import { OverlayTrigger, Button, Tooltip } from "react-bootstrap";
import { FaMedal, FaUserFriends } from "react-icons/fa";
import { FiBox } from "react-icons/fi";
import {
  AiFillLike,
  AiOutlinePercentage,
  AiOutlineShoppingCart,
} from "react-icons/ai";
import { DiGoogleAnalytics } from "react-icons/di";
import { GiCyberEye, GiMagnifyingGlass } from "react-icons/gi";
import { BsPlug } from "react-icons/bs";

const Supplier_Partner = () => {
  
  useEffect(() => {
    document.title = "ProtectBox | Suppliers & Partners"
  }, [])

  return (
    <div className="wrapper partner">
      <section className="pb-section slider-main">
        <Header />
        <div className="banner-section-main supplier-block">
          <div className="black-overlay">
            <div className="banner-text">
              <h1 className="pbh-2 white-color">Want an easy way to fight cybercrime (for your customers)<br/>and generate sales (not just leads)?</h1>
              <br/><br/>
              <Link className="btn btn-primary" to="/Registration">Sign me up</Link>
            </div>
          </div>
        </div>
      </section>
      <article className="pb-section pb-home-security">
        <div className="container">
          <h1 className="pbh-3">
            Marketplace (Cybersecurity &amp; more) as a service
          </h1>
          <div className="row">
            <div className="col-12 col-md-6 pr-3">
              <p>
                ProtectBox is the go-to platform for businesses looking for
                jargon-free cybersecurity bundles to protect every area of their
                business. We have built ProtectBox with your specific
                frustrations and needs in mind.
              </p>
              <p>
                {" "}
                Our pioneering search and comparison engine has been developed
                by a team of cyber and data experts from government and industry
                backgrounds. We also partner with orgnaisations who are the
                voice of small businesses.
              </p>
              <p>
                {" "}
                Our free comparison tool uses our award-winning algorithms to
                find the perfect cybersecurity bundles tailored to your
                business' needs.
              </p>
              <p>
                No bias. No jargon. Just a fast, easy to understand solution to
                your problem.
              </p>
            </div>
            <div className="col-12 col-md-6">
              <div className="pb-video">
                <div className="video-overlay">
                  <div className="video-icon">
                    <a
                      id="videobtn"
                      add
                      target="_blank"
                      className="video-btn white"
                      href="https://youtu.be/vWoNJ3nwzXs"
                    >
                      <i className="fa fa-play" />
                    </a>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-12 col-md-12">
              <div className="pb-btn-box">
                <Link className="btn btn-secondary" to="/Login">
                  Get Started
                </Link>
              </div>
            </div>
          </div>
        </div>
      </article>
      <UseCases />
      <section className="pb-section why-protect-box">
        <div className="container">
          <h1 className="pbh-3">Why work with ProtectBox?</h1>
          <div className="pb-row">
            <div className="pb-col-12 pb-col-md-2">
              <div className="pb-block">
                <i className="flaticon-medal"></i>
                <p>Award-winning technology and leadership</p>
              </div>
            </div>
            <div className="pb-col-12 pb-col-md-2">
              <div className="pb-block">
                <img className="img-icons" src="/icon/trust.png" />
                <p>We're building trust and confidence with responsible AI</p>
              </div>
            </div>
            <div className="pb-col-12 pb-col-md-2">
              <div className="pb-block">
                <img className="img-icons" src="/icon/friend.png" />
                <p>User-friendly portal, creating a great customer experience</p>
              </div>
            </div>
            <div className="pb-col-12 pb-col-md-2">
              <div className="pb-block">
                <img className="img-icons" src="/icon/analytics.png" />
                <p>Useful analytics provided to help you understand what your customers want (Subscription only)</p>
              </div>
            </div>
            <div className="pb-col-12 pb-col-md-2">
              <div className="pb-block">
                <img className="img-icons" src="/icon/product.png" />
                <p>White-label option to create a new product to offer your clients</p>
              </div>
            </div>
            <div className="pb-col-12 pb-col-md-2">
              <div className="pb-block">
                <img className="img-icons" src="/icon/percent.png" />
                <p>Generate sales - not just leads. We do the hard work for you</p>
              </div>
            </div>
            <div className="pb-col-12 pb-col-md-2">
              <div className="pb-block">
                <img className="img-icons" src="/icon/supplier.png" />
                <p>First to bundle cyber products and tailor to user requirements</p>
              </div>
            </div>
            <div className="pb-col-12 pb-col-md-2">
              <div className="pb-block">
                <img className="img-icons" src="/icon/plugin.png" />
                <p>Plug and play option to integrate in your own technology stack</p>
              </div>
            </div>
            <div className="pb-col-12 pb-col-md-2">
              <div className="pb-block">
                <img className="img-icons" src="/icon/search.png" />
                <p>Increase brand awareness and recognition</p>
              </div>
            </div>
            <div className="pb-col-12 pb-col-md-2">
              <div className="pb-block">
                <img className="img-icons" src="/icon/offering.png" />
                <p>Sustainable, fully-inclusive offering that makes cybersecurity accessible to everyone. But protection (beyond security) too</p>
              </div>
            </div>
            <div className="clearfix" />
          </div>
        </div>
      </section>
      <PartnerInvestor />
      <ExistingNews />
      <article className="pb-section home-different-section">
        <div className="container">
          <h1 className="pbh-3">Ready to get started?</h1>
          <div className="row">
            <div className="col-12 col-md-6 ul-partner">
              <br/>
              <p>It's simple to get started. Generate sales within minutes</p>
              <br/>
              <ul>
                <li>Register for a ProtectBox account</li>
                <li>Complete our user-friendly questionnaire</li>
                <li>
                  Add your payment details. See our transparent pricing here
                </li>
                <li>
                  Press the 'submit' button and wait for the sales to come in
                </li>
                <li>
                  While you're waiting for the sales, you can review your analytics <br/>
                  to understand exactly what your potential customers are <br/>
                  looking for, to further optimise your ofering
                </li>
              </ul>
              <div className="pb-btn-box">
                <a className="btn btn-secondary" href="javascript:void(0)">
                  Get Started
                </a>
              </div>
            </div>
            <div className="col-12 col-md-6">
              <div className="different-img">
                <img src="./../../../image/video-overview.png" alt />
              </div>
            </div>
          </div>
        </div>
      </article>
      <AwardWinning />
      <LastInsidesAndNews />
      <LatestInsides />
      <Footer />
    </div>
  );
};

export default Supplier_Partner;
