import React, {Component} from "react";
import Header from "../../components/Header";
import Footer from "./../../components/Footer";
import BlogItem from "./../../components/BlogItem";
import LatestInsides from "./../../components/LatestInsides";
import { Pagination } from "react-bootstrap";
import axios from "axios";
import { BiEnvelope } from "react-icons/bi";

class Blog extends Component {

  constructor(props) {
    super(props);

    this.state = {
      entries: [],
      entries_category: "All",
      entries_order: "recent",
      currentPage: 1,
      entriesPerPage: "20",
      entriesTotalCount: 0,
      loading: false
    }
  }

  componentDidMount() {
    this._getBlogList();
  }

  _getBlogList() {

    this.setState({loading: true});
    const url = "/www/api/blogapi";
    let formData = new FormData();

    const category = this.state.entries_category;
    const order = this.state.entries_order;
    const page = this.state.currentPage;

    formData.append("category", category);
    formData.append("order", order);
    formData.append("page", page);

    axios.post(url, formData, {
      headers: {
        "Content-Type": `multi-part/form-data`,
        "Access-Control-Allow-Origin": "*"
      },
    }).then((res) => {
      if (res.data.message == "Success") {
        // console.log(res.data);
        this.setState({entries: res.data.data.bloglist});
        this.setState({entriesTotalCount: res.data.data.total_rows});
      }
    });
    this.setState({loading: false});
  }

  changeOrder = (event) => {
    this.setState({entries_order: event.target.value, currentPage: 1}, () => this._getBlogList());
  }

  changeCategory = (event) => {
    this.setState({entries_category: event.target.value, currentPage: 1}, () => this._getBlogList());
  }

  setCurrentPage = (event) => {
    if (event.target.text != null) {
      const value = event.target.text;
      const totalpage = Math.ceil(this.state.entriesTotalCount / this.state.entriesPerPage);
      const currentpage = this.state.currentPage;

      switch (value) {
        case "«First":
          this.setState({currentPage: 1}, () => this._getBlogList());
          break
        case "‹Previous":
          this.setState({currentPage: Math.max(parseInt(currentpage)-1, 1)}, () => this._getBlogList());
          break
        case "›Next":
          this.setState({currentPage: Math.min(parseInt(currentpage)+1, totalpage)}, () => this._getBlogList());
          break
        case "»Last":
          this.setState({currentPage: totalpage}, () => this._getBlogList());
          break
        default:
          this.setState({currentPage: event.target.text}, () => this._getBlogList());
      }
    }
  }

  render () {

    const entriesList = this.state.entries;

    const entriesViews = entriesList.map((item, index) => {
      const category = item.category;
      const link = item.link;
      const year = item.year;
      const month = item.month;
      const day = item.day;
      const author = item.post_author;
      const body = item.post_body;
      const id = item.post_id;
      const image = item.post_image;
      const meta_desc = item.post_meta_desc;
      const meta_keywords = item.post_meta_keywords;
      const publish_date = item.post_publish_date;
      const short_body = item.post_short_body;
      const slug = item.post_slug;
      const title = item.post_title;
      const updated_date = item.post_updated_date;

      return <BlogItem
        key={index}
        blog_category={category}
        blog_link={link}
        blog_year={year}
        blog_month={month}
        blog_day={day}
        blog_author={author}
        blog_body={body}
        blog_id={id}
        blog_imageUrl={image}
        blog_meta_desc={meta_desc}
        blog_meta_keywords={meta_keywords}
        blog_publish_date={publish_date}
        blog_short_body={short_body}
        blog_slug={slug}
        blog_title={title}
        blog_updated_date={updated_date}
      />
    });

    const currentPage = this.state.currentPage;
    const totalPage = Math.ceil(this.state.entriesTotalCount / this.state.entriesPerPage);
    const pageNumbers = [];
    const loading = this.state.loading;

    for (let i = Math.max(currentPage, 1); i <= Math.min(parseInt(currentPage)+2, totalPage); i ++) {
      pageNumbers.push(i);
    }

    return (
        <div>
          <div className="wrapper">
            <section className="pb-section slider-main">
              <Header/>
              <div className="banner-section-main blog-block">
                <div className="black-overlay">
                  <div className="container conclass">
                    <form name="form_latest_insights_top" className="" action="#" method="post">
                      <div className="row">
                        <div className="col-sm-8 text-center pt-4 blog-top-title">
                          <h1 className="pbh-2 text-white">Latest insights on Al, cybersecurity and women in tech</h1>
                          <p className="text-white">Provide your email address to receive insights straight to your
                            inbox</p>
                        </div>
                      </div>
                      <div className="row">
                        <div className="col-sm-8 inputrow mt-4">
                          <div className="col-sm-10 mail-box px-1">
                            <input type="hidden" name="page_type" value="blog"/>
                            <input type="hidden" name="type" value="latest-insights"/>
                            <input type="email" name="email" className="form-control br50" id="email"
                                   placeholder="Email Address"/>
                          </div>
                          <div className="col-sm-2 px-1 mail-button">
                            <button type="submit" className="emailbtn btn-block">Submit</button>
                          </div>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </section>
          </div>
          <section id="posts" className="p-1 pt-5 pb-5">
            <div className="container">
              <div className="row">
                <div className="col-12 text-center">
                  <h4 className="text-center">Our latest resources</h4>
                </div>
                <div className="col-12 post-news" style={{background: "#fff", marginTop: 50}}>
                  <div className="container">
                    <div className="row">
                      <div className="col-4 text-left order-section">
                        <select
                            name="order"
                            id="blog-order"
                            onChange={this.changeOrder}
                        >
                          <option value="recent">Recent</option>
                          <option value="oldest">Oldest</option>
                        </select>
                      </div>
                      <div className="col-4 text-center">
                        <h2 className="text-center m-b-40">All</h2>
                      </div>
                      <div className="col-4 text-right category-section">
                        <select
                            name="category"
                            id="blog-category"
                            onChange={this.changeCategory}
                        >
                          <option value="All">All</option>
                          <option value="Awards">Awards</option>
                          <option value="Blog">Blog</option>
                          <option value="Events">Events</option>
                          <option value="News">News</option>
                        </select>
                      </div>
                    </div>
                  </div>

                  <div className="row blog-row" style={{"marginTop":"20px", "textAlign":"center"}}>
                    {loading ? <h2>Loading...</h2> : entriesViews}
                  </div>
                </div>
              </div>
            </div>
          </section>

          {currentPage ? <div className="row" style={{margin: "auto"}}>
            <div className="pagging">
              <Pagination onClick={this.setCurrentPage}>
                <Pagination.First />
                <Pagination.Prev />
                {pageNumbers.map(number => (
                    <Pagination.Item key={number} active={number == currentPage}>{number}</Pagination.Item>
                ))}
                {<Pagination.Next />}
                {<Pagination.Last />}
              </Pagination>
            </div>
          </div> : <div />}

          <LatestInsides/>
          <Footer/>
        </div>
    )
  }
};

export default Blog;
