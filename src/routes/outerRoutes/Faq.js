import React, { useEffect } from "react";
import { NavLink, Link } from 'react-router-dom';
import Header from "../../components/Header";
import Footer from "./../../components/Footer";
import { Accordion, Card, Button } from "react-bootstrap";

const Faq = ({location}) => {
  useEffect(() => {
    document.title = "ProtectBox | FAQs"
    console.log("search", location.search);
  }, [])

  const CardGroupStyle = {
    fontWeight: "bold",
    margin: "2rem auto"
  }

  const CardStyle = {
    marginTop: "10px"
  }

  const CardHeaderStyle = {
    padding: "0.5rem",
    border: "1px solid #000",
    backgroundColor: "#fff"
  }

  const CardBodyStyle = {
    border: "1px solid #000"
  }

  return (
    <div id="load faqs">
      <section className="pb-section slider-main">
        <Header />
        <div className="banner-section-main faq-block">
					<div className="black-overlay">
							<div className="banner-text">
								<h1 className="pbh-2 white-color">Frequently Asked Questions (FAQs)</h1>
							</div>
					</div>
			</div>
      </section>

      <main className="faqs-page">
        <div className="container" style={{ "margin": "3rem auto"}}>
          <div className="row">
            <div className="col-10 col-md-offset-1 faq-row">
              <h3 className="text" style={{ ...CardGroupStyle }}>GENERAL FAQS</h3>
              <Accordion className="mainsearchdiv2">
                <div className="repeatrow" style={{ ...CardStyle }}>
                  <div className="qheading" style={{ ...CardHeaderStyle }}>
                    <Accordion.Toggle as={Link} variant="link" eventKey="1">
                      <div>
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse2" aria-expanded="false" className="collapsed" style={{ "fontSize": 16, "color": "#262626"}}>
                        What is ProtectBox?{" "}
                        </a>
                        <span className="spanarrow rarrow" style={{ "float": "right", "marginLeft": "5px", "color":"#000", "fontSize":"21px" }} >
                          <i className="fa fa-arrow-right icon" aria-hidden="true" />
                        </span>
                      </div>
                    </Accordion.Toggle>
                  </div>
                  <Accordion.Collapse eventKey="1">
                    <div style={{  ...CardBodyStyle }}>
                      <p className="p3">Award-winning cybersecurity comparison website/marketplace that lets any small & medium business anywhere in the world. In an hour for free, find & buy all their cybersecurity in 1-place whether they're technical or not & whether they're affected by COV-19 or not. We're also taking the pain away for Cybersecurity Suppliers by matching them to actual Sales quickly, simply & affordable.<br/><br/>We're also not just about improving the process but championing small & medium businesses to get fairer deals. Plus making the cybersecurity world more approachable for them by hiring those that wouldn't normally be seen as the norm for cybersecurity. We’re ‘less geek’ and as much a small & medium business, like them! <br/><br/>In the same way that banks sell insurance as an add-on, ProtectBox can be sold as an all-in-one cyber add-on by accountants, banks, insurers, any data service provider incl govt. By adding (on their website) a link to our website to buy from us, when they sell one of their products. Or advocate for us through sales, social media, client briefings. We build brand loyalty, increase market penetration into new areas & are CSR for partners. </p>
                    </div>
                  </Accordion.Collapse>
                </div>
                <div className="repeatrow" style={{ ...CardStyle }}>
                  <div className="qheading" style={{ ...CardHeaderStyle }}>
                    <Accordion.Toggle as={Link} variant="link" eventKey="2">
                      <div>
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse2" aria-expanded="false" className="collapsed" style={{ "fontSize": 16, "color": "#262626"}}>
                          Why are you called ProtectBox?
                        </a>
                        <span className="spanarrow rarrow" style={{ "float": "right", "marginLeft": "5px", "color":"#000", "fontSize":"21px" }} >
                          <i className="fa fa-arrow-right icon" aria-hidden="true" />
                        </span>
                      </div>
                    </Accordion.Toggle>
                  </div>
                  <Accordion.Collapse eventKey="2">
                    <div style={{  ...CardBodyStyle }}>
                      <p className="p3">We are Protection in a Box (or cybersecurity-as-a-service), whether that’s buying it, selling it, promoting it etc.</p>
                    </div>
                  </Accordion.Collapse>
                </div>
                <div className="repeatrow" style={{ ...CardStyle }}>
                  <div className="qheading" style={{ ...CardHeaderStyle }}>
                    <Accordion.Toggle as={Link} variant="link" eventKey="3">
                      <div>
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse2" aria-expanded="false" className="collapsed" style={{ "fontSize": 16, "color": "#262626"}}>Is ProtectBox for UK businesses only?</a>
                        <span className="spanarrow rarrow" style={{ "float": "right", "marginLeft": "5px", "color":"#000", "fontSize":"21px" }} >
                          <i className="fa fa-arrow-right icon" aria-hidden="true" />
                        </span>
                      </div>
                    </Accordion.Toggle>
                  </div>
                  <Accordion.Collapse eventKey="3">
                    <div style={{  ...CardBodyStyle }}>
                      <p className="p3">No, we are operating across the world, our website is available in multiple languages, and we are expanding our services all the time. Please sign up for our email updates and social media to keep you up to date.</p>
                    </div>
                  </Accordion.Collapse>
                </div>
                <div className="repeatrow" style={{ ...CardStyle }}>
                  <div className="qheading" style={{ ...CardHeaderStyle }}>
                    <Accordion.Toggle as={Link} variant="link" eventKey="4">
                      <div>
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse2" aria-expanded="false" className="collapsed" style={{ "fontSize": 16, "color": "#262626"}}>Customer Services/how do I contact you?</a>
                        <span className="spanarrow rarrow" style={{ "float": "right", "marginLeft": "5px", "color":"#000", "fontSize":"21px" }} >
                          <i className="fa fa-arrow-right icon" aria-hidden="true" />
                        </span>
                      </div>
                    </Accordion.Toggle>
                  </div>
                  <Accordion.Collapse eventKey="4">
                    <div style={{  ...CardBodyStyle }}>
                      <p className="p3">Chat to us using the blue chat icon in the bottom right corner. Or email us, as shown below (or call us on +44 (0)207 993 3037), our customer service team is available Mon-Fri 9am-5pm (UK time).<br/>
                                    team@protectbox.com (Businesses)<br/>
                                    supplier@protectbox.com (Suppliers)<br/>
                                    kiran@protectbox.com (Investors & Media)</p>
                    </div>
                  </Accordion.Collapse>
                </div>
              </Accordion>
              <h3 className="text" style={{ ...CardGroupStyle }}>SMALL BUSINESS FAQS</h3>
              <Accordion className="mainsearchdiv2">
                <div className="repeatrow" style={{ ...CardStyle }}>
                  <div className="qheading" style={{ ...CardHeaderStyle }}>
                    <Accordion.Toggle as={Link} variant="link" eventKey="5">
                      <div>
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse2" aria-expanded="false" className="collapsed" style={{ "fontSize": 16, "color": "#262626"}}>What’s the benefit of using ProtectBox for a small and medium business?</a>
                        <span className="spanarrow rarrow" style={{ "float": "right", "marginLeft": "5px", "color":"#000", "fontSize":"21px" }} >
                          <i className="fa fa-arrow-right icon" aria-hidden="true" />
                        </span>
                      </div>
                    </Accordion.Toggle>
                  </div>
                  <Accordion.Collapse eventKey="5">
                    <div style={{  ...CardBodyStyle }}>
                      <p className="p3">We make the whole process of finding & buying security quick, simple and affordable.<br/><br/>We’re a free service to help you find the right cybersecurity for your comparing. Searching for cybersecurity products and services can be confusing (lots of jargon!) and costly. ProtectBox uses your answers from a simple questionnaire to match your cybersecurity needs to up to six bundles of Suppliers. Each bundle gives you details on products listed under different categories, a cyber risk score for each category & prices per bundle. As with other comparison sites, ProtectBox lets you flex the bundles to get exactly the right one for you, with sliders that let you change according to budget or the risk score for each category, and automatically updates the whole bundle instantly. Then with 1-click you can pay for the whole bundle online, with lots of different ways to pay. Even being able to spread your payments out over 12 months (part of it interest-free) with an instant decision online through our website.  <br/><br/>We are a fairly priced marketplace. Founded in 2017, by CEO Miss Kiran Bhagotra after she left her Security role in the UK government’s Cabinet Office, who realised that businesses were spending huge amounts but also getting a bad deal when buying security. We do not think that is fair, which is why we are transparent (showing Supplier prices online, not just referring you to a Supplier who could then increase their price) and in-line with the market. We chose to offer you both in-direct and direct sales of Suppliers’ products/services so as to ensure the prices you see are market aligned.  <br/><br/>We're also not just about improving the process but also championing small & medium businesses to get fairer deals. 
                      Plus making the cybersecurity world more approachable for you by hiring those that wouldn't normally be seen as the norm for cybersecurity. We’re ‘less geek’ and as much a small & medium business, like you!</p>
                    </div>
                  </Accordion.Collapse>
                </div>
                <div className="repeatrow" style={{ ...CardStyle }}>
                  <div className="qheading" style={{ ...CardHeaderStyle }}>
                    <Accordion.Toggle as={Link} variant="link" eventKey="6">
                      <div>
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse2" aria-expanded="false" className="collapsed" style={{ "fontSize": 16, "color": "#262626"}}>Do I need to understand/know cybersecurity to use ProtectBox</a>
                        <span className="spanarrow rarrow" style={{ "float": "right", "marginLeft": "5px", "color":"#000", "fontSize":"21px" }} >
                          <i className="fa fa-arrow-right icon" aria-hidden="true" />
                        </span>
                      </div>
                    </Accordion.Toggle>
                  </div>
                  <Accordion.Collapse eventKey="6">
                    <div style={{  ...CardBodyStyle }}>
                      <p className="p3">No, we have designed ProtectBox for technical and non-technical small and medium businesses. In the questionnaire you need to complete, you can delegate each question to the most relevant team member (whether they’re in your company or an external provider, as it’s all done by email), and once all the questions are completed - our algorithm does the hard work for you and finds suitable products and services from the relevant Suppliers.</p>
                    </div>
                  </Accordion.Collapse>
                </div>
                <div className="repeatrow" style={{ ...CardStyle }}>
                  <div className="qheading" style={{ ...CardHeaderStyle }}>
                    <Accordion.Toggle as={Link} variant="link" eventKey="7">
                      <div>
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse2" aria-expanded="false" className="collapsed" style={{ "fontSize": 16, "color": "#262626"}}>Who can be a delegate user?</a>
                        <span className="spanarrow rarrow" style={{ "float": "right", "marginLeft": "5px", "color":"#000", "fontSize":"21px" }} >
                          <i className="fa fa-arrow-right icon" aria-hidden="true" />
                        </span>
                      </div>
                    </Accordion.Toggle>
                  </div>
                  <Accordion.Collapse eventKey="7">
                    <div style={{  ...CardBodyStyle }}>
                      <p className="p3">This can be both someone from your organisation or outside of your organisation. To setup a user, you will just need to provide their name and email address, ProtectBox will then provide the user access to relevant question(s) you would like them to assist with. The user will need to register with ProtectBox to complete the delegated questions.</p>
                    </div>
                  </Accordion.Collapse>
                </div>
                <div className="repeatrow" style={{ ...CardStyle }}>
                  <div className="qheading" style={{ ...CardHeaderStyle }}>
                    <Accordion.Toggle as={Link} variant="link" eventKey="8">
                      <div>
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse2" aria-expanded="false" className="collapsed" style={{ "fontSize": 16, "color": "#262626"}}>What access does a delegate user have?</a>
                        <span className="spanarrow rarrow" style={{ "float": "right", "marginLeft": "5px", "color":"#000", "fontSize":"21px" }} >
                          <i className="fa fa-arrow-right icon" aria-hidden="true" />
                        </span>
                      </div>
                    </Accordion.Toggle>
                  </div>
                  <Accordion.Collapse eventKey="8">
                    <div style={{  ...CardBodyStyle }}>
                      <p className="p3">You will determine the level of access of each delegate user. Normally this would just be the assigned questions and answers they have provided.</p>
                    </div>
                  </Accordion.Collapse>
                </div>
                <div className="repeatrow" style={{ ...CardStyle }}>
                  <div className="qheading" style={{ ...CardHeaderStyle }}>
                    <Accordion.Toggle as={Link} variant="link" eventKey="9">
                      <div>
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse2" aria-expanded="false" className="collapsed" style={{ "fontSize": 16, "color": "#262626"}}>Will I be notified once a delegate has completed their question(s)?</a>
                        <span className="spanarrow rarrow" style={{ "float": "right", "marginLeft": "5px", "color":"#000", "fontSize":"21px" }} >
                          <i className="fa fa-arrow-right icon" aria-hidden="true" />
                        </span>
                      </div>
                    </Accordion.Toggle>
                  </div>
                  <Accordion.Collapse eventKey="9">
                    <div style={{  ...CardBodyStyle }}>
                      <p className="p3">Yes, you will receive an email notification, then once logged into ProtectBox you will be able to view their answer.</p>
                    </div>
                  </Accordion.Collapse>
                </div>
                <div className="repeatrow" style={{ ...CardStyle }}>
                  <div className="qheading" style={{ ...CardHeaderStyle }}>
                    <Accordion.Toggle as={Link} variant="link" eventKey="10">
                      <div>
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse2" aria-expanded="false" className="collapsed" style={{ "fontSize": 16, "color": "#262626"}}>Can I delegate the same question to more than one person?</a>
                        <span className="spanarrow rarrow" style={{ "float": "right", "marginLeft": "5px", "color":"#000", "fontSize":"21px" }} >
                          <i className="fa fa-arrow-right icon" aria-hidden="true" />
                        </span>
                      </div>
                    </Accordion.Toggle>
                  </div>
                  <Accordion.Collapse eventKey="10">
                    <div style={{  ...CardBodyStyle }}>
                      <p className="p3">Yes, you will be able to see the names of the delegates under each question.</p>
                    </div>
                  </Accordion.Collapse>
                </div>
                <div className="repeatrow" style={{ ...CardStyle }}>
                  <div className="qheading" style={{ ...CardHeaderStyle }}>
                    <Accordion.Toggle as={Link} variant="link" eventKey="11">
                      <div>
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse2" aria-expanded="false" className="collapsed" style={{ "fontSize": 16, "color": "#262626"}}>What if a delegate has not completed their question?</a>
                        <span className="spanarrow rarrow" style={{ "float": "right", "marginLeft": "5px", "color":"#000", "fontSize":"21px" }} >
                          <i className="fa fa-arrow-right icon" aria-hidden="true" />
                        </span>
                      </div>
                    </Accordion.Toggle>
                  </div>
                  <Accordion.Collapse eventKey="11">
                    <div style={{  ...CardBodyStyle }}>
                      <p className="p3">You are able to send them a reminder from ProtectBox. Navigate to 'Account' and select 'Delegates' from here you are able to send reminders.</p>
                    </div>
                  </Accordion.Collapse>
                </div>
                <div className="repeatrow" style={{ ...CardStyle }}>
                  <div className="qheading" style={{ ...CardHeaderStyle }}>
                    <Accordion.Toggle as={Link} variant="link" eventKey="12">
                      <div>
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse2" aria-expanded="false" className="collapsed" style={{ "fontSize": 16, "color": "#262626"}}>How can I use my www.Sage.com account to help me with ProtectBox?</a>
                        <span className="spanarrow rarrow" style={{ "float": "right", "marginLeft": "5px", "color":"#000", "fontSize":"21px" }} >
                          <i className="fa fa-arrow-right icon" aria-hidden="true" />
                        </span>
                      </div>
                    </Accordion.Toggle>
                  </div>
                  <Accordion.Collapse eventKey="12">
                    <div style={{  ...CardBodyStyle }}>
                      <p className="p3">You can use your www.Sage.com account to help answer ProtectBox’s questionnaire. After you have provided permission, we are able to search through all your Sage invoices for answers to our questions, and then show you the answers we’ve found (i.e. what you have purchased and when, that match our questions) under our questions online. You can then use that info to pick the right answer. </p>
                    </div>
                  </Accordion.Collapse>
                </div>
                <div className="repeatrow" style={{ ...CardStyle }}>
                  <div className="qheading" style={{ ...CardHeaderStyle }}>
                    <Accordion.Toggle as={Link} variant="link" eventKey="13">
                      <div>
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse2" aria-expanded="false" className="collapsed" style={{ "fontSize": 16, "color": "#262626"}}>Can I import my data from other online accounting, banking, insurance, legal, government etc services too?</a>
                        <span className="spanarrow rarrow" style={{ "float": "right", "marginLeft": "5px", "color":"#000", "fontSize":"21px" }} >
                          <i className="fa fa-arrow-right icon" aria-hidden="true" />
                        </span>
                      </div>
                    </Accordion.Toggle>
                  </div>
                  <Accordion.Collapse eventKey="13">
                    <div style={{  ...CardBodyStyle }}>
                      <p className="p3">Yes, you can see a button for all these partners at the top of your questionnaire when you log in. There is also a list of our partners on our ‘About’ page. We are adding to these all the time. Please sign up for our email updates and social media to keep you up to date.</p>
                    </div>
                  </Accordion.Collapse>
                </div>
                <div className="repeatrow" style={{ ...CardStyle }}>
                  <div className="qheading" style={{ ...CardHeaderStyle }}>
                    <Accordion.Toggle as={Link} variant="link" eventKey="14">
                      <div>
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse2" aria-expanded="false" className="collapsed" style={{ "fontSize": 16, "color": "#262626"}}>What if I have a provider who isn’t listed that I would like to use through ProtectBox?</a>
                        <span className="spanarrow rarrow" style={{ "float": "right", "marginLeft": "5px", "color":"#000", "fontSize":"21px" }} >
                          <i className="fa fa-arrow-right icon" aria-hidden="true" />
                        </span>
                      </div>
                    </Accordion.Toggle>
                  </div>
                  <Accordion.Collapse eventKey="14">
                    <div style={{  ...CardBodyStyle }}>
                      <p className="p3">We can only offer this service if the relevant partner lets us do so. If you’d like a data service provider you use to offer this service through us, we’d appreciate your support in asking them to work with us.  </p>
                    </div>
                  </Accordion.Collapse>
                </div>
                <div className="repeatrow" style={{ ...CardStyle }}>
                  <div className="qheading" style={{ ...CardHeaderStyle }}>
                    <Accordion.Toggle as={Link} variant="link" eventKey="15">
                      <div>
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse2" aria-expanded="false" className="collapsed" style={{ "fontSize": 16, "color": "#262626"}}>Can I import from multiple partners at the same time?</a>
                        <span className="spanarrow rarrow" style={{ "float": "right", "marginLeft": "5px", "color":"#000", "fontSize":"21px" }} >
                          <i className="fa fa-arrow-right icon" aria-hidden="true" />
                        </span>
                      </div>
                    </Accordion.Toggle>
                  </div>
                  <Accordion.Collapse eventKey="15">
                    <div style={{  ...CardBodyStyle }}>
                      <p className="p3">Yes, you can.</p>
                    </div>
                  </Accordion.Collapse>
                </div>
                <div className="repeatrow" style={{ ...CardStyle }}>
                  <div className="qheading" style={{ ...CardHeaderStyle }}>
                    <Accordion.Toggle as={Link} variant="link" eventKey="16">
                      <div>
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse2" aria-expanded="false" className="collapsed" style={{ "fontSize": 16, "color": "#262626"}}>Is it safe to pay by card on ProtectBox?</a>
                        <span className="spanarrow rarrow" style={{ "float": "right", "marginLeft": "5px", "color":"#000", "fontSize":"21px" }} >
                          <i className="fa fa-arrow-right icon" aria-hidden="true" />
                        </span>
                      </div>
                    </Accordion.Toggle>
                  </div>
                  <Accordion.Collapse eventKey="16">
                    <div style={{  ...CardBodyStyle }}>
                      <p className="p3">At ProtectBox we make sure that your card information is secured. We do not save any card information, during the payment process you will be redirected to a secure third party payment gateway which is 100% secured when processing the payment and PCI/DSS compliant. </p>
                    </div>
                  </Accordion.Collapse>
                </div>
                <div className="repeatrow" style={{ ...CardStyle }}>
                  <div className="qheading" style={{ ...CardHeaderStyle }}>
                    <Accordion.Toggle as={Link} variant="link" eventKey="17">
                      <div>
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse2" aria-expanded="false" className="collapsed" style={{ "fontSize": 16, "color": "#262626"}}>Can I add/remove items from my order?</a>
                        <span className="spanarrow rarrow" style={{ "float": "right", "marginLeft": "5px", "color":"#000", "fontSize":"21px" }} >
                          <i className="fa fa-arrow-right icon" aria-hidden="true" />
                        </span>
                      </div>
                    </Accordion.Toggle>
                  </div>
                  <Accordion.Collapse eventKey="17">
                    <div style={{  ...CardBodyStyle }}>
                      <p className="p3">You can amend the order during the selection process, using the comparison tool filters whilst evaluating the products and services. Once you have placed an order online, you will not be able to amend the items ordered. However, if you would like to cancel your order, dependent on the T&C's of that product and service, you may be able to receive a refund and have that order cancelled.  </p>
                    </div>
                  </Accordion.Collapse>
                </div>
                <div className="repeatrow" style={{ ...CardStyle }}>
                  <div className="qheading" style={{ ...CardHeaderStyle }}>
                    <Accordion.Toggle as={Link} variant="link" eventKey="18">
                      <div>
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse2" aria-expanded="false" className="collapsed" style={{ "fontSize": 16, "color": "#262626"}}>If I have requested a refund, when will I receive this?</a>
                        <span className="spanarrow rarrow" style={{ "float": "right", "marginLeft": "5px", "color":"#000", "fontSize":"21px" }} >
                          <i className="fa fa-arrow-right icon" aria-hidden="true" />
                        </span>
                      </div>
                    </Accordion.Toggle>
                  </div>
                  <Accordion.Collapse eventKey="18">
                    <div style={{  ...CardBodyStyle }}>
                      <p className="p3">This will depend on the supplier from whom you’re getting the refund and their T&C's. The best place to find all this information is in your ‘Account’ in the ‘Orders’ section. You can perform all actions from there and all updates can be seen there too. If you have any other questions, you can contact us too. See “Customer Services/how do I contact you?” above.   </p>
                    </div>
                  </Accordion.Collapse>
                </div>
                <div className="repeatrow" style={{ ...CardStyle }}>
                  <div className="qheading" style={{ ...CardHeaderStyle }}>
                    <Accordion.Toggle as={Link} variant="link" eventKey="19">
                      <div>
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse2" aria-expanded="false" className="collapsed" style={{ "fontSize": 16, "color": "#262626"}}>What is the ProtectBox subscription service?</a>
                        <span className="spanarrow rarrow" style={{ "float": "right", "marginLeft": "5px", "color":"#000", "fontSize":"21px" }} >
                          <i className="fa fa-arrow-right icon" aria-hidden="true" />
                        </span>
                      </div>
                    </Accordion.Toggle>
                  </div>
                  <Accordion.Collapse eventKey="19">
                    <div style={{  ...CardBodyStyle }}>
                      <p className="p3">Our subscription is a small admin charge for ongoing access to our comparison tool so you can store and update your details/answers, place more orders & track your orders and keep up to date with the latest news on ProtectBox and the cybersecurity market as a whole. If you don’t buy our subscription, then you will have all the info about your orders in your emails but your questionnaire won’t be saved. The benefit of the subscription is that all the info will be in the 1 easy to see page alongwith extras such as market news, best buys & more.  </p>
                    </div>
                  </Accordion.Collapse>
                </div>
                <div className="repeatrow" style={{ ...CardStyle }}>
                  <div className="qheading" style={{ ...CardHeaderStyle }}>
                    <Accordion.Toggle as={Link} variant="link" eventKey="20">
                      <div>
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse2" aria-expanded="false" className="collapsed" style={{ "fontSize": 16, "color": "#262626"}}>What does the ProtectBox subscription service cost?</a>
                        <span className="spanarrow rarrow" style={{ "float": "right", "marginLeft": "5px", "color":"#000", "fontSize":"21px" }} >
                          <i className="fa fa-arrow-right icon" aria-hidden="true" />
                        </span>
                      </div>
                    </Accordion.Toggle>
                  </div>
                  <Accordion.Collapse eventKey="20">
                    <div style={{  ...CardBodyStyle }}>
                      <p className="p3">It varies according to the size of your company and we show you the amount at the end of your order process. Or you can access it under ‘Accounts’. But it is a very fair price, starting at less than a price of coffee (per month).   </p>
                    </div>
                  </Accordion.Collapse>
                </div>
              </Accordion>
              <h3 className="text" style={{ ...CardGroupStyle }}>SUPPLIER FAQS</h3>
              <Accordion className="mainsearchdiv2">
                <div className="repeatrow" style={{ ...CardStyle }}>
                  <div className="qheading" style={{ ...CardHeaderStyle }}>
                    <Accordion.Toggle as={Link} variant="link" eventKey="21">
                      <div>
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse2" aria-expanded="false" className="collapsed" style={{ "fontSize": 16, "color": "#262626"}}>
                        What’s the benefit of using ProtectBox for a Supplier?
                        </a>
                        <span className="spanarrow rarrow" style={{ "float": "right", "marginLeft": "5px", "color":"#000", "fontSize":"21px" }} >
                          <i className="fa fa-arrow-right icon" aria-hidden="true" />
                        </span>
                      </div>
                    </Accordion.Toggle>
                  </div>
                  <Accordion.Collapse eventKey="21">
                    <div style={{  ...CardBodyStyle }}>
                      <p className="p3">We make the whole process of finding & buying security quick, simple and affordable.<br/><br/>You can add individual products/services &/or bundles of them. You can also choose which locations you wish to sell which of your products/services. We may already be selling some of your products/services through direct feeds from re-sellers/distributors, which you’ll see listed on your page. Feel free to contact us to discuss how you would like to manage the mix of sales through re-sellers/distributors and directly through us. See “Customer Services/how do I contact you?” above. You can use your ProtectBox account to track/manage your Sales better, whether that be for all of your products/services or just some of them. <br/><br/>We are a fairly priced marketplace. We are aware of the wide-ranging figures asked of Suppliers to promote, sell or re-sell their cybersecurity products/services by our peers. We do not think that is fair, which is why we are transparent about our fixed commissions that are also in-line with the market. This is why we chose to offer both in-direct and direct sales options to you. </p>
                    </div>
                  </Accordion.Collapse>
                </div>
                <div className="repeatrow" style={{ ...CardStyle }}>
                  <div className="qheading" style={{ ...CardHeaderStyle }}>
                    <Accordion.Toggle as={Link} variant="link" eventKey="22">
                      <div>
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse2" aria-expanded="false" className="collapsed" style={{ "fontSize": 16, "color": "#262626"}}>How do I add my products and services? </a>
                        <span className="spanarrow rarrow" style={{ "float": "right", "marginLeft": "5px", "color":"#000", "fontSize":"21px" }} >
                          <i className="fa fa-arrow-right icon" aria-hidden="true" />
                        </span>
                      </div>
                    </Accordion.Toggle>
                  </div>
                  <Accordion.Collapse eventKey="22">
                    <div style={{  ...CardBodyStyle }}>
                      <p className="p3">Once you have registered you will be able to add the products and services you provide. In the overview section, ensure you select all the categories that apply to your product and services and complete the remaining questions. Then move onto the services section, this section is where you will add your products and services with the relevant details. You can add individual products/services and/or bundles of them. You can also choose which locations you wish to sell which of your products/services. We may already be selling some of your products/services through direct feeds from re-sellers/distributors, which you’ll see listed on your page. Feel free to contact us to discuss how best to manage the mix of sales through re-sellers/distributors and directly through us. See “Customer Services/how do I contact you?” above. </p>
                    </div>
                  </Accordion.Collapse>
                </div>
                <div className="repeatrow" style={{ ...CardStyle }}>
                  <div className="qheading" style={{ ...CardHeaderStyle }}>
                    <Accordion.Toggle as={Link} variant="link" eventKey="23">
                      <div>
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse2" aria-expanded="false" className="collapsed" style={{ "fontSize": 16, "color": "#262626"}}>How can I be sure about which category fits better for my service or services? </a>
                        <span className="spanarrow rarrow" style={{ "float": "right", "marginLeft": "5px", "color":"#000", "fontSize":"21px" }} >
                          <i className="fa fa-arrow-right icon" aria-hidden="true" />
                        </span>
                      </div>
                    </Accordion.Toggle>
                  </div>
                  <Accordion.Collapse eventKey="23">
                    <div style={{  ...CardBodyStyle }}>
                      <p className="p3">To ensure ease of use for the business customer, we have limited the categories available. If there isn’t an exact category, we would recommend choosing the closest fit to ensure your service is accessible to potential customers. Feel free to discuss with us how best to do this for you. See “Customer Services/how do I contact you?” above.</p>
                    </div>
                  </Accordion.Collapse>
                </div>
                <div className="repeatrow" style={{ ...CardStyle }}>
                  <div className="qheading" style={{ ...CardHeaderStyle }}>
                    <Accordion.Toggle as={Link} variant="link" eventKey="24">
                      <div>
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse2" aria-expanded="false" className="collapsed" style={{ "fontSize": 16, "color": "#262626"}}>How are my products and services matched to a potential SMB customer?</a>
                        <span className="spanarrow rarrow" style={{ "float": "right", "marginLeft": "5px", "color":"#000", "fontSize":"21px" }} >
                          <i className="fa fa-arrow-right icon" aria-hidden="true" />
                        </span>
                      </div>
                    </Accordion.Toggle>
                  </div>
                  <Accordion.Collapse eventKey="24">
                    <div style={{  ...CardBodyStyle }}>
                      <p className="p3">Your products and services are matched with a potential SMB customer by the customer completing a questionnaire and using this information, matched via our algorithm with your product and services. We are not the same as insurance comparison websites, who are authorised/regulated advisors. As cybersecurity is constantly evolving regulations make the small and medium business ultimately responsible and accountable for their cybersecurity decisions. Based on the answers the businesses provide, we are helping make that decision-making & buying process easier for them.</p>
                    </div>
                  </Accordion.Collapse>
                </div>
                <div className="repeatrow" style={{ ...CardStyle }}>
                  <div className="qheading" style={{ ...CardHeaderStyle }}>
                    <Accordion.Toggle as={Link} variant="link" eventKey="25">
                      <div>
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse2" aria-expanded="false" className="collapsed" style={{ "fontSize": 16, "color": "#262626"}}>What is the associated cost for me as a supplier?</a>
                        <span className="spanarrow rarrow" style={{ "float": "right", "marginLeft": "5px", "color":"#000", "fontSize":"21px" }} >
                          <i className="fa fa-arrow-right icon" aria-hidden="true" />
                        </span>
                      </div>
                    </Accordion.Toggle>
                  </div>
                  <Accordion.Collapse eventKey="25">
                    <div style={{  ...CardBodyStyle }}>
                      <p className="p3">As a Supplier, you will pay us a fee, which you select when you first sign up your product/service with us. This is a Re-seller or Affiliate type fee, so a percentage of your total product price sold. We only take our fee when an actual sale is made. We process payment for your product sold, online through ProtectBox when the Small Business pays for your product online with us & we process our fee. We will send you an email notification each time one of your products is sold. You can also pay for a Subscription service for continuous access to info / analytics on all your sales.</p>
                    </div>
                  </Accordion.Collapse>
                </div>
                <div className="repeatrow" style={{ ...CardStyle }}>
                  <div className="qheading" style={{ ...CardHeaderStyle }}>
                    <Accordion.Toggle as={Link} variant="link" eventKey="26">
                      <div>
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse2" aria-expanded="false" className="collapsed" style={{ "fontSize": 16, "color": "#262626"}}>How do I know that my product is sold?</a>
                        <span className="spanarrow rarrow" style={{ "float": "right", "marginLeft": "5px", "color":"#000", "fontSize":"21px" }} >
                          <i className="fa fa-arrow-right icon" aria-hidden="true" />
                        </span>
                      </div>
                    </Accordion.Toggle>
                  </div>
                  <Accordion.Collapse eventKey="26">
                    <div style={{  ...CardBodyStyle }}>
                      <p className="p3">We will send you an email notification each time one of your products is sold. You can also pay for a Subscription service for continuous access to info / analytics on all your sales.omer?</p>
                    </div>
                  </Accordion.Collapse>
                </div>
                <div className="repeatrow" style={{ ...CardStyle }}>
                  <div className="qheading" style={{ ...CardHeaderStyle }}>
                    <Accordion.Toggle as={Link} variant="link" eventKey="27">
                      <div>
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse2" aria-expanded="false" className="collapsed" style={{ "fontSize": 16, "color": "#262626"}}>With the email alert, will it also show me which other suppliers I am bundled with?</a>
                        <span className="spanarrow rarrow" style={{ "float": "right", "marginLeft": "5px", "color":"#000", "fontSize":"21px" }} >
                          <i className="fa fa-arrow-right icon" aria-hidden="true" />
                        </span>
                      </div>
                    </Accordion.Toggle>
                  </div>
                  <Accordion.Collapse eventKey="27">
                    <div style={{  ...CardBodyStyle }}>
                      <p className="p3">No, you will not have access to any other suppliers’ information. With the subscription sales analytics for your product categories (a feature which is coming soon) our data is anonymised and encrypted. This data and subscription information would enable you to perform ‘go to market’ research for your products/services.</p>
                    </div>
                  </Accordion.Collapse>
                </div>
                <div className="repeatrow" style={{ ...CardStyle }}>
                  <div className="qheading" style={{ ...CardHeaderStyle }}>
                    <Accordion.Toggle as={Link} variant="link" eventKey="28">
                      <div>
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse2" aria-expanded="false" className="collapsed" style={{ "fontSize": 16, "color": "#262626"}}>What is the ProtectBox subscription service offered to me as a supplier?</a>
                        <span className="spanarrow rarrow" style={{ "float": "right", "marginLeft": "5px", "color":"#000", "fontSize":"21px" }} >
                          <i className="fa fa-arrow-right icon" aria-hidden="true" />
                        </span>
                      </div>
                    </Accordion.Toggle>
                  </div>
                  <Accordion.Collapse eventKey="28">
                    <div style={{  ...CardBodyStyle }}>
                      <p className="p3">Our Subscription service offers continuous access to info/analytics on all your sales (similar to Salesforce) & keep up to date with the latest news on ProtectBox and the cybersecurity market. We will be adding new features (such as seeing geographical demand for your products) and welcome your feedback on what you’d like to see. </p>
                    </div>
                  </Accordion.Collapse>
                </div>
                <div className="repeatrow" style={{ ...CardStyle }}>
                  <div className="qheading" style={{ ...CardHeaderStyle }}>
                    <Accordion.Toggle as={Link} variant="link" eventKey="29">
                      <div>
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse2" aria-expanded="false" className="collapsed" style={{ "fontSize": 16, "color": "#262626"}}>What is the subscription fee cost?</a>
                        <span className="spanarrow rarrow" style={{ "float": "right", "marginLeft": "5px", "color":"#000", "fontSize":"21px" }} >
                          <i className="fa fa-arrow-right icon" aria-hidden="true" />
                        </span>
                      </div>
                    </Accordion.Toggle>
                  </div>
                  <Accordion.Collapse eventKey="29">
                    <div style={{  ...CardBodyStyle }}>
                      <p className="p3">It varies according to the size of your company and we show you the amount for your company size online under ‘Accounts’. But it is a very fair price, starting at less than a price of coffee (per month). Unlike some of our peers.</p>
                    </div>
                  </Accordion.Collapse>
                </div>
                <div className="repeatrow" style={{ ...CardStyle }}>
                  <div className="qheading" style={{ ...CardHeaderStyle }}>
                    <Accordion.Toggle as={Link} variant="link" eventKey="30">
                      <div>
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse2" aria-expanded="false" className="collapsed" style={{ "fontSize": 16, "color": "#262626"}}>How can I subscribe for the ProtectBox Subscription service?</a>
                        <span className="spanarrow rarrow" style={{ "float": "right", "marginLeft": "5px", "color":"#000", "fontSize":"21px" }} >
                          <i className="fa fa-arrow-right icon" aria-hidden="true" />
                        </span>
                      </div>
                    </Accordion.Toggle>
                  </div>
                  <Accordion.Collapse eventKey="30">
                    <div style={{  ...CardBodyStyle }}>
                      <p className="p3">Log in, click on “Sales” in “Account” Click on link at the end of “Solutions”. 
    You will need to have uploaded a Product/Service to enable us to ensure you receive relevant information.</p>
                    </div>
                  </Accordion.Collapse>
                </div>
                <div className="repeatrow" style={{ ...CardStyle }}>
                  <div className="qheading" style={{ ...CardHeaderStyle }}>
                    <Accordion.Toggle as={Link} variant="link" eventKey="31">
                      <div>
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse2" aria-expanded="false" className="collapsed" style={{ "fontSize": 16, "color": "#262626"}}>On my sales dashboard, can I separate between each product and location?</a>
                        <span className="spanarrow rarrow" style={{ "float": "right", "marginLeft": "5px", "color":"#000", "fontSize":"21px" }} >
                          <i className="fa fa-arrow-right icon" aria-hidden="true" />
                        </span>
                      </div>
                    </Accordion.Toggle>
                  </div>
                  <Accordion.Collapse eventKey="31">
                    <div style={{  ...CardBodyStyle }}>
                      <p className="p3">Yes, remember to include your product and services in all relevant categories to make the most of the sales dashboard. By adding all locations, you will be able to see who is interested from different markets in which particular services/products. We recommend widening your range of categories to get the most from our sales analytics.</p>
                    </div>
                  </Accordion.Collapse>
                </div>
              </Accordion>
            </div>
          </div>
        </div>
      </main>
      <Footer />
    </div>
  );
};

export default Faq;
