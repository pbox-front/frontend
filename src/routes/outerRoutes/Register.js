import React, { useEffect } from "react";
import ReCAPTCHA from "react-google-recaptcha";
import Footer from "./../../components/Footer";
import Header from "./../../components/Header";
import { Link } from "react-router-dom";
import axios from "axios";
import LoaderGif from '../../images2/favicon.gif';
import { Alert } from "react-bootstrap";


class Register extends React.Component {
  constructor() {
    super();
    this.state = {

    };
    this.onCaptchaChange = this.onCaptchaChange.bind(this);
    this.onFormChange = this.onFormChange.bind(this);
    this.onRegister = this.onRegister.bind(this);
    this.checkBoxChanged = this.checkBoxChanged.bind(this);

    this.accountTypeRef = React.createRef();
    this.companyNameRef = React.createRef();
    this.firstNameRef = React.createRef();
    this.lastNameRef = React.createRef();
    this.emailIdRef = React.createRef();
    this.confirmEmailIdRef = React.createRef();
    this.passwordRef = React.createRef();
    this.repeatPasswordRef = React.createRef();
    this.delegateUserRef = React.createRef();
  }

  componentDidMount() {
    document.title = "ProtectBox | Registration";
  }

  onCaptchaChange(val) {
    this.setState({ captcha: val })
  }

  onFormChange(event) {
    this.setState({ [event.target.name]: event.target.value });
  }

  checkBoxChanged(event) {
    this.setState({ [event.target.name]: event.target.checked });
  }

  onRegister() {

    const { account_type, company_name, first_name, last_name, email_id, password,
      delegate_status, delegate_email, receive_email, terms_condition, captcha,
      confirm_email, repeat_password } = this.state
    this.setState({ formSubmitted: true });
    if (!account_type) {
      this.accountTypeRef.current.focus();
      return;
    }
    if (!company_name) {
      this.companyNameRef.current.focus();
      return;
    }
    if (!first_name) {
      this.firstNameRef.current.focus();
      return;
    }
    if (!last_name) {
      this.lastNameRef.current.focus();
      return;
    }
    if (!email_id) {
      this.emailIdRef.current.focus();
      return;
    }
    if (!confirm_email) {
      this.confirmEmailIdRef.current.focus();
      return;
    }
    if (!password) {
      this.passwordRef.current.focus();
      return;
    }
    if (!repeat_password) {
      this.repeatPasswordRef.current.focus();
      return;
    }
    if (email_id !== confirm_email) {
      this.confirmEmailIdRef.current.focus();
      return
    }
    if (password !== repeat_password) {
      this.repeatPasswordRef.current.focus();
      return
    }
    if (delegate_status && !delegate_email) {
      this.delegateUserRef.current.focus();
      return;
    }

    if (!(terms_condition && captcha)) {
      return;
    }

    const formData = new FormData();
    formData.append('account_type', account_type)
    formData.append('company_name', company_name)
    formData.append('first_name', first_name)
    formData.append('last_name', last_name)
    formData.append('key', '')
    formData.append('email_id', email_id)
    formData.append('password', password)
    formData.append('delegate_email', delegate_email)
    formData.append('receive_email', receive_email ? 'on' : 'off')

    this.setState({ callingServer: true })
    axios.post('www/api/registerapi', formData).then((res) => {
      if (res.data.message === "Success") {
        this.setState({ callingServer: false }, () => {
          this.props.history.push(`/${res.data.redirect}`);
        })
      }
    }).catch(error => {
      console.log(error);
      if (error && error.response && error.response.data) {
        this.setState({ registerErrorMessage: error.response.data.message, callingServer: false })
      }
    })
  }

  render() {
    const { account_type = '', company_name = '', first_name = '', last_name = '',
      email_id = '', confirm_email = '', password = '', repeat_password = '', delegate_email = '', delegate_status,
      callingServer = false, formSubmitted = false, registerErrorMessage } = this.state;
    return (
      <div>
        <div id="load" />

        <section className="pb-section slider-main">
          <Header />
          <div className="banner-section-main register-block">
            <div className="black-overlay">
              <div className="banner-text">
                <h1 className="pbh-2 white-color">Registration</h1>
              </div>
            </div>
          </div>
        </section>

        <main className="register">
          <div className="container margin_60">
            <div className="row">
              {!callingServer && <div
                className="col-md-6 col-md-offset-3"
                style={{
                  padding: "10px 15px 10px",
                  border: "1px solid #CCC",
                  boxShadow: "0px 0px 3px #bfbfbf",
                  borderRadius: 5,
                  background: "#f5f5f5",
                  margin: "auto",
                }}
                id="login-form"
              >
                {registerErrorMessage && <Alert variant={'danger'}>
                  <strong>{registerErrorMessage}</strong>
                </Alert>
                }
                <div className="another">
                  <div className="row">
                    <div className="col-md-12">
                      <p style={{ "color": "#ec8b0d", "fontSize": "14px", "fontWeight": "bold" }}>If you are a <a href="https://www.sage.com/" target="_blank">Sage</a> or <a href="https://www.xero.com/" target="_blank">Xero</a> customer, please register here (on ProtectBox) first before logging in as a ProtectBox user. Then press the <a href="https://www.sage.com/" target="_blank">Sage</a> or <a href="https://www.xero.com/" target="_blank">Xero</a> button on the questionnaire to authorise us to pull your <a href="https://www.sage.com/" target="_blank">Sage</a> or <a href="https://www.xero.com/" target="_blank">Xero</a> data into our questionnaire.</p>
                    </div>
                  </div>
                </div>

                <div className="row" style={{ "marginTop": "20px" }}>
                  <div className="col-md-12 col-sm-12">
                    <div className="form-group">
                      <label>Account Type <span style={{ "color": "red", "fontSize": "20px" }}>*</span></label>
                      <select className="form-control" name="account_type" id="acc_type"
                        ref={this.accountTypeRef} value={account_type} onChange={this.onFormChange}>
                        <option selected="" disabled="">Select account type</option>
                        <option value="Small and medium business" >Small and medium business</option>
                        <option value="Supplier">Supplier</option>
                      </select>
                      {formSubmitted && !account_type && <div>This field is required</div>}
                    </div>
                  </div>
                </div>

                <div className="row">
                  <div className="col-md-12 col-sm-12">
                    <div className="form-group">
                      <label>Company Name <span style={{ "color": "red", "fontSize": "20px" }}>*</span></label>
                      <input type="text" className="form-control"
                        name="company_name" maxLength={60}
                        ref={this.companyNameRef}
                        value={company_name} onChange={this.onFormChange} />
                      {formSubmitted && !company_name && <div>This field is required</div>}
                    </div>
                  </div>
                </div>

                <div className="row">
                  <div className="col-md-6 col-sm-6">
                    <div className="form-group">
                      <label>First Name <span style={{ "color": "red", "fontSize": "20px" }}>*</span></label>
                      <input type="text" className="form-control" name="first_name"
                        value={first_name} maxLength={30}
                        ref={this.firstNameRef}
                        onChange={this.onFormChange} />
                      {formSubmitted && !first_name && <div>This field is required</div>}
                    </div>
                  </div>
                  <div className="col-md-6 col-sm-6">
                    <div className="form-group">
                      <label>Last Name <span style={{ "color": "red", "fontSize": "20px" }}>*</span></label>
                      <input type="text" className="form-control" name="last_name"
                        ref={this.lastNameRef} value={last_name} maxLength={30} onChange={this.onFormChange} />
                      {formSubmitted && !last_name && <div>This field is required</div>}
                    </div>
                  </div>
                </div>

                <div className="row">
                  <div className="col-md-6 col-sm-6">
                    <div className="form-group">
                      <label>Email Address <span style={{ "color": "red", "fontSize": "20px" }}>*</span></label>
                      <input type="email" className="form-control email1" name="email_id"
                        ref={this.emailIdRef}
                        value={email_id} onChange={this.onFormChange} />
                      {formSubmitted && !email_id && <div>This field is required</div>}
                    </div>
                  </div>
                  <div className="col-md-6 col-sm-6">
                    <div className="form-group">
                      <label>Confirm Email Address <span style={{ "color": "red", "fontSize": "20px" }}>*</span></label>
                      <input type="email" className="form-control email2" name="confirm_email"
                        ref={this.confirmEmailIdRef}
                        value={confirm_email} onChange={this.onFormChange} />
                      {email_id && confirm_email && email_id !== confirm_email && <small className="email_error" style={{ "color": "red" }}>Email doesn't match!</small>}
                      {formSubmitted && !confirm_email && <div>This field is required</div>}
                    </div>
                  </div>
                </div>

                <div className="row">
                  <div className="col-md-6 col-sm-6">
                    <div className="form-group">
                      <label>Password <span style={{ "color": "red", "fontSize": "20px" }}>*</span></label>
                      <input type="password" className="form-control pass1" name="password" id="myPassword"
                        ref={this.passwordRef}
                        value={password} onChange={this.onFormChange} />
                      <span toggle="#myPassword" className="icon-eye field-icon toggle-password"></span>
                      {formSubmitted && !password && <div>This field is required</div>}
                    </div>
                  </div>
                  <div className="col-md-6 col-sm-6">
                    <div className="form-group">
                      <label>Repeat Password <span style={{ "color": "red", "fontSize": "20px" }}>*</span></label>
                      <input type="password" className="form-control pass2" name="repeat_password"
                        ref={this.repeatPasswordRef}
                        id="myConfirmPassword" value={repeat_password} onChange={this.onFormChange} />
                      {formSubmitted && !repeat_password && <div>This field is required</div>}
                      <span toggle="#myConfirmPassword" className="icon-eye field-icon toggle-password"></span>
                    </div>
                  </div>
                  {/* <div id="errors" className="col-md-12">
                    <pre>
                      {this.getPasswordError() && <span>Password must contain at least 1 Upper Case Letters. <br /></span>}
                      <span>Password must contain at least 1 Lower Case Letters.</span><br />
                      <span>Password must contain at least 1 Digits.</span><br />
                      <span>Password must contain at least 1 Special Characters.</span><br />
                      <span>Password must contain at least 8 characters.</span><br />
                    </pre>
                  </div> */}
                </div>

                <div className="row">
                  <div className="col-md-12 col-sm-12">
                    <div className="form-group">
                      <span className="password-note">(NOTE : Password must be at least 8 characters, which are a mix of  capital letters (A B), numbers (1 2 3), special characters (! &#63; &lt; &#37; ) etc)</span>
                    </div>
                  </div>
                </div>

                <div className="row" id="delegate_div">
                  <div className="col-md-12 col-sm-12">
                    <div className="form-group">
                      <input type="checkbox" name="delegate_status" id="del_add" onClick={this.checkBoxChanged} />
                      <label>&nbsp;I want to add a delegate<a data-container="body" title="Delegate user is not the company's main account holder but is authorised by main account holder to perform actions on their behalf. Only the main account holder can set up a delegate user." href="javascript:void(0);" className="tooltiplink" data-toggle="tooltip" data-placement="right" data-html="true"><i className="icon-info-circled-3"></i></a> user. Only available to small and medium businesses at the moment.</label>
                    </div>
                  </div>
                  {delegate_status &&
                    <div className="col-md-12 col-sm-12" id="del_input">
                      <div className="form-group">
                        <label>Email of delegate user <span style={{ "color": "red", "fontSize": "20px" }} >*</span></label>
                        <input type="mail" className="form-control" name="delegate_email"
                          ref={this.delegateUserRef} value={delegate_email} onChange={this.onFormChange} />
                        {formSubmitted && delegate_status && !delegate_email && <div>This field is required</div>}
                      </div>
                    </div>
                  }
                </div>

                <div className="row">
                  <div className="col-md-12 col-sm-12">
                    <div className="form-group">
                      <input type="checkbox" name="receive_email" onClick={this.checkBoxChanged} />
                      <label>&nbsp;I want to receive emails from ProtectBox in future</label>
                    </div>
                  </div>
                </div>

                <div className="row">
                  <div className="col-md-12 col-sm-12">
                    <div className="form-group">
                      <input type="checkbox" name="terms_condition" onClick={this.checkBoxChanged} />
                      <label>&nbsp;I acknowledge the <Link to="/Termsandconditions" target="_blank">Terms and Conditions</Link>. <span style={{ "color": "red", "fontSize": "20px" }}>*</span></label>
                    </div>
                  </div>
                </div>

                <div className="row">
                  <div className="col-md-12 col-sm-12">
                    <div className="form-group">
                      <ReCAPTCHA
                        sitekey="6LcdfpEUAAAAAJdvFlCEZA_j-nWNYvJQj1KjwA3S"
                        onChange={this.onCaptchaChange}
                      />
                    </div>
                  </div>
                  <div className="col-md-12 col-sm-12">
                    <div className="form-group">
                      <button className="btn btn-success btn-lg col-md-offset-5 btn-register-submit" onClick={this.onRegister} > Submit </button>
                    </div>
                  </div>
                </div>

                <div className="row">
                  <div className="col-md-12 col-sm-12">
                    <div className="form-group">
                      <label>If you already have an account, <Link to="/Login"> Login</Link> here</label>
                    </div>
                  </div>
                </div>
              </div>}
              {callingServer &&
                <div
                  className="col-md-8 col-md-offset-4"
                  style={{
                    padding: "10px 25px 10px",
                    boxShadow: "0px 0px 3px #bfbfbf",
                    borderRadius: 5,
                    textAlign: "center",
                    height: 400,
                    margin: "auto",
                  }}
                  id="loaderImg"
                >
                  <img src={LoaderGif} alt="loader" style={{ height: 64, marginTop: 100 }} />
                  <h3>Please wait while we process your request.....</h3>
                </div>
              }
              {/* End col-md-3 */}
            </div>
            {/* End row */}
          </div>
          {/* End container */}
        </main>
        <Footer />
      </div>
    );
  }
};

export default Register;
