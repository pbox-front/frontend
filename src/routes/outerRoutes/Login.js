import React from "react";
import { Link } from 'react-router-dom';
import ReCAPTCHA from "react-google-recaptcha";
import Header from "../../components/Header";
import Footer from "./../../components/Footer";
import axios from "axios";
import LoaderGif from '../../images2/favicon.gif';
import { Alert } from "react-bootstrap";

class Login extends React.Component {

  constructor() {
    super();
    this.state = {
      email: '',
      password: '',
    }

    this.onCaptchaChange = this.onCaptchaChange.bind(this);
    this.onHandleLogin = this.onHandleLogin.bind(this);
    this.onFormChange = this.onFormChange.bind(this);
  }

  componentDidMount() {
    document.title = "ProtectBox | Login"
  }

  onCaptchaChange(val) {
    this.setState({ captcha: val })
  }

  onFormChange(event) {
    this.setState({ [event.target.name]: event.target.value })
  }

  onHandleLogin() {
    if (!this.state.captcha) {
      return;
    }

    const formData = new FormData();
    formData.append('email', this.state.email);
    formData.append('password', this.state.password);
    this.setState({ callingServer: true })
    axios.post('www/api/loginapi', formData)
      .then((res) => {
        if (res.data.message === "Success") {
          this.setState({ callingServer: false }, () => {
            this.props.history.push(`/${res.data.redirect}`);
          })
        }
      }).catch(error => {
        if (error && error.response && error.response.data) {
          this.setState({ loginErrorMessage: error.response.data.message, callingServer: false, email: '', password: '' })
        }
      })
  }

  render() {
    const { email, password, callingServer = false, loginErrorMessage } = this.state;
    return (
      <div>
        <div id="load" />
        <section className="pb-section slider-main">
          <Header />
          <div className="banner-section-main login-block">
            <div className="black-overlay">
              <div className="banner-text">
                <h1 className="pbh-2 white-color">Login</h1>
              </div>
            </div>
          </div>
        </section>
        {/* End section */}
        <main>
          <div className="container margin_60">
            <div className="row">
              {!callingServer ? <React.Fragment>
                <div
                  className="col-md-6 col-md-offset-3"
                  style={{
                    padding: "10px 25px 10px",
                    border: "1px solid #CCC",
                    boxShadow: "0px 0px 3px #bfbfbf",
                    borderRadius: 5,
                    background: "#f5f5f5",
                    margin: "auto",
                  }}
                  id="login-form"
                >
                  {/* <form> */}
                  <div>
                    {loginErrorMessage && <Alert variant={'danger'}>
                      <strong>{loginErrorMessage}</strong>
                    </Alert>
                    }
                    <div className="row" style={{ marginTop: 20 }}>
                      <div className="col-md-10 col-sm-10 col-md-offset-1">
                        <div className="form-group">
                          <label>
                            Email Address{" "}
                            <span style={{ color: "red", fontSize: 20 }}>*</span>
                          </label>
                          <input
                            type="email"
                            className="form-control"
                            name="email"
                            value={email}
                            onChange={this.onFormChange}
                          />
                        </div>
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-md-10 col-sm-10 col-md-offset-1">
                        <div className="form-group">
                          <label>
                            Password{" "}
                            <span style={{ color: "red", fontSize: 20 }}>*</span>
                          </label>
                          <input
                            id="password-field"
                            type="password"
                            className="form-control"
                            name="password"
                            value={password}
                            onChange={this.onFormChange}
                          />
                          <span
                            toggle="#password-field"
                            className="icon-eye field-icon toggle-password"
                          />
                        </div>
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-md-10 col-sm-10 col-md-offset-1">
                        <div className="form-group">
                          <label>
                            Forgotten Password ?
                          <Link to={'/forgetpass'} className="inline">(click here)</Link>
                          </label>
                        </div>
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-md-10 col-sm-10 col-md-offset-1">
                        <div className="form-group">
                          <ReCAPTCHA
                            sitekey="6LcdfpEUAAAAAJdvFlCEZA_j-nWNYvJQj1KjwA3S"
                            onChange={this.onCaptchaChange}
                          />
                        </div>
                      </div>
                    </div>
                    <div className="col-md-12 col-sm-12">
                      <div className="form-group">
                        <button className="btn btn-success btn-lg col-md-offset-4 btn-login-submit" onClick={this.onHandleLogin} >Login</button>
                      </div>
                    </div>
                    {/* <div class="row">
                    <div class="col-md-10 col-sm-10 col-md-offset-1">
                      <div class="form-group">
                        <label>
                          Are you a delegate user
                          <a
                            data-container="body"
                            title="Delegate user is not the company's main account holder but is authorised by main account holder to perform actions on their behalf. Only the main account holder can set up a delegate user."
                            href="javascript:void(0);"
                            class="tooltiplink"
                            data-toggle="tooltip"
                            data-placement="right"
                            data-html="true"
                          >
                            <i class="icon-info-circled-3"></i>
                          </a>
                          ? Only available to small and medium business at the
                          moment.
                          <a href="<?php echo base_url('delegate_login');?>">
                            {" "}
                            Login
                          </a>{" "}
                          here
                        </label>
                      </div>
                    </div>
                  </div> */}
                    <div className="row">
                      <div className="col-md-10 col-sm-10 col-md-offset-1">
                        <div className="form-group">
                          <label>
                            If you have not registered an account,
                          <Link to="/Registration">&nbsp;Register&nbsp;</Link>
                          here
                        </label>
                        </div>
                      </div>
                    </div>
                    {/*End step */}
                  </div>
                  {/* </form> */}
                </div>
              </React.Fragment> :
                <React.Fragment>
                  <div
                    className="col-md-6 col-md-offset-3"
                    style={{
                      padding: "10px 25px 10px",
                      boxShadow: "0px 0px 3px #bfbfbf",
                      borderRadius: 5,
                      textAlign: "center",
                      height: 400,
                      margin: "auto",
                    }}
                    id="loaderImg"
                  >
                    <img
                      src={LoaderGif}
                      style={{ height: 64, marginTop: 100 }}
                    />
                    <h3>Please wait while we process your request.....</h3>
                  </div>
                </React.Fragment>}
              {/* End col-md-3 */}
            </div>
            {/* End row */}
          </div>
          {/* End container */}
        </main>
        <Footer />
      </div>
    );
  }
};

export default Login;
