import React from "react";
import Header from "./../../components/Header";
import Footer from "./../../components/Footer";
import { Table } from "react-bootstrap";

const Invoice = () => {
  return (
    <div id="load">
      <section className="pb-section slider-main">
        <Header />
      </section>
      <section
        id="sub_header"
        style={{
          background: "#f5f5f5",
          boxShadow: "inset 0 1px 10px 1px rgba(0,0,0,.41)",
        }}
      >
        <div className="container">
          <div
            className="main_title"
            style={{
              background: "none",
              textAlign: "center",
              fontSize: 40,
              color: "#000",
              bottom: 30,
            }}
          >
            Order Invoice
          </div>
        </div>
      </section>

      <div id="load">
        <main>
          <div className="container margin_60">
            <form method="post">
              <div
                className="container"
                id="content"
                style={{ background: "white" }}
              >
                <div className="invoice-header calc">
                  <div className="invoice-heading">INVOICE</div>
                  <div className="invoice-image">
                    <img
                      src="https://www.staging.protectbox.com/images/logo.png"
                      style={{ height: "40px" }}
                    />
                  </div>
                </div>
                <div className="ist-table ">
                  <div className="table-1 col-md-3">
                    <table>
                      {" "}
                      <tbody>
                        <tr>
                          <td class="cell-border-right">Order number</td>
                          <td>161</td>
                        </tr>

                        <tr>
                          <td class="cell-border-right">Date</td>
                          <td>22/15/2020</td>
                        </tr>
                        <tr>
                          <td class="cell-border-right">VAT No.</td>
                          <td>45</td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                  <div className="sec-2">
                    <div className="table-2 col-md-3 ">
                      <table>
                        <table>
                          <tbody>
                            <tr>
                              <td class="cell-border-right">To</td>
                              <td>Kiran Baghotra</td>
                            </tr>

                            <tr>
                              <td class="cell-border-right">Address</td>
                              <td></td>
                            </tr>
                          </tbody>
                        </table>
                      </table>
                    </div>
                  </div>
                </div>
                <div className="invoice">
                  <Table bordered>
                    <thead>
                      <tr>
                        <th className="left-cell">Description</th>
                        <th className="right-cell ">Amount</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td className="left-cell cell-border-right   cell-border-bottom ">
                          HP Data Protector Direct Backup using NDMP - Licence -
                          1 server, 1TB capacity - Win ** MANDATORY SUPPORT MUST
                          BE PURCHASED - PLEASE CONTACT THE SERVER TEAM FOR MORE
                          INFORMATION **
                        </td>
                        <td className="right-cell text-right ml-3 cell-border-bottom">
                          £ 1044.9
                        </td>
                      </tr>
                      <tr>
                        <td className="left-cell text-right calc cell-border-top">
                          Subtotal
                        </td>
                        <td className="right-cell text-right calc ">
                          £ 1044.9.0
                        </td>
                      </tr>
                      <tr>
                        <td className="left-cell text-right calc ">VAT</td>
                        <td className="right-cell text-right calc ">£ 0.00</td>
                      </tr>
                      <tr>
                        <td className="left-cell text-right calc ">Total</td>
                        <td className="right-cell text-right calc ">
                          £ 1044.9.00
                        </td>
                      </tr>
                    </tbody>
                  </Table>
                  <p>
                    Copyright © 2018 ProtectBox Ltd. Company number: NI643316
                    VAT
                  </p>

                  <p>registration number 297 5082 62</p>
                  <br />
                  <div style={{ textAlign: "center" }}>
                    <button
                      type="submit"
                      className="btn btn-success btn-lg col-md-offset-<?php echo (($email == 'activate')?'4':'5');?> btn-login-submit"
                    >
                      Download as pdf
                    </button>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </main>
      </div>
      <Footer />
    </div>
  );
};

export default Invoice;
