import React, { useEffect } from "react";
import { Link } from 'react-router-dom';
import Footer from "./../../components/Footer";
import Header from "./../../components/Header";
import UseCases from "./../../components/UseCases";
import AwardWinning from "./../../components/AwardWinning";
import ExistingNews from "./../../components/ExistingNews";
import LastInsidesAndNews from "./../../components/LastInsidesAndNews";
import LatestInsides from "./../../components/LatestInsides";
import PartnerInvestor from "./../../components/PartnerInvestor";
import { OverlayTrigger, Button, Tooltip, Popover } from "react-bootstrap";

import { FaLinkedinIn } from "react-icons/fa";
import { FaTwitter } from "react-icons/fa";
import { FaRegEnvelope, FaEnvelope } from "react-icons/fa";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import img1 from "./../../../src/images2/xero.png";
import img2 from "./../../../src/images2/investor-01.png";
import img3 from "./../../../src/images2/oracle-logosm.png";
import img4 from "./../../../src/images2/investor-10.png";
import img5 from "./../../../src/images2/investor-12.png";
import img6 from "./../../../src/images2/investor-11.png";
import img7 from "./../../../src/images2/investor-09.png";
import img8 from "./../../../src/images2/investor-08.png";

const About = () => {

  useEffect(() => {
    document.title = "ProtectBox | About"
  }, [])

  var settings1 = {
    arrows: true,
    slidesToShow: 6,
    autoplay: false,
    swipeToSlide: true,
    focusOnSelect: true,

    responsive: [
      {
        breakpoint: 1024,
        settings: {
          arrows: false,
          slidesToShow: 4,
          autoplay: false,
          swipeToSlide: true,
          focusOnSelect: true,
        },
      },
      {
        breakpoint: 600,
        settings: {
          arrows: false,
          slidesToShow: 2,
          autoplay: false,
          swipeToSlide: true,
          focusOnSelect: true,
        },
      },
      {
        breakpoint: 480,
        settings: {
          arrows: false,
          slidesToShow: 2,
          autoplay: false,
          swipeToSlide: true,
          focusOnSelect: true,
        },
      },
    ],
  };

  var settings2 = {
    dots: true,

    arrows: false,
    slidesToShow: 4,
    autoplay: true,
    swipeToSlide: true,
    focusOnSelect: true,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 4,
          autoplay: true,
          swipeToSlide: true,
          focusOnSelect: true,
        },
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          autoplay: true,
          swipeToSlide: true,
          focusOnSelect: true,
        },
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          autoplay: true,
          swipeToSlide: true,
          focusOnSelect: true,
        },
      },
    ],
  };

  var Font13Style = {
    fontSize: 13
  };

  var popoverStyle = {
    maxWidth: "500px",
    position: "absolute",
    fontSize: 13,
    zIndex: 9999,
    padding: "0px",
    width: "98%"
  };

  const popoverKiran = (
    <Popover id="popover-kiran" style={{ ...popoverStyle }}>
        <Popover.Content>
            <p style={{ ...Font13Style }}>All of Kiran's career has been about winning hearts & minds, most recently managing UK govt bi-lateral & multi-lateral relations oncyber. Her prior consulting for founders, industry & investment banking/VC & PE careers have given her the expertise to design product incl writing ProtectBox’s award-winning AI & managing the developers who deliver it. Plus gave her the network to take them to market. This is Kiran's 3rd start-up bringing government, business and consumers together.</p>
            <p style={{ ...Font13Style }}>Career highlights include scuba diving the world for a year alone; co-ordinating #WeProtect (children against online exploitation) Summit; delivering Ministerial Reviews on Critical National Infrastructure (CNI) resilience & machine learning digital redaction; Health (incl Pandemics) lead for the Secretariat that managed the UK govt's Cabinet Office Briefing Room (COBR) meetings between 2013-14; raising the 1st Socially Responsible Investment in South Africa supported by Archbishop Tutu for miner Lonmin & raising the 1st Indian infrastructure private equity fund at 3i. Selling lingerie & working as a nurse whilst training as a doctor. First Class honours degree & Jelf Medal for her BSc Mathematics & Management from the University of London.</p>
        </Popover.Content>
    </Popover>
  );

  const popoverJulia = (
    <Popover id="popover-julia" style={{ ...popoverStyle }}>
        <Popover.Content>
            <p style={{ ...Font13Style }}>Julia and her team at https://www.incisive-edge.com/ (IE) manage ProtectBox’s sales strategies and marketing campaigns designed to deliver revenue growth & scale to drive towards our next round of funding. Julia/IE has worked with founder Kiran on 3 previous website builds and sales/marketing campaigns, all with tight budgets and time constraints. IE designed ProtectBox’s original MVP (front- & back-end), right through to the latest content and campaigns.</p>
            <p style={{ ...Font13Style }}>IE have 10+ years of generating high-value growth for tech companies by combining strategic planning, content creation, demand generation and optimisation to drive results delivering on Key Metrics, across multiple geographies.Prior to running IE as a successful marketing agency for 10 years, Julia’s experience includes as in-house sales and marketing and as a lawyer. </p>
        </Popover.Content>
    </Popover>
  );

  const popoverBen = (
    <Popover id="popover-ben" style={{ ...popoverStyle }}>
        <Popover.Content>
            <p style={{ ...Font13Style }}>Ben helps companies grow by finding customers and investors, and by building engaged teams and strong scalable processes. He excels at combining vision with technological pragmatism. His experience is in technology, cybersecurity, fintech, real estate and government. He also holds Board roles. Kiran & Ben met at the start of ProtectBox’s journey when Ben ran fintech, cyber & retail hub Level39 in Canary Wharf, London. More recently, founder Kiran pledged 1% of ProtectBox to Ben’s https://genieshares.com/ which is bringing everyone a chance of ownership of great growing companies. Making him our first investor</p>
            <p style={{ ...Font13Style }}>Ben previously founded, built, and sold an online payments business, and completed a contract for the UK government improving access to finance for UK startups and innovative companies. He brings together practical experience of entrepreneurship with deep understanding of policy and delivery. Before founding his own business, he was an officer in the Royal Marines Commandos and an investment banker with J.P. Morgan in the UK, USA, and Middle East.</p>
        </Popover.Content>
    </Popover>
  );

  const popoverAdvisors = (
    <Popover id="popover-advisors" style={{ ...popoverStyle }}>
        <Popover.Content>
            <p style={{ ...Font13Style }}>All our Advisors have previously helped develop ProtectBox’s strategy, product, marketing, channels, amongst other functions. They continue to support on an on-going basis, as and when their time and portfolio careers allow, or the need arises. They include serial entrepreneurs, business, and defence/security experts.</p>
            <p style={{ ...Font13Style }}> Most recent of which is Nick Bray CBE who devised and automated ProtectBox’s global operations. He is now Chief Strategy Officer of a Satellite intelligence company and leads on innovation for a Defence & Security Consultancy. He also continues to serve as Air Commodore in the Royal Air Force Reserve, driving RAF innovation. Nick & Kiran worked together in UK government. A profile of accomplishment (albeit in other disciplines) common to the other Advisors.</p>
        </Popover.Content>
    </Popover>
  );

  const popoverDevelopers = (
    <Popover id="popover-developers" style={{ ...popoverStyle }}>
        <Popover.Content>
            <p style={{ ...Font13Style }}>10+ years coding experience<br /> Government and Corporate contracts<br /> Remote CTO for start-ups<br /> Predominantly BAME teams</p>
        </Popover.Content>
    </Popover>
  );

  const popoverInterns = (
    <Popover id="popover-interns" style={{ ...popoverStyle }}>
        <Popover.Content>
            <p style={{ ...Font13Style }}>We have over the years taken on several batches of interns from https://www.hult.edu/, https://www.ulster.ac.uk/ & https://www.callutheran.edu/management/ who have done industry placements with us, as part of their studies. Open to similar initiatives in the same & other parts of the world</p>
            <p style={{ ...Font13Style }}>We have also interned ex-Forces career transitioners through AWS restart, as well as developing internal initiatives</p>
            <p style={{ ...Font13Style }}>Predominantly BAME and female teams</p>
        </Popover.Content>
    </Popover>
  );

  return (
    <div className="wrapper">
      <section className="pb-section slider-main">
        <Header />
        <div className="banner-section-main">
          <div className="black-overlay">
            <div className="banner-text">
              <h1 className="pbh-2 white-color inclusive">
                Inclusive and responsible AI cybersecurity for everyone
              </h1>
              <Link className="btn btn-primary" to="/ContactUs">
                Contact us
              </Link>
            </div>
          </div>
        </div>
      </section>

      <section className="pb-section gray-bg pb-content-section">
        <div className="container">
          <div className="content-box">
            <h1 className="pbh-3">
              Award-winning AI cybersecurity creating a fair and inclusive
              marketplace
            </h1>
            <p>
              ProtectBox is the go-to platform for businesses looking for
              jargon-free cybersecurity bundles to protect their whole business.
              Our award-winning, responsible AI is eliminating bias and creating
              a fair and inclusive marketplace for everyone.
            </p>
            <p>
              We are a fairly priced marketplace, founded in 2017, by CEO, Kiran
              Bhagotra. After she left her Security role in the UK government’s
              Cabinet Office, Kiran realised businesses were spending huge
              amounts, but also getting a bad deal when buying security.
            </p>
            <p>
              {" "}
              We don't think that is fair, which is why we are transparent
              (showing Supplier prices online, not just referring you to a
              Supplier who could then increase their price) and in-line with the
              market. We offer you both in-direct and direct sales of Suppliers’
              products/services to ensure the prices you see are market aligned.
            </p>
            <p>
              {" "}
              We're also not just about improving the process, but championing
              small and medium businesses to receive fairer deals. We are making
              the cybersecurity world more approachable for you by hiring those
              that wouldn't normally be seen as the norm for cybersecurity.
            </p>
            <p>
              We’re ‘less geek’ and as much a small and medium business, just
              like you.
            </p>
          </div>
        </div>
      </section>
      <section className="pb-section why-protect-box">
        <div className="container">
          <h1 className="pbh-3">Whats Important to Us</h1>
          <div className="pb-row">
            <div className="pb-col-12 pb-col-md-2">
              <div className="pb-block">
                <img
                  src="/icon/important-us0.png"
                  width="75px"
                />
                <p>
                  Supporting Women in AI and bringing women into the
                  cybersecurity space
                </p>
              </div>
            </div>
            <div className="pb-col-12 pb-col-md-2">
              <div className="pb-block">
                <img
                  src="/icon/important-us1.png"
                  width="75px"
                />
                <p>50% diversity targets for team hires and partners</p>
              </div>
            </div>
            <div className="pb-col-12 pb-col-md-2">
              <div className="pb-block">
                <img
                  src="/icon/important-us2.png"
                  width="75px"
                />
                <p>
                  Clean growth strategy. Our vision is for our platform to be
                  carbon neutral, so we have built our technology in Cloud
                  providers who aim to be net-zero by 2040, 10 years ahead of
                  the Paris Agreement
                </p>
              </div>
            </div>
            <div className="pb-col-12 pb-col-md-2">
              <div className="pb-block">
                <img
                  src="/icon/important-us3.png"
                  width="75px"
                />
                <p>
                  Reducing bias in our algorithm to create a fair marketplace
                  for buyers and suppliers
                </p>
              </div>
            </div>
            <div className="pb-col-12 pb-col-md-2">
              <div className="pb-block">
                <img
                  src="/icon/important-us4.png"
                  width="75px"
                />
                <p>
                  Supporting businesses affected by COVID-19 and UN sustainable
                  development goals
                </p>
              </div>
            </div>
            <div className="clearfix" />
          </div>
        </div>
      </section>
      <article className="pb-section gray-bg pb-team-section">
        <div className="container">
          <h1 className="pbh-3">Meet the team</h1>
          <div className="row">
            <div className="col-12">
            <Slider {... settings1}>
              <div className="item" style={{ marginRight: "10px" }}>
                <div className="team-box">
                  <div className="team-img">
                    <img alt="teams" src="./../../../image/about/5.png" className="img-fluid img-member" />
                  </div>
                  <div className="team-info">
                    <h2>Kiran Bhagotra</h2>
                    <h5>CEO / CTO / Founder</h5>
                  </div>
                </div>
                <div className="social-account ">
                  <a href="https://www.linkedin.com/in/kiran-bhagotra-9a9455137/" target="_blank" >
                    <FaLinkedinIn />
                  </a>
                  <a href="https://twitter.com/ProtectBoxLtd" target="_blank">
                    <FaTwitter />
                  </a>
                </div>
              </div>
              <div className="item" style={{ marginRight: "10px" }}>
                <div className="team-box">
                  <div className="team-img">
                    <img alt="teams" src="./../../../image/about/2.png" className="img-fluid img-member" />
                  </div>
                  <div className="team-info">
                    <h2>Julia Payne</h2>
                    <h5>CMO (Part-time, Outsourced)</h5>
                  </div>
                </div>
                <div className="social-account ">
                  <a href="https://www.linkedin.com/in/juliapaynestartupmarketing/" target="_blank" >
                    <FaLinkedinIn />
                  </a>
                  <a href="https://twitter.com/juliapayne404?lang=en" target="_blank">
                    <FaTwitter />
                  </a>
                </div>
              </div>
              <div className="item" style={{ marginRight: "10px" }}>
                <div className="team-box">
                  <div className="team-img">
                    <img alt="teams" src="./../../../image/about/3.png" className="img-fluid img-member" />
                  </div>
                  <div className="team-info">
                    <h2>Ben Brabyn</h2>
                    <h5>COO (Part-time)</h5>
                  </div>
                </div>
                <div className="social-account ">
                  <a href="https://www.linkedin.com/in/juliapaynestartupmarketing/" target="_blank" >
                    <FaLinkedinIn />
                  </a>
                  <a href="https://twitter.com/juliapayne404?lang=en" target="_blank">
                    <FaTwitter />
                  </a>
                </div>
              </div>
              <div className="item" style={{ marginRight: "10px" }}>
                <div className="team-box">
                  <div className="team-img">
                    <img alt="teams" src="./../../../image/about/6.png" className="img-fluid img-member" />
                  </div>
                  <div className="team-info">
                    <h2>Emma Middleton</h2>
                    <h5>Sales Director</h5>
                  </div>
                </div>
                <div className="social-account ">
                  <a href="https://uk.linkedin.com/in/emma-middleton-b1944422" target="_blank" >
                    <FaLinkedinIn />
                  </a>
                  <a href="#" target="_blank">
                    <FaTwitter />
                  </a>
                </div>
              </div>
              <div className="item" style={{ marginRight: "10px" }}>
                <div className="team-box">
                  <div className="team-img">
                    <img alt="teams" src="./../../../image/about/profile.png" className="img-fluid img-member" />
                  </div>
                  <div className="team-info">
                    <h2>Advisors</h2>
                    <h5>(Ad-hoc)</h5>
                  </div>
                </div>
              </div>
              <div className="item" style={{ marginRight: "10px" }}>
                <div className="team-box">
                  <div className="team-img">
                    <img alt="teams" src="./../../../image/about/profile.png" className="img-fluid img-member" />
                  </div>
                  <div className="team-info">
                    <h2>Developers</h2>
                    <h5>(Outsourced)</h5>
                  </div>
                </div>
              </div>
              <div className="item" style={{ marginRight: "10px" }}>
                <div className="team-box">
                  <div className="team-img">
                    <img alt="teams" src="./../../../image/about/profile.png" className="img-fluid img-member" />
                  </div>
                  <div className="team-info">
                    <h2>Interns</h2>
                    <h5>(Ad-hoc)</h5>
                  </div>
                </div>

              </div>
            </Slider>
            </div>
            <div className="col-md-12 infocard p-3"></div>
          </div>
        </div>
      </article>
      <section className="pb-section pb-content-section">
        <div className="container">
          <div className="content-box">
            <h1 className="pbh-3">Our Story</h1>
            <p>
              Our founder, Kiran Bhagotra, is a third-time, successful
              entrepreneur. Kiran left her UK government role in the Cabinet
              Office promoting UK cyber globally to put her (little) money where
              her (big) mouth is' to disrupt the unacceptable security gap for
              small and medium businesses globally.
            </p>
            <p>
              Security is a right we all deserve and we are making it a reality,
              by automating empowerment. Using an independent plug and play
              marketplace, global strategic alliances, regulation focus and
              sales analytics, our aim is to create a diverse and fully
              inclusive cybersecurity marketplace for everyone.
            </p>
          </div>
        </div>
      </section>
      <section className="pb-section pb-journey-info">
        <div className="container">
          <div className="pbj-row">
            <div className="pbj-col-12 pbj-col-md-1">
              <div className="pbj-block">
                <div className="pbj-icon-box">
                  <i className="fa fa-lightbulb-o" aria-hidden="true" />
                  <h6>Idea</h6>
                </div>
                <h3 className="year-bar">2016</h3>
                <h4>In the beginning...</h4>
              </div>
            </div>
            <div className="pbj-col-12 pbj-col-md-3">
              <div className="pbj-block">
                <div className="pbj-icon-box">
                  <i className="fa fa-rocket" aria-hidden="true" />
                  <h6>Mission and purpose</h6>
                </div>
                <div className="pbj-icon-box">
                  <i className="fa fa-cogs" aria-hidden="true" />
                  <h6>Process</h6>
                </div>
                <div className="pbj-icon-box">
                  <i className="fa fa-handshake-o" aria-hidden="true" />
                  <h6>Partnerships</h6>
                </div>
                <h3 className="year-bar">2017</h3>
              </div>
            </div>
            <div className="pbj-col-12 pbj-col-md-3">
              <div className="pbj-block">
                <div className="pbj-icon-box">
                  <img
                    src="/icon/protectbox.png"
                    width="65px"
                    style={{ padding: 5 }}
                  />
                  <h6>ProtectBox officially launched globally</h6>
                </div>
                <div className="pbj-icon-box">
                  <i className="fa fa-flag" aria-hidden="true" />
                  <h6>Today</h6>
                </div>
                <div className="pbj-icon-box">
                  <i className="fa fa-globe" aria-hidden="true" />
                  <h6>What's next</h6>
                </div>
                <h3 className="year-bar">March 2020</h3>
                <h4>Where we're heading...</h4>
              </div>
            </div>
            <div className="clearfix" />
          </div>
        </div>
      </section>
      <article class="pb-section pb-home-security">
          <div class="container">
              <div class="row">
                  <div class="col-12 col-md-6 mb-3 pr-3">
                      <div class="pb-video">
                          <div class="video-overlay">
                              <div class="video-icon">
                                  <a id="videobtn" class="video-btn white" href="https://youtu.be/3D4b743Leio" target="_blank">
                                      <i class="fa fa-play"></i>
                                  </a>
                              </div>
                              <h2 class="pbh-2 white-color">1 minute pitch</h2>
                          </div>
                      </div>
                  </div>
                  <div class="col-12 col-md-6 mb-3 pr-3">
                      <div class="pb-video">
                          <div class="video-overlay video-overlay2">
                              <div class="video-icon">
                                  <a id="videobtn" class="video-btn white" href="https://youtu.be/wM1lfbCKtSo" target="_blank">
                                      <i class="fa fa-play"></i>
                                  </a>
                              </div>
                              <h2 class="pbh-2 white-color">3 minute pitch</h2>
                          </div>
                      </div>
                  </div>
                  <div class="col-12 col-md-12">
                      <div class="pb-btn-box">
                          <a class="btn btn-primary" href="/ContactUs">Contact Us</a>
                      </div>
                  </div>
              </div>
          </div>
      </article>
      <PartnerInvestor />
      <ExistingNews />
      <AwardWinning />
      <LastInsidesAndNews />
      <LatestInsides />
      <Footer />
    </div>
  );
};

export default About;
