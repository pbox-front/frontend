import React, { Fragment } from "react";
import Footer2 from "./../../components/Footer2";
import Header2 from "./../../components/Header2";
import { Form } from "react-bootstrap";
import DataTable from "react-data-table-component";

const datas = [
  {
    id: 1,
    dn: 1,
    na: "	Kiran Bhagotra",
    em: "	kbbhagotra@yahoo.com",
    ph: "	475645456",
    id: "12/11/2020",
    lrd: "	12/11/2020",
    st: "True",
    sr: "True",
    ac: "True",
    qa: "djbdjbd",
  },
];
const columns = [
  {
    name: "Delegate Number",
    selector: "dn",
    sortable: true,
  },
  {
    name: "Name",
    selector: "na",
    sortable: true,
    right: true,
  },
  {
    name: "Email",
    selector: "em",
    sortable: true,
    right: true,
  },
  {
    name: "Phone",
    selector: "ph",
    sortable: true,
    right: true,
  },
  {
    name: "Questionnaire Access",
    selector: "qa",
    sortable: true,
    right: true,
  },
  {
    name: "Invite Date",
    selector: "id",
    sortable: true,
    right: true,
  },
  {
    name: "Last Reminder date",
    selector: "lrd",
    sortable: true,
    right: true,
  },

  {
    name: "Status",
    selector: "st",
    sortable: true,
    right: true,
  },
  {
    name: "Send Reminder",
    selector: "sr",
    sortable: true,
    right: true,
  },
  {
    name: "Action",
    selector: "ac",
    sortable: true,
    right: true,
  },
];

const ManageDelegates = () => {
  return (
    <div>
       <section className="pb-section slider-main">
        <Header2 />
      </section>
      <section
        id="sub_header"
        style={{
          background: "#f5f5f5",
          boxShadow: "inset 0 1px 10px 1px rgba(0,0,0,.41)",
        }}
      >
        <div className="container">
          <div
            className="main_title"
            style={{
              background: "none",
              textAlign: "center",
              fontSize: 40,
              color: "#000",
              bottom: 30,
            }}
          >
            Be a Delegate
          </div>
        </div>
      </section>

      <main>
        <div className="container margin_60 manage-delegate">
          <div className="row">
            <div className="col-md-12">
              <div className="tab-content rounded_div">
                <a
                  href="<?php echo base_url('delegate_questionaire_tech_info');?>/<?php echo $fetch_del->sme_id;?>"
                  className="btn btn-success"
                  style={{ borderRadius: "5px" }}
                >
                  Add Delegate
                </a>
                <ul className="cupon-btn">
                  <li>
                    <button>Copy</button>
                  </li>
                  <li>
                    <button>CSV</button>
                  </li>
                  <li>
                    <button>Excel</button>
                  </li>
                  <li>
                    <button>Print</button>
                  </li>
                </ul>
                <DataTable
                  title="All Sales"
                  columns={columns}
                  data={datas}
                  subHeader={true}
                  subHeaderAlign={"left"}
                  pagination
                  compact={true}
                  wrap={true}
                  subHeaderComponent={
                    <div style={{ display: "flex", alignItems: "center" }}>
                      <Form.Group
                        style={{
                          display: "flex",
                          alignItems: "center",
                        }}
                        controlId="exampleForm.ControlSelect1"
                      >
                        <Form.Label>Show</Form.Label>
                        <Form.Control
                          as="select"
                          style={{ margin: "0 0.5rem" }}
                        >
                          <option>1</option>
                          <option>2</option>
                          <option>3</option>
                          <option>4</option>
                          <option>5</option>
                        </Form.Control>
                        <Form.Label>Entries</Form.Label>
                      </Form.Group>
                    </div>
                  }
                />
                <div
                  className=" table-responsive"
                  style={{ zoom: "90%" }}
                ></div>
              </div>
            </div>
          </div>
        </div>
      </main>
      <Footer2 />
    </div>
  );
};

export default ManageDelegates;
