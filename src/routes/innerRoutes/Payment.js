import React from "react";
import { Tabs, Tab } from "react-bootstrap";
import { FaInfoCircle } from "react-icons/fa";
import { BsArrowsCollapse } from "react-icons/bs";
import { AiFillDollarCircle } from "react-icons/ai";

import Footer2 from "./../../components/Footer2";
import Header2 from "./../../components/Header2";

const Payment = () => {
  return (
    <div>
      <div id="load"></div>
      <section className="pb-section slider-main">
        <Header2 />
      </section>

      <section
        id="sub_header"
        style={{
          background: "#f5f5f5",
          boxShadow: "inset 0 1px 10px 1px rgba(0,0,0,.41)",
        }}
      >
        <div className="container">
          <div
            className="main_title"
            style={{
              background: "none",
              textAlign: "center",
              fontSize: 40,
              color: "#000",
              bottom: 30,
            }}
          >
            Payments
          </div>
        </div>
      </section>

      <main className="payment">
        <div className="container margin_60">
          <div className="row">
            <div className="col-md-12">
              <ul className="nav " />
              <div className="tab-content rounded_div">
                <div
                  className="tab-pane active"
                  id="home"
                  style={{ minHeight: 350 }}
                >
                  <div className="form_title" style={{ marginBottom: 50 }}>
                    <h3 className="dollar-wrapper">
                      <strong>
                        <span className="dollar">
                          <AiFillDollarCircle />
                        </span>
                      </strong>
                      Payment
                    </h3>
                  </div>
                  <Tabs defaultActiveKey="paypal" id="uncontrolled-tab-example">
                    <Tab eventKey="paypal" title="Paypal">
                      <div
                        role="tabpanel"
                        className="tab-pane active"
                        id="paypal"
                      >
                        <div
                          className="row"
                          id="paypalBrowser"
                          style={{ marginTop: 30 }}
                        >
                          <div
                            className="alert alert-danger paypal_required"
                            style={{ display: "none" }}
                          >
                            <strong>You must fill the details!</strong>
                          </div>
                          <div
                            className="alert alert-success paypal_success"
                            style={{ display: "none" }}
                          >
                            <strong>Paypal details has been updated.</strong>{" "}
                          </div>
                          <div className="col-md-7">
                            <div className="form-group">
                              <label>
                                Please can you give us your PayPal email so that
                                we can pay you (automatically, directly and
                                securely) when we sell your products or services
                                through ProtectBox.We only provide payment
                                through PayPal at the moment. But will be
                                offering other methods shortly.
                                <span style={{ color: "red" }}>*</span>
                              </label>
                            </div>
                          </div>
                          <div className="col-md-5">
                            <div className="row">
                              <div
                                style={{ width: "100%" }}
                                className="form-group"
                              >
                                <input
                                  type="email"
                                  className="form-control required valid paypal_in"
                                  name="paypal_in"
                                  placeholder="Enter your paypal email."
                                  required
                                />
                                <br />
                                <button
                                  className="btn btn-success "
                                  name="paypal"
                                  value="Update"
                                  style={{
                                    marginTop: 5,
                                    textAlign: "left",

                                    float: "left",
                                    borderRadius: "5px",
                                  }}
                                  onclick="update_paypal();"
                                >
                                  Update
                                </button>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </Tab>
                    <Tab eventKey="profile" title="Stripe">
                      <div role="tabpanel" className="tab-pane" id="stripe">
                        <div
                          className="row"
                          id="paypalBrowser"
                          style={{ marginTop: 30 }}
                        >
                          <div className="col-md-7">
                            <div className="form-group">
                              <label>
                                Connect Stripe to recieve payments.
                                <span style={{ color: "red" }}>*</span>
                              </label>
                            </div>
                          </div>

                          <div className="col-md-5">
                            <span
                              className="btn btn-success"
                              style={{
                                marginTop: 5,
                                textAlign: "left",
                                float: "left",
                                borderRadius: "5px",
                              }}
                            >
                              <i className="fa fa-check" aria-hidden="true" />
                              Connected!!
                            </span>
                          </div>
                        </div>
                      </div>
                    </Tab>
                    <Tab eventKey="contact" title="Bank">
                      <div role="tabpanel" className="tab-pane" id="bank">
                        <div className="col-md-12">
                          <div className="form-group">
                            <label>
                              Choose Currency &nbsp;
                              <span
                                style={{
                                  color: "#ec8b0d",
                                  fontSize: 22,
                                }}
                              >
                                *
                              </span>
                            </label>
                            <select
                              className="form-control bank_currency"
                              name="bank_country"
                              onchange="country_select(this.value);"
                            >
                              <option hidden value>
                                Choose an Option
                              </option>
                              <option value="EUR">EUR</option>
                              <option value="GBP">GBP</option>
                              <option value="USD">USD</option>
                            </select>
                            <br />
                            <button
                              style={{ borderRadius: "5px" }}
                              className="btn btn-success "
                            >
                              Update
                            </button>
                          </div>
                        </div>
                      </div>
                    </Tab>
                    <Tab eventKey="bank" title="Priority">
                      <div role="tabpanel" className="tab-pane" id="priority">
                        <div
                          className="row"
                          id="paypalBrowser"
                          style={{ marginTop: 30 }}
                        >
                          <div
                            className="alert alert-success"
                            id="yoo"
                            style={{ display: "none" }}
                          >
                            <strong>Payment priority has been changed.</strong>{" "}
                          </div>
                          <div
                            className="alert alert-success "
                            id="result"
                            style={{ display: "none" }}
                          >
                            <strong>
                              Payment receive option has been changed.
                            </strong>{" "}
                          </div>
                          <div
                            className="col-md-12 row"
                            style={{ padding: 10 }}
                          >
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>
                                  <b>
                                    Choose how you want to process order and
                                    receive payments
                                  </b>
                                </label>
                              </div>
                            </div>
                            <div className="col-md-6">
                              <label
                                style={{ marginRight: "15px" }}
                                className="radio-inline"
                              >
                                <input
                                  type="radio"
                                  name="optradio"
                                  defaultValue="PRIOR"
                                />
                                Auto{" "}
                                <span
                                  style={{ fontSize: "15px", color: "green" }}
                                >
                                  <FaInfoCircle />
                                </span>
                                <a
                                  data-container="body"
                                  title="This option enables you to receive payments instantly when a order is placed."
                                  href="javascript:void(0);"
                                  className="tooltiplink"
                                  data-toggle="tooltip"
                                  data-placement="right"
                                  data-html="true"
                                ></a>
                              </label>
                              <label className="radio-inline">
                                <input
                                  type="radio"
                                  name="optradio"
                                  defaultValue="POST"
                                />{" "}
                                Manual{" "}
                                <span
                                  style={{ fontSize: "15px", color: "green" }}
                                >
                                  <FaInfoCircle />
                                </span>
                                <a
                                  data-container="body"
                                  title="This option requires you to approve order manually on your dashboard to receive payment."
                                  href="javascript:void(0);"
                                  className="tooltiplink"
                                  data-toggle="tooltip"
                                  data-placement="right"
                                  data-html="true"
                                >
                                  <i className="icon-info-circled-3" />
                                </a>
                              </label>
                            </div>
                          </div>
                          <div
                            className="col-md-12 row"
                            style={{ marginTop: 10, padding: 10 }}
                          >
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>
                                  <b>Drag/drop to set your payment priority.</b>
                                </label>
                              </div>
                            </div>
                            <div className="col-md-6">
                              <ul
                                id="sortable"
                                style={{
                                  width: "100%",
                                  marginLeft: "-45px",
                                  listStyle: "none",
                                }}
                              >
                                <li
                                  className="ui-state-default"
                                  id="<?php echo $get_priority[$i];?>"
                                >
                                  <BsArrowsCollapse />
                                </li>
                              </ul>
                            </div>
                          </div>
                        </div>
                      </div>
                    </Tab>
                  </Tabs>
                </div>
              </div>
            </div>
            {/* End col-md-12*/}
          </div>
          {/* End row */}
        </div>
        {/* End container */}
      </main>
      <Footer2 />
    </div>
  );
};

export default Payment;
