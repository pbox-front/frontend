import React from "react";
import Header2 from "./../../components/Header2";
import Footer2 from "./../../components/Footer2";
import skip from "./../../images2/skip.png";
import { BsCircle } from "react-icons/bs";
import { FaInfoCircle, FaIndustry, FaUsers } from "react-icons/fa";
import { HiUserCircle } from "react-icons/hi";
import { ImLocation2 } from "react-icons/im";

import user from "./../..//images2/deligate_icon.png";

const Questionaire = () => {
  return (
    <div>
      <div id="load"></div>
      <section className="pb-section slider-main">
        <Header2 />
      </section>
      <section
        id="sub_header"
        style={{
          background: "#f5f5f5",
          boxShadow: "inset 0 1px 10px 1px rgba(0,0,0,.41)",
        }}
      >
        <div className="container">
          <div className="main_title other_title">
            Let's source your protection..
          </div>
        </div>
      </section>

      <main id="mouse_move questionaire">
        <div className="container margin_60">
          <div className="row">
            <div className="col-md-12">
              <div
                className="alert alert-success"
                id="success_div"
                style={{ display: "none" }}
              >
                <strong>
                  You have successfully assigned this question to your chosen
                  delegate user. You will see their name appear in red under the
                  question &amp; can manage their access through "Account -
                  Delegates" in top right hand corner.
                </strong>{" "}
              </div>

              <ul
                className=" tab-question"
                id="myDIV"
                style={{ marginBottom: 10 }}
              >
                <li>
                  <a
                    style={{ background: "#EC8C0E", color: "white" }}
                    href="<?php echo base_url('questionaire');?>"
                    className="red-gradient"
                  >
                    Basics
                  </a>
                </li>

                <li>
                  <a
                    href="<?php echo base_url('questionaire_tech_info');?>"
                    className="red-gradient_success"
                  >
                    Technical Info
                  </a>
                </li>

                <li>
                  <a href="#!" className="red-gradient_success">
                    Non-Technical Info
                  </a>
                </li>

                <li>
                  <a
                    href="<?php echo base_url('questionaire_budget');?>"
                    className="red-gradient_success"
                  >
                    Budget
                  </a>{" "}
                </li>
              </ul>
              <div className="tab-content rounded_div">
                <div className="tab-pane active" id="home">
                  <div className="account_info">
                    <div className="row">
                      <div className="col-md-12">
                        <p style={{ fontSize: 15, fontWeight: "normal" }}>
                          <span style={{ fontSize: "10px" }}>
                            <BsCircle />{" "}
                          </span>{" "}
                          Click "Account" button in top right corner to change
                          your details (Settings), see your purchases (Orders)
                          or to come back to your saved answers before your
                          first purchase (Questionnaire).
                        </p>
                      </div>
                      <div className="col-md-12">
                        <p style={{ fontSize: 15, fontWeight: "normal" }}>
                          <span style={{ fontSize: "10px" }}>
                            <BsCircle />
                          </span>{" "}
                          You must answer each section (Basics, Technical Info
                          etc) COMPLETELY to save your answers. You can press
                          the <img src={skip} style={{ height: 30 }} /> button
                          (in the bottom right of the page) if you have asked
                          delegates to answer questions on a tab &amp; are
                          waiting for them to reply. This will take you to the
                          next tab of questions. But your delegate will need to
                          provide their answers before you can see the bundles
                          in our Results. You can send reminder emails to your
                          delegates, delete or re-assign delegates, from the
                          "Delegates" menu, found by clicking the "Account"
                          button in top right corner.
                        </p>
                      </div>
                      <div className="col-md-12">
                        <p style={{ fontSize: 15, fontWeight: "normal" }}>
                          <span style={{ fontSize: "10px" }}>
                            <BsCircle />
                          </span>{" "}
                          After you've bought from us once, you can buy a
                          monthly subscription to store your answers to buy
                          add-ons as many times as you'd like.
                        </p>
                      </div>
                    </div>
                  </div>
                  <div className="another">
                    <div className="row">
                      <div className="col-md-12">
                        <p
                          style={{
                            color: "#ec8b0d",
                            fontSize: 15,
                            fontWeight: "normal",
                          }}
                        >
                          <span style={{ fontSize: "10px" }}>
                            <BsCircle />
                          </span>{" "}
                          All questions with an asterisk
                          <span
                            style={{
                              color: "#ec8b0d",
                              fontSize: 22,
                              marginTop: 10,
                            }}
                          >
                            *
                          </span>{" "}
                          must be answered.
                        </p>
                      </div>
                      <div
                        className="col-md-12"
                        style={{ padding: 0, margin: 0 }}
                      >
                        <div className="col-md-12">
                          <p
                            style={{
                              color: "#83C72A",
                              fontSize: 15,
                              fontWeight: "normal",
                            }}
                          >
                            <span style={{ fontSize: "10px" }}>
                              <BsCircle />
                            </span>{" "}
                            Click on
                            <a
                              data-container="body"
                              href="javascript:void(0);"
                              className="tooltiplink"
                              data-toggle="tooltip"
                              data-placement="right"
                              data-html="true"
                              style={{ display: "inline" }}
                            >
                              <FaInfoCircle />
                            </a>
                            for more info.
                          </p>
                        </div>
                      </div>
                      <div className="col-md-12">
                        <p
                          style={{
                            color: "#83C72A",
                            fontSize: 15,
                            fontWeight: "normal",
                          }}
                        >
                          <span style={{ fontSize: "10px" }}>
                            <BsCircle />
                          </span>{" "}
                          If you still have further questions (after reading our
                          green info icons, please use our chat function, in
                          bottom right corner, in blue).
                        </p>
                      </div>
                      <div className="col-md-12">
                        <p
                          style={{
                            color: "#808080",
                            fontSize: 13,
                            fontWeight: "normal",
                          }}
                        >
                          <span style={{ fontSize: "10px" }}>
                            <BsCircle />
                          </span>{" "}
                          Click on{" "}
                          <img
                            src={user}
                            style={{ height: 15, marginTop: "-2px" }}
                          />{" "}
                          (next to each question) to ask delegate user to answer
                          this question. You can also add a new delegate user by
                          clicking this button.&nbsp;&nbsp;&nbsp;&nbsp;
                          <button
                            className="btn btn-previous "
                            style={{
                              textAlign: "center",
                              borderRadius: "5px",
                            }}
                            data-toggle="modal"
                            data-target="#modal"
                          >
                            Create Delegate User
                          </button>
                        </p>
                      </div>
                      {/* <div class="col-md-12">
                                  <p style="color:#83C72A;font-size:15px;"><code>Names of delegates that have been assigned a question will be shown in this way (red background)</code> &nbsp;&nbsp;&nbsp;&nbsp;<button class="btn btn-success" style="text-align:center;" data-toggle="modal" data-target="#modal">Create Delegate  User</button></p>
                              </div> */}
                      <div className="col-md-12 text-center">
                        <form
                          action=""
                          method="post"
                          class="delegate_modal"
                          enctype="multipart/form-dat"
                        >
                          <div
                            className="modal"
                            id="modal"
                            tabIndex={-1}
                            role="dialog"
                            aria-labelledby="exampleModalCenterTitle"
                            aria-hidden="true"
                          >
                            <div
                              className="modal-dialog modal-dialog-centered"
                              role="document"
                            >
                              <div className="modal-content">
                                <div className="modal-header">
                                  <h5
                                    className="modal-title"
                                    id="exampleModalLongTitle"
                                  >
                                    Add Delegate User
                                  </h5>
                                </div>
                                <div className="modal-body">
                                  <div className="form-group">
                                    <label
                                      htmlFor="exampleFormControlTextarea1"
                                      style={{
                                        fontWeight: "normal",
                                        float: "left",
                                      }}
                                    >
                                      Delegate User's Email
                                    </label>
                                    <input
                                      type
                                      className="form-control"
                                      name="delegate_mail"
                                      required
                                    />
                                  </div>
                                </div>
                                <div className="modal-footer">
                                  <button
                                    type="button"
                                    className="btn btn-secondary"
                                    data-dismiss="modal"
                                  >
                                    Close
                                  </button>
                                  <button
                                    type="submit"
                                    className="btn btn-warning"
                                  >
                                    Send Invitation
                                  </button>
                                </div>
                              </div>
                            </div>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>

                  <div className="sage_border">
                    <div className="row">
                      <div className="col-md-12">
                        <p
                          style={{
                            color: "#83C72A",
                            fontSize: 15,
                            fontWeight: "normal",
                          }}
                        >
                          <span style={{ fontSize: "10px" }}>
                            <BsCircle />{" "}
                          </span>{" "}
                          We can get your answers to some of the following
                          questions, if you are a customer of the following
                          partners. Click the box to allow us to get that
                          information from them. Thank you
                        </p>
                      </div>
                      <div className="col-md-12 text-center">
                        <a
                          href="javascript:void();"
                          className="zero-p btn btn-success btn-login-submit"
                          style={{ textAlign: "center", borderRadius: "5px" }}
                        >
                          Already Imported From Sage
                        </a>
                      </div>
                    </div>
                  </div>

                  <div className="sage_border">
                    <div className="row">
                      <div className="col-md-12">
                        <p
                          style={{
                            color: "#83C72A",
                            fontSize: 14,
                            fontWeight: "normal",
                          }}
                        >
                          <span style={{ fontSize: "10px" }}>
                            <BsCircle />{" "}
                          </span>{" "}
                          We can get your answers to some of the following
                          questions, if you are a customer of the following
                          partners. Click the box to allow us to get that
                          information from them. Thank you
                        </p>
                      </div>
                      <div className="col-md-12 text-center">
                        <a
                          href="javascript:void();"
                          className="btn btn-success mb-2"
                          data-toggle="modal"
                          data-target="#myModal"
                          style={{
                            textAlign: "center",
                            borderRadius: "5px",
                            marginRight: "0.5rem",
                          }}
                        >
                          Import using SAGE
                        </a>
                        <a
                          href="#!"
                          class="btn btn-success mb-2"
                          style={{ textAlign: "center", borderRadius: "5px" }}
                        >
                          Imported using XERO{" "}
                        </a>
                      </div>
                    </div>
                  </div>

                  {/* <div style={{ margin: "10px", marginBottom: "30px" }}>
                    <div class="row">
                      <div class="col-md-12">
                        <p
                          style={{
                            color: "#808080",
                            fontSize: "15px",
                            fontWeight: "bpld",
                          }}
                        >
                          <i class="icon-circle-empty"> </i>Give Delegate Access
                          to your team member for answering your question.
                        </p>
                      </div>
                      <div class="col-md-12">
                        <p style={{ color: "#83C72A", fontSize: "15px" }}>
                          <code>
                            Names of delegates that have been assigned a
                            question will be shown in this way (red background)
                          </code>{" "}
                          &nbsp;&nbsp;&nbsp;&nbsp;
                          <button
                            class="btn btn-success"
                            style={{ textAlign: "center" }}
                            data-toggle="modal"
                            data-target="#modal"
                          >
                            Create Delegate User
                          </button>
                        </p>
                      </div>

                      <div class="col-md-12 text-center">
                        <form
                          method="post"
                          class="delegate_modal"
                          enctype="multipart/form-data"
                        >
                          <div
                            class="modal fade"
                            id="modal"
                            tabindex="-1"
                            role="dialog"
                            aria-labelledby="exampleModalCenterTitle"
                            aria-hidden="true"
                          >
                            <div
                              class="modal-dialog modal-dialog-centered"
                              role="document"
                            >
                              <div class="modal-content">
                                <div class="modal-header">
                                  <h5
                                    class="modal-title"
                                    id="exampleModalLongTitle"
                                  >
                                    Add Delegate User
                                  </h5>
                                </div>
                                <div class="modal-body">
                                  <div class="form-group">
                                    <label
                                      for="exampleFormControlTextarea1"
                                      style={{
                                        fontWeight: "normal",
                                        float: "left",
                                      }}
                                    >
                                      Delegate User's Email
                                    </label>
                                    <input
                                      type
                                      class="form-control"
                                      type="email"
                                      name="delegate_mail"
                                      required
                                    />
                                  </div>
                                </div>
                                <div class="modal-footer">
                                  <button
                                    type="button"
                                    class="btn btn-secondary"
                                    data-dismiss="modal"
                                  >
                                    Close
                                  </button>
                                  <button type="submit" class="btn btn-warning">
                                    Send Invitation
                                  </button>
                                </div>
                              </div>
                            </div>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div> */}

                  <div className="progress">
                    <div
                      className="progress-bar progress-bar-striped bg-info"
                      role="progressbar"
                      style={{ width: "15%" }}
                      aria-valuenow={15}
                      aria-valuemin={0}
                      aria-valuemax={100}
                    >
                      {" "}
                      15%{" "}
                    </div>
                  </div>
                  <div
                    style={{
                      marginBottom: 20,
                      marginRight: 20,
                    }}
                  >
                    <span>
                      10 minutes to complete this section, 1 hour in total
                    </span>
                  </div>
                  <form
                    id="qstn"
                    name="questionaire"
                    className="questionaire-form"
                    method="POST"
                    action="<?php echo base_url('questionaire/add_info');?>"
                  >
                    <div className="form_title">
                      <h3>
                        <strong>
                          <span style={{ color: "#ec8c0e" }}>
                            <FaIndustry />
                          </span>
                        </strong>{" "}
                        Industry
                      </h3>
                    </div>

                    <input type="hidden" defaultValue="sage" name="method" />

                    <input type="hidden" defaultValue="sage" name="method" />

                    <input type="hidden" defaultValue="direct" name="method" />

                    <div className="step">
                      <div className="row">
                        <div className="col-md-5 col-sm-5">
                          <div className="form-group">
                            <label>
                              <b>1a</b> What industry are you in?&nbsp;{" "}
                              <span
                                style={{ color: "green", fontSize: "19px" }}
                              >
                                {" "}
                                <FaInfoCircle />{" "}
                              </span>
                              <HiUserCircle />
                              <span style={{ color: "#ec8b0d", fontSize: 22 }}>
                                *
                              </span>
                            </label>

                            {/* <a
                              data-container="body"
                              title="Please select your primary industry from the list <br><br><i>If you have more questions, please ask in 'Search' in top right hand corner of website. Or then ask our team on chat in bottom right corner, or as shown in footer.</i>"
                              href="javascript:void(0);"
                              className="tooltiplink"
                              data-toggle="tooltip"
                              data-placement="right"
                              data-html="true"
                            >
                              <i className="icon-info-circled-3" />
                            </a>
                            <img
                              src="<?php echo base_url();?>images/deligate_icon.png"
                              className="industry_button"
                              style={{
                                height: 15,
                                cursor: "pointer",
                                marginTop: "-2px",
                              }}
                            /> */}
                            <br />
                            <div id="industry_in">
                              <select
                                className="form-control del_industry reset_onchange"
                                style={{
                                  width: "50%",
                                  display: "none",
                                  marginBottom: 10,
                                }}
                                onchange="get_delegate(this.value,'industry_input',<?php echo $this->session->userdata['logged_in']['user_id']; ?>);"
                              >
                                <option selected disabled>
                                  Select Delegate
                                </option>

                                <option value="<?php echo $get_del_name->user_id;?>,<?php echo $del_result->industry_input; ?>">
                                  {/*?php echo $name;?*/}
                                </option>

                                <option
                                  value="add_new_del"
                                  data-toggle="modal"
                                  data-target="#modal"
                                >
                                  Add Delegate User
                                </option>
                              </select>
                            </div>
                          </div>
                        </div>

                        <div className="col-md-7 col-sm-7">
                          <div className="form-group">
                            <select
                              name="industry"
                              id="industry_1a"
                              className="form-control"
                              required
                            >
                              <option disabled selected>
                                Choose an option
                              </option>

                              <option value="Agriculture and Mining">
                                Agriculture and mining
                              </option>
                              <option value="Business Services">
                                Business Services
                              </option>
                              <option value="Computer and Electronics">
                                {" "}
                                Computer and Electronics
                              </option>
                              <option value="Consumer Services">
                                {" "}
                                Consumer Services
                              </option>
                            </select>
                            <div id="1a" />
                          </div>

                          <br />
                        </div>
                      </div>
                      <div className="row">
                        <div className="col-md-5 col-sm-5">
                          <div className="form-group">
                            <label>
                              <b>1a</b> How many employees do you have?&nbsp;{" "}
                              <span
                                style={{ color: "green", fontSize: "19px" }}
                              >
                                {" "}
                                <FaInfoCircle />{" "}
                              </span>
                              <HiUserCircle />
                              <span style={{ color: "#ec8b0d", fontSize: 22 }}>
                                *
                              </span>
                            </label>
                            {/* <a
                              data-container="body"
                              title="Please select 1 of the 3 categories for the number of current employees in your company<br><br><i>If you have more questions, please ask in 'Search' in top right hand corner of website. Or then ask our team on chat in bottom right corner, or as shown in footer.</i>"
                              href="javascript:void(0);"
                              className="tooltiplink"
                              data-toggle="tooltip"
                              data-placement="right"
                              data-html="true"
                            >
                              <i className="icon-info-circled-3" />
                            </a>
                            <img
                              src="<?php echo base_url();?>images/deligate_icon.png"
                              className="employees_button"
                              style={{
                                height: 15,
                                cursor: "pointer",
                                marginTop: "-2px",
                              }}
                            /> */}
                            <br />
                            <div id="employee_havezz">
                              <select
                                className="form-control del_employees reset_onchange"
                                style={{
                                  width: "50%",
                                  display: "none",
                                  marginBottom: 10,
                                }}
                                onchange="get_delegate(this.value,'employees_input',<?php echo $this->session->userdata['logged_in']['user_id']; ?>);"
                              >
                                <option selected disabled>
                                  Select Delegate
                                </option>

                                <option value="<?php echo $get_del_name->user_id;?>">
                                  {/*?php echo $name;?*/}
                                </option>

                                <option
                                  value="add_new_del"
                                  data-toggle="modal"
                                  data-target="#modal"
                                >
                                  Add Delegate User
                                </option>
                              </select>
                              <input
                                type="hidden"
                                className="del_employees_val"
                                defaultValue="<?php echo $del_access->employees_input;?>"
                                style={{ width: 200, height: 30 }}
                              />
                            </div>
                          </div>
                        </div>

                        <div className="col-md-7 col-sm-7">
                          <div className="form-group">
                            <select
                              name="employees"
                              id="employees_1b"
                              className="form-control"
                            >
                              <option disabled selected>
                                Choose an option
                              </option>

                              <option value="1-500" selected>
                                1-500
                              </option>
                              <option value="500-5000">500-5000</option>
                              <option value="5000 >">500</option>

                              <option value="1-500">1-500</option>
                              <option value="500-5000" selected>
                                500-5000
                              </option>
                              <option value="5000 >">500</option>

                              <option value="1-500">1-500</option>
                              <option value="500-5000">500-5000</option>
                              <option value="5000 >" selected>
                                500
                              </option>
                              <option value="1-500"></option>
                            </select>
                            <div id="1b" />
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="form_title">
                      <h3>
                        <strong>
                          <span style={{ color: "#ec8c0e" }}>
                            <ImLocation2 />
                          </span>
                        </strong>{" "}
                        Location&nbsp;
                      </h3>
                    </div>
                    <div className="step">
                      <div className="row">
                        <div className="col-md-5 col-sm-5">
                          <div className="form-group">
                            <label>
                              <b>2</b> Where are you located?&nbsp;{" "}
                              <span
                                style={{ color: "green", fontSize: "19px" }}
                              >
                                {" "}
                                <FaInfoCircle />{" "}
                              </span>
                              <HiUserCircle />
                              <span style={{ color: "#ec8b0d", fontSize: 22 }}>
                                *
                              </span>
                            </label>

                            <br />
                            <div id="located">
                              <select
                                className="form-control del_location reset_onchange"
                                style={{
                                  width: "50%",
                                  display: "none",
                                  marginBottom: 10,
                                }}
                                onchange="get_delegate(this.value,'location_input',<?php echo $this->session->userdata['logged_in']['user_id']; ?>);"
                              >
                                <option selected disabled>
                                  Select Delegate
                                </option>

                                <option value="<?php echo $get_del_name->user_id;?>,<?php echo $del_result->location_input; ?>">
                                  {/*?php echo $name;?*/}
                                </option>

                                <option
                                  value="add_new_del"
                                  data-toggle="modal"
                                  data-target="#modal"
                                >
                                  Add Delegate User
                                </option>
                              </select>
                            </div>
                          </div>
                        </div>
                        <div className="col-md-7 col-sm-7">
                          <div className="form-group">
                            <div className="row">
                              <div className="col-md-6">
                                <p>
                                  Located?&nbsp;
                                  <span
                                    style={{ color: "#ec8b0d", fontSize: 22 }}
                                  >
                                    *
                                  </span>
                                </p>

                                <select
                                  name="located[]"
                                  id="located_2a"
                                  className="form-control"
                                  multiple="multiple"
                                >
                                  <option disabled value>
                                    Northen Ireland
                                  </option>
                                  <option disabled value>
                                    Ireland
                                  </option>
                                  <option disabled value>
                                    Mainland UK
                                  </option>
                                  <option disabled value>
                                    Europe Continenetal
                                  </option>
                                  <option disabled value>
                                    North America
                                  </option>
                                  <option disabled value>
                                    Central America
                                  </option>
                                  <option disabled value>
                                    South America
                                  </option>
                                  <option disabled value>
                                    Africa
                                  </option>
                                  <option disabled value>
                                    Middleast
                                  </option>
                                </select>
                                <div id="2a" />
                              </div>
                              <div className="col-md-6">
                                <p>
                                  Do business in?&nbsp;
                                  <span
                                    style={{ color: "#ec8b0d", fontSize: 22 }}
                                  >
                                    *
                                  </span>
                                </p>
                                <select
                                  name="location_business[]"
                                  id="location_business_2b"
                                  className="form-control"
                                  multiple="multiple"
                                >
                                  <option disabled value>
                                    press ctrl for multiple locations
                                  </option>

                                  <option value="Northern Ireland">
                                    {" "}
                                    Northern Ireland
                                  </option>
                                  <option value="Ireland">
                                    Ireland (Europe)
                                  </option>
                                  <option value="Mainland UK">
                                    Mainland UK
                                  </option>
                                  <option value="Europe">
                                    Europe (Continental)
                                  </option>
                                  <option value="North America"> </option>
                                  <option value="Central America"></option>
                                  <option value="South America">
                                    South America
                                  </option>
                                  <option value="Africa">AFrica</option>
                                  <option value="Middle East Qatar">
                                    Middle East Qatar
                                  </option>
                                  <option value="Middle East Israel">
                                    Middle East Israel
                                  </option>
                                  <option value="Russia">Russia</option>
                                  <option value="South Asia">South Asia</option>
                                  <option value="South East Asia">
                                    South East Asia
                                  </option>
                                  <option value="South Pacific"></option>
                                </select>
                                <div id="2b" />

                                <br />

                                <br />

                                <br />
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="form_title">
                      <h3>
                        <span style={{ color: "#ec8c0e" }}>
                          <FaUsers />
                        </span>
                        {"  "}
                        Supply Chain
                      </h3>
                    </div>
                    <div className="step">
                      <div className="row">
                        <div className="col-md-5 col-sm-5">
                          <div className="form-group">
                            <label>
                              <b>3</b> Do you handle or manage personal or
                              financial data or information for others <br />
                              (e.g. your supply chain or customers)?&nbsp;
                              <span style={{ color: "#ec8b0d", fontSize: 22 }}>
                                *
                              </span>
                            </label>

                            <br />
                            <div id="handlez_data">
                              <select
                                className="form-control del_supply reset_onchange"
                                style={{
                                  width: "50%",
                                  display: "none",
                                  marginBottom: 10,
                                }}
                                onchange="get_delegate(this.value,'handle_data_input',<?php echo $this->session->userdata['logged_in']['user_id']; ?>);"
                              >
                                <option selected disabled>
                                  Select Delegate
                                </option>

                                <option value="<?php echo $get_del_name->user_id;?>,<?php echo $del_result->handle_data_input; ?>">
                                  {/*?php echo $name;?*/}
                                </option>

                                <option
                                  value="add_new_del"
                                  data-toggle="modal"
                                  data-target="#modal"
                                >
                                  Add Delegate User
                                </option>
                              </select>
                            </div>
                          </div>
                        </div>
                        <div className="col-md-7 col-sm-7">
                          <div className="form-group">
                            <select
                              name="handle_data"
                              id="handle_data_3"
                              className="form-control "
                            >
                              <option disabled selected>
                                {" "}
                                choose an option
                              </option>
                              {/* <option value={1} <?php echo ((isset($basics_data->handle_data_input) &amp;&amp; $basics_data;handle_data_input == "1")?'selected':'') Yes </option>
                          <option value={0} <?php echo ((isset($basics_data->handle_data_input) &amp;&amp; $basics_data;handle_data_input == "0")?'selected':'') No </option> */}
                            </select>
                            <div id="3a"></div>
                          </div>
                        </div>
                        <div className="row">
                          {/* <div className="row"> */}
                          <div className="col-md-12">
                            <table className="table table-questionaire">
                              <thead>
                                <tr>
                                  <th />
                                  <th>You are supplier to?</th>
                                  <th>Your customer?</th>
                                </tr>
                              </thead>
                              <tbody>
                                <tr>
                                  <td>Individuals</td>
                                  <td>
                                    <input
                                      type="radio"
                                      name="budget_individual"
                                      defaultValue="supplier"
                                    />
                                  </td>

                                  <td>
                                    <input
                                      type="radio"
                                      name="budget_individual"
                                      defaultValue="customer"
                                    />
                                  </td>
                                </tr>
                                <tr>
                                  <td>Small and medium businesses</td>
                                  <td>
                                    <input
                                      type="radio"
                                      name="budget_individual"
                                      defaultValue="supplier"
                                    />
                                  </td>
                                  <td>
                                    <input
                                      type="radio"
                                      name="budget_individual"
                                      defaultValue="supplier"
                                    />
                                  </td>
                                </tr>
                                <tr>
                                  <td>Enterprise</td>
                                  <td>
                                    <input
                                      type="radio"
                                      name="budget_individual"
                                      defaultValue="supplier"
                                    />
                                  </td>
                                  <td>
                                    <input
                                      type="radio"
                                      name="budget_individual"
                                      defaultValue="supplier"
                                    />
                                  </td>
                                </tr>
                                <tr>
                                  <td>Governments</td>
                                  <td>
                                    <input
                                      type="radio"
                                      name="budget_individual"
                                      defaultValue="supplier"
                                    />
                                  </td>
                                  <td>
                                    <input
                                      type="radio"
                                      name="budget_individual"
                                      defaultValue="supplier"
                                    />
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                          </div>
                          {/* End col-md-12*/}
                          {/* </div> */}
                        </div>
                      </div>
                      <div className="btn-3" style={{ padding: 10 }}>
                        <div>
                          <button
                            name="save_continue"
                            style={{ borderRadius: "5px" }}
                            type="submit"
                            value="continue"
                            className="btn btn-previous btn-medium mb-2"
                            onclick="skip_error();"
                          >
                            Skip
                          </button>
                        </div>
                        <div>
                          <button
                            name="save_continue"
                            type="submit"
                            value="return"
                            style={{
                              borderRadius: "5px",
                            }}
                            className="btn btn-warning btn-medium mb-2"
                          >
                            Save and Return
                          </button>
                        </div>
                        <div>
                          <button
                            name="save_continue"
                            style={{ borderRadius: "5px" }}
                            type="submit"
                            value="continue"
                            className="btn btn-success btn-medium"
                            onclick="skip_error();"
                          >
                            Save and Continue
                          </button>
                        </div>
                      </div>
                    </div>
                  </form>
                  {/*---------------------------------------------------------Step 2--------------------------------------------------------------------------------*/}
                  <div className="tab-pane" id="profile" />
                  {/*---------------------------------------------------------Step 3--------------------------------------------------------------------------------*/}
                  <div className="tab-pane" id="messages" />
                  {/*---------------------------------------------------------------------Step 4--------------------------------------------------------------------*/}
                  <div className="tab-pane" id="settings" />
                </div>
              </div>
              {/* End col-md-12*/}
            </div>
            {/* End row */}
          </div>
          {/* End container */}
          <div className="modal fade" id="myModal" role="dialog">
            <div className="modal-dialog">
              <div className="modal-content">
                <div className="modal-header">
                  <button type="button" className="close" data-dismiss="modal">
                    ×
                  </button>
                  <h4 className="modal-title">Import From Sage</h4>
                </div>
                <div className="modal-body">
                  <div className="container-fluid">
                    <div className="row">
                      <div
                        className="col-md-6 ml-auto"
                        style={{ border: "1px solid #ccc", padding: 30 }}
                      >
                        <img
                          src="<?php echo base_url();?>images/sageone.jpg"
                          style={{ height: 110, width: 110, marginLeft: 50 }}
                        />
                        <a
                          href="<?php echo $redirect_url;?>"
                          className="btn btn-success"
                          style={{ textAlign: "center" }}
                        >
                          Import From Sage Accounting
                        </a>
                      </div>
                      <div
                        className="col-md-6 ml-auto"
                        style={{ border: "1px solid #ccc", padding: 30 }}
                      >
                        <img
                          src="<?php echo base_url();?>images/salesforce.png"
                          style={{ height: 110, width: 110, marginLeft: 50 }}
                        />
                        <a
                          href="<?php echo $auth_url;?>"
                          className="btn btn-success"
                          style={{ textAlign: "center" }}
                        >
                          Import From Sage Financials
                        </a>
                      </div>
                      <div
                        className="col-md-3 ml-auto"
                        style={{ padding: 30 }}
                      />
                    </div>
                  </div>
                </div>
                <div className="modal-footer">
                  <button
                    type="button"
                    className="btn btn-default"
                    data-dismiss="modal"
                  >
                    Close
                  </button>
                </div>
              </div>
            </div>
          </div>
          <div
            className="modal fade"
            id="alert_modal"
            tabIndex={-1}
            role="dialog"
            aria-labelledby="exampleModalCenterTitle"
            aria-hidden="true"
          >
            <div className="modal-dialog modal-dialog-centered" role="document">
              <div className="modal-content">
                <div className="modal-body">
                  <div className="form-group">
                    <h3 style={{ textAlign: "center" }}>
                      You have already assign this delegate user for this
                      question!
                    </h3>
                  </div>
                </div>
                <div
                  className="modal-footer"
                  style={{
                    borderTop: "1px solid white",
                    textAlign: "center",
                    marginTop: "-25px",
                  }}
                >
                  <button
                    type="button"
                    className="btn btn-secondary"
                    data-dismiss="modal"
                  >
                    Close
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </main>
      <Footer2 />
    </div>
  );
};

export default Questionaire;
