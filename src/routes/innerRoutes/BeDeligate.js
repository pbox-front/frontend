import React, { Fragment } from "react";
import Footer2 from "../../components/Footer2";
import Header2 from "./../../components/Header2";
import { BsCircle } from "react-icons/bs";
import { Form } from "react-bootstrap";
import DataTable from "react-data-table-component";
const datas = [
  {
    id: 1,
    sno: 1,
    sn: "	Kiran Bhagotra",
    se: "	kbbhagotra@yahoo.com",
    sp: "	SMB Phone",
    sq: (
      <Fragment>
        {" "}
        <button style={{ width: "100%" }} className="btn-success btn-table">
          Basics
        </button>
        <button
          style={{ width: "100%" }}
          className="btn btn-success  btn-login-submit"
        >
          Technical
        </button>
      </Fragment>
    ),
    id: "12/11/2020",
  },
];
const columns = [
  {
    name: "Serial Number",
    selector: "sno",
    sortable: true,
  },
  {
    name: "SMB Name",
    selector: "sn",
    sortable: true,
    right: true,
  },
  {
    name: "SMB Email",
    selector: "se",
    sortable: true,
    right: true,
  },
  {
    name: "SMB Phone",
    selector: "sp",
    sortable: true,
    right: true,
  },
  {
    name: "SMB Questionnaire Answers",
    selector: "sq",
    sortable: true,
    right: true,
  },
  {
    name: "Invite Date",
    selector: "id",
    sortable: true,
    right: true,
  },
];

const BeDeligate = () => {
  return (
    <div>
       <section className="pb-section slider-main">
        <Header2 />
      </section>
      <section
        id="sub_header"
        style={{
          background: "#f5f5f5",
          boxShadow: "inset 0 1px 10px 1px rgba(0,0,0,.41)",
        }}
      >
        <div className="container">
          <div
            className="main_title"
            style={{
              background: "none",
              textAlign: "center",
              fontSize: 40,
              color: "#000",
              bottom: 30,
            }}
          >
            Be a Delegate
          </div>
        </div>
      </section>

      <main>
        <div className="container margin_60">
          <div className="row">
            <div className="col-md-12">
              <div className="tab-content rounded_div">
                <ul className="cupon-btn">
                  <li>
                    <button>Copy</button>
                  </li>
                  <li>
                    <button>CSV</button>
                  </li>
                  <li>
                    <button>Excel</button>
                  </li>
                  <li>
                    <button>Print</button>
                  </li>
                </ul>
                <DataTable
                  title="All Sales"
                  columns={columns}
                  data={datas}
                  subHeader={true}
                  subHeaderAlign={"left"}
                  pagination
                  compact={true}
                  wrap={true}
                  subHeaderComponent={
                    <div style={{ display: "flex", alignItems: "center" }}>
                      <Form.Group
                        style={{
                          display: "flex",
                          alignItems: "center",
                        }}
                        controlId="exampleForm.ControlSelect1"
                      >
                        <Form.Label>Show</Form.Label>
                        <Form.Control
                          as="select"
                          style={{ margin: "0 0.5rem" }}
                        >
                          <option>1</option>
                          <option>2</option>
                          <option>3</option>
                          <option>4</option>
                          <option>5</option>
                        </Form.Control>
                        <Form.Label>Entries</Form.Label>
                      </Form.Group>
                    </div>
                  }
                />
              </div>
            </div>
          </div>
        </div>
      </main>
      <Footer2 />
    </div>
  );
};

export default BeDeligate;
