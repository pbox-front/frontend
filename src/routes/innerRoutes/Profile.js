import React from "react";
import Header2 from "./../../components/Header2";
import { AiOutlineEyeInvisible } from "react-icons/ai";
import Footer2 from "./../../components/Footer2";

import {
  FaInfoCircle,
  FaBriefcase,
  FaLock,
  FaUserCircle,
} from "react-icons/fa";
import { VscLocation } from "react-icons/vsc";
import { ImLocation2 } from "react-icons/im";

const Profile = () => {
  return (
    <div>
      <div id="load"></div>
      <section className="pb-section slider-main">
        <Header2 />
      </section>
      <section
        id="sub_header"
        style={{
          background: "#f5f5f5",
          boxShadow: "inset 0 1px 10px 1px rgba(0,0,0,.41)",
        }}
      >
        <div className="container">
          <div className="main_title other_title ">My Profile</div>
        </div>
      </section>
      <main>
        <div className="container margin_60">
          <div className="row">
            <div className="col-md-12">
              <div className="tab-content rounded_div profile">
                <form name="profile" method="POST">
                  <div className="form_title">
                    <h3 className="icon-wrapper">
                      <span className="icon-size">
                        <FaUserCircle />
                      </span>
                      <label className="icon">Personal info</label>
                    </h3>
                  </div>
                  <div className="step">
                    <div className="row">
                      <div className="col-md-6 col-sm-6">
                        <div className="form-group">
                          <label>
                            First name{" "}
                            <span style={{ color: "#ec8b0d", fontSize: 22 }}>
                              *
                            </span>
                          </label>
                          <input
                            type="text"
                            className="form-control"
                            name="firstname"
                            maxLength={30}
                          />
                        </div>
                      </div>
                      <div className="col-md-6 col-sm-6">
                        <div className="form-group">
                          <label>
                            Last name{" "}
                            <span style={{ color: "#ec8b0d", fontSize: 22 }}>
                              *
                            </span>
                          </label>
                          <input
                            type="text"
                            className="form-control"
                            name="lastname"
                            maxLength={30}
                          />
                        </div>
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-md-6 col-sm-6">
                        <div className="form-group">
                          <label>
                            Email{" "}
                            <span style={{ color: "#ec8b0d", fontSize: 22 }}>
                              *
                            </span>
                          </label>
                          <input
                            type="email"
                            className="form-control"
                            name="email"
                          />
                        </div>
                      </div>
                      <div className="col-md-6 col-sm-6">
                        <div className="form-group">
                          <label>
                            Telephone{" "}
                            <span style={{ color: "#ec8b0d", fontSize: 22 }}>
                              *
                            </span>
                            <a
                              data-container="body"
                              style={{ display: "inline" }}
                              title="Please enter without any gaps and country code eg +4402080000000 Or +97316670000."
                              href="javascript:void(0);"
                              className="tooltiplink"
                              data-toggle="tooltip"
                              data-placement="right"
                              data-html="true"
                            >
                              <span style={{ color: "green" }}>
                                <FaInfoCircle />
                              </span>
                            </a>
                          </label>
                          <input
                            type="text"
                            className="form-control"
                            name="phone"
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="form_title">
                    <h3 className="icon-wrapper">
                      <strong className="icon-size">
                        <FaBriefcase />
                      </strong>
                      <span className="icon"> Company info</span>
                    </h3>
                  </div>
                  <div className="step">
                    <div className="row">
                      <div className="col-md-12 col-sm-12">
                        <div className="form-group">
                          <label>
                            Company Name{" "}
                            <span style={{ color: "#ec8b0d", fontSize: 22 }}>
                              *
                            </span>
                          </label>
                          <input
                            type="text"
                            className="form-control"
                            name="company_name"
                            maxLength={60}
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="form_title">
                    <h3 className="icon-wrapper">
                      <strong className="icon-size">
                        <FaLock />
                      </strong>
                      <span className="icon">Change Password</span>
                    </h3>
                  </div>
                  <div className="step">
                    <div className="row">
                      <div className="col-md-6 col-sm-6">
                        <div className="form-group password-wrapper">
                          <label>New Password</label>
                          <input
                            type="password"
                            className="form-control pass1"
                            id="myPassword"
                            name="new_password"
                          />
                          <a href="#!" className="eye">
                            <AiOutlineEyeInvisible />
                          </a>
                        </div>
                      </div>
                      <div className="col-md-6 col-sm-6">
                        <div className="form-group password-wrapper">
                          <label>Confirm Password</label>
                          <input
                            type="password"
                            className="form-control pass2"
                            id="myConfirmPassword"
                            name="confirm_password"
                          />
                          <a href="#!" className="eye">
                            <AiOutlineEyeInvisible />
                          </a>
                        </div>
                      </div>
                      <div id="errors" className="col-md-12">
                        <div className="pre">
                          Password must contain at least 1 Upper Case Letters.{" "}
                          <br />
                          Password must contain at least 1 Digits. Password must{" "}
                          <br />
                        </div>
                      </div>
                    </div>
                  </div>

                  <div className="form_title">
                    <h3 className="icon-wrapper">
                      <strong className="icon-size">
                        <ImLocation2 />
                      </strong>
                      <span className="icon">Your Address</span>
                    </h3>
                  </div>
                  <div className="step">
                    <div className="row">
                      <div className="col-md-6 col-sm-6">
                        <div className="form-group">
                          <label>
                            Address Line1&nbsp;
                            <span style={{ color: "#ec8b0d", fontSize: 22 }}>
                              *
                            </span>
                          </label>
                          <input name="address1" className="form-control" />
                        </div>
                      </div>
                      <div className="col-md-6 col-sm-6">
                        <div className="form-group">
                          <label>Address Line2</label>
                          <input name="address2" className="form-control" />
                        </div>
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-md-6 col-sm-6">
                        <div className="form-group">
                          <label>
                            City&nbsp;
                            <span style={{ color: "#ec8b0d", fontSize: 22 }}>
                              *
                            </span>
                          </label>
                          <input name="city" className="form-control" />
                        </div>
                      </div>
                      <div className="col-md-6 col-sm-6">
                        <div className="form-group">
                          <label>
                            State/Province&nbsp;
                            <span style={{ color: "#ec8b0d", fontSize: 22 }}>
                              *
                            </span>
                            <a
                              data-container="body"
                              style={{ display: "inline" }}
                              title='Please enter the state/province code without any gaps (like: For Florida, please type "FL")'
                              href="javascript:void(0);"
                              className="tooltiplink"
                              data-toggle="tooltip"
                              data-placement="right"
                              data-html="true"
                            >
                              <span style={{ color: "green" }}>
                                <FaInfoCircle />
                              </span>
                            </a>
                          </label>
                          <input
                            name="state_province"
                            className="form-control"
                            maxLength={2}
                          />
                        </div>
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-md-6 col-sm-6">
                        <div className="form-group">
                          <label>
                            Postal Code&nbsp;
                            <span style={{ color: "#ec8b0d", fontSize: 22 }}>
                              *
                            </span>
                          </label>
                          <input name="postal_code" className="form-control" />
                        </div>
                      </div>
                      <div className="col-md-6 col-sm-6">
                        <div className="form-group">
                          <label>
                            Country&nbsp;
                            <span style={{ color: "#ec8b0d", fontSize: 22 }}>
                              *
                            </span>
                          </label>
                          <select
                            className="selectpicker form-control"
                            data-live-search="true"
                            name="country"
                            data-dropup-auto="false"
                          >
                            <option value selected disabled>
                              Please Select
                            </option>
                          </select>
                        </div>
                      </div>
                    </div>

                    <div className="row">
                      <div className="col-md-12 col-sm-12">
                        <div className="form-group">
                          <input
                            className="mx-2"
                            type="checkbox"
                            name="receive_email"
                          />
                          <label>
                            I want to receive emails from ProtectBox in future
                          </label>
                        </div>
                      </div>
                    </div>
                    {/*?php
                      }
                      ?*/}
                    {/*<div class="row">
                          <div class="col-md-12 col-sm-12">
                            <div class="form-group">
                              <label>Click <a href="<?php echo base_url("manage_delegates");?>">here</a> to manage your delegates <a data-container="body" title="Delegate user is not the company's main account holder but is authorised by main account holder to perform actions on their behalf" href="javascript:void(0);" class="tooltiplink" data-toggle="tooltip" data-placement="right" data-html="true"><i class="icon-info-circled-3"></i></a></label>
                            </div>
                          </div>
                      </div>*/}
                    <div style={{ textAlign: "right" }} className="form-group">
                      <button
                        type="submit"
                        className="btn btn-success "
                        style={{ borderRadius: "5px" }}
                      >
                        Update Profile
                      </button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </main>
      <Footer2 />
    </div>
  );
};

export default Profile;
