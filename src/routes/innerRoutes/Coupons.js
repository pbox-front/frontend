import React from "react";
import Header2 from "./../../components/Header2";
import Footer2 from "./../../components/Footer2";
import DataTable from "react-data-table-component";
import { Form } from "react-bootstrap";

const datas = [
  {
    id: 1,
    sl: 1,
    cc: "coupon_code",
    dt: "	discount_type",
    dv: "	discount_value",
    ac: <button className=" btn-success btn-table">Delete</button>,
  },
];
const columns = [
  {
    name: "SL No.",
    selector: "sl",
    sortable: true,
  },
  {
    name: "Coupon Code",
    selector: "cc",
    sortable: true,
    right: true,
  },
  {
    name: "Discount Type",
    selector: "dt",
    sortable: true,
    right: true,
  },
  {
    name: "Discount Value",
    selector: "dv",
    sortable: true,
    right: true,
  },
  {
    name: "Action",
    selector: "ac",
    sortable: true,
    right: true,
  },
];

const Cupons = () => {
  return (
    <div>
      <div id="load"></div>
      <section className="pb-section slider-main">
        <Header2 />
      </section>
      <section
        id="sub_header"
        style={{
          background: "#f5f5f5",
          boxShadow: "inset 0 1px 10px 1px rgba(0,0,0,.41)",
        }}
      >
        <div className="container">
          <div
            className="main_title"
            style={{
              background: "none",
              textAlign: "center",
              fontSize: 40,
              color: "#000",
              bottom: 30,
            }}
          >
            View all coupons
          </div>
        </div>
      </section>

      <main>
        <div className="container margin_60">
          <div className="row">
            <div className="col-md-12">
              <div id="alertzzzz" style={{ display: "none" }}>
                <div className="alert alert-success">
                  <strong>Coupon added successfully!</strong>{" "}
                </div>
              </div>

              <div className="tab-content rounded_div">
                <div className=" table-responsive">
                  <h1>
                    <a
                      href="#!"
                      style={{ borderRadius: "5px", marginTop: "20px" }}
                      class="coupons-buttons"
                    >
                      Create New Coupon
                    </a>
                  </h1>
                  <ul className="cupon-btn">
                    <li>
                      <button>Copy</button>
                    </li>
                    <li>
                      <button>CSV</button>
                    </li>
                    <li>
                      <button>Excel</button>
                    </li>
                    <li>
                      <button>Print</button>
                    </li>
                  </ul>

                  <DataTable
                    title="All Sales"
                    columns={columns}
                    data={datas}
                    subHeader={true}
                    subHeaderAlign={"left"}
                    pagination
                    compact={true}
                    wrap={true}
                    subHeaderComponent={
                      <div style={{ display: "flex", alignItems: "center" }}>
                        <Form.Group
                          style={{
                            display: "flex",
                            alignItems: "center",
                          }}
                          controlId="exampleForm.ControlSelect1"
                        >
                          <Form.Label>Show</Form.Label>
                          <Form.Control
                            as="select"
                            style={{ margin: "0 0.5rem" }}
                          >
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                            <option>5</option>
                          </Form.Control>
                          <Form.Label>Entries</Form.Label>
                        </Form.Group>
                      </div>
                    }
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </main>
      <Footer2 />
    </div>
  );
};

export default Cupons;
