import React, { Component } from "react";
import Header2 from "./../../components/Header2";
import Footer2 from "./../../components/Footer2";
import { Accordion, Card, Button } from "react-bootstrap";
export default class Sales extends Component {
  render() {
    return (
      <div id="load">
         <section className="pb-section slider-main">
        <Header2 />
      </section>
        <section
          id="sub_header"
          style={{
            background: "#f5f5f5",
            boxShadow: "inset 0 1px 10px 1px rgba(0,0,0,.41)",
          }}
        >
          <div className="container">
            <div
              className="main_title"
              style={{
                background: "none",
                textAlign: "center",
                fontSize: 40,
                color: "#000",
                bottom: 30,
              }}
            >
              Subscription
            </div>
          </div>
        </section>
        <main className="subscription">
          <div className="container margin_60">
            <div className="row">
              <div className="col-md-12">
                <Accordion>
                  <Card>
                    <Card.Header>
                      <Accordion.Toggle as={Button} variant="link" eventKey="1">
                        <p
                          className="collapse-flex"
                          style={{ marginBottom: 0 }}
                        >
                          <a
                            data-toggle="collapse"
                            data-parent="#accordion"
                            href="#collapse4"
                            aria-expanded="false"
                            className="collapsed"
                            style={{
                              fontSize: 20,
                              fontWeight: "bold",
                              color: "#262626",
                              paddingLeft: "0",
                            }}
                          >
                            Comparisons
                          </a>
                          <span
                            className="spanarrow rarrow"
                            style={{ float: "right", marginLeft: "5px" }}
                          >
                            <i
                              className="fa fa-arrow-right"
                              aria-hidden="true"
                            />
                          </span>
                        </p>
                      </Accordion.Toggle>
                    </Card.Header>
                    <Accordion.Collapse eventKey="1">
                      <Card.Body>Comparisons</Card.Body>
                    </Accordion.Collapse>
                  </Card>
                  <Card>
                    <Card.Header>
                      <Accordion.Toggle as={Button} variant="link" eventKey="2">
                        <p
                          className="collapse-flex"
                          style={{ marginBottom: 0 }}
                        >
                          <a
                            data-toggle="collapse"
                            data-parent="#accordion"
                            href="#collapse4"
                            aria-expanded="false"
                            className="collapsed"
                            style={{
                              fontSize: 20,
                              fontWeight: "bold",
                              color: "#262626",
                              paddingLeft: "0",
                            }}
                          >
                            Orders
                          </a>
                          <span
                            className="spanarrow rarrow"
                            style={{ float: "right", marginLeft: "5px" }}
                          >
                            <i
                              className="fa fa-arrow-right"
                              aria-hidden="true"
                            />
                          </span>
                        </p>
                      </Accordion.Toggle>
                    </Card.Header>
                    <Accordion.Collapse eventKey="2">
                      <Card.Body>Orders</Card.Body>
                    </Accordion.Collapse>
                  </Card>
                  <Card>
                    <Card.Header>
                      <Accordion.Toggle as={Button} variant="link" eventKey="3">
                        <p
                          className="collapse-flex"
                          style={{ marginBottom: 0 }}
                        >
                          <a
                            data-toggle="collapse"
                            data-parent="#accordion"
                            href="#collapse4"
                            aria-expanded="false"
                            className="collapsed"
                            style={{
                              fontSize: 20,
                              fontWeight: "bold",
                              color: "#262626",
                              paddingLeft: "0",
                            }}
                          >
                            News
                          </a>
                          <span
                            className="spanarrow rarrow"
                            style={{ float: "right", marginLeft: "5px" }}
                          >
                            <i
                              className="fa fa-arrow-right"
                              aria-hidden="true"
                            />
                          </span>
                        </p>
                      </Accordion.Toggle>
                    </Card.Header>
                    <Accordion.Collapse eventKey="3">
                      <Card.Body>News</Card.Body>
                    </Accordion.Collapse>
                  </Card>
                </Accordion>

                <div className="mainsearchdiv2" id="accordion">
                  <div className="row">
                    <div className="col-md-12" style={{ padding: 0 }}>
                      <div className="tab-content rounded_div">
                        <div className="col-md-12 text-center">
                          <div className="col-md-12"></div>
                          <div className="col-md-12 text-center">
                            <div className="row">
                              <div className="col-md-12 col-sm-5">
                                <div className="form-group">
                                  <form
                                    name="copnsss"
                                    action="https://staging.protectbox.com/questionniare_results/addcoupn"
                                    method="POST"
                                  >
                                    <div
                                      className="details"
                                      style={{ textAlign: "right" }}
                                    >
                                      Do you have coupon code?
                                      <input
                                        type="text"
                                        defaultValue
                                        name="subscr_coupon_code"
                                        className="clss_cpn_code"
                                        placeholder="Enter your coupon code"
                                        style={{
                                          padding: 5,
                                          background: "#ccff99",
                                          border: "1px solid #CCC",
                                        }}
                                      />
                                      <input
                                        type="submit"
                                        value="Apply"
                                        name="sub"
                                        style={{ padding: 6 }}
                                      />
                                    </div>
                                  </form>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div className="col-md-12 text-center">
                            <p
                              style={{
                                color: "#EC8C0E",
                                fontSize: 20,
                                fontWeight: "bold",
                              }}
                            >
                              SUBSCRIBE for £&nbsp;10 per month for access to
                              your answers &amp; our tool!
                            </p>
                          </div>

                          <div className="row">
                            <div className="col-md-5 col-sm-5">
                              <div className="form-group">
                                <div id="paypal-button-container-subscribe" />
                              </div>
                            </div>
                            <div className="col-md-2 col-sm-2">
                              <div className="form-group">
                                <p style={{ fontSize: 30 }}> OR </p>
                              </div>
                            </div>
                            <div className="col-md-5 col-sm-5">
                              <div className="form-group">
                                <form
                                  action="https://staging.protectbox.com/order_process/smb_subscription_stripe"
                                  method="POST"
                                >
                                  .
                                </form>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </main>
        <Footer2 />
      </div>
    );
  }
}
