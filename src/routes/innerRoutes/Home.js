import React, { useEffect } from "react";
import { NavLink, Link } from 'react-router-dom';
import Slider from "react-slick";
// import "slick-carousel/slick/slick.css";
// import "slick-carousel/slick/slick-theme.css";
import Footer from "./../../components/Footer";
import Header from "./../../components/Header";
import ExistingNews from "./../../components/ExistingNews";
import UseCases from "./../../components/UseCases";
import AwardWinning from "./../../components/AwardWinning";
import LastInsidesAndNews from "./../../components/LastInsidesAndNews";
import LatestInsides from "./../../components/LatestInsides";
import { Carousel } from "react-bootstrap";
import google from "./../../../src/images2/google-play.png";
import apple from "./../../../src/images2/apple_store.png";

import { FaRegHandshake, FaEnvelope } from "react-icons/fa";
import { ImFilesEmpty } from "react-icons/im";
import { GiGroundSprout, GiVintageRobot, GiCoffeeCup } from "react-icons/gi";
import { BiRadio } from "react-icons/bi";
import { AiOutlineSmile, AiOutlineUserAdd } from "react-icons/ai";
import { FaUserCog, FaUsers, FaMedal, FaUnlink, FaFreebsd} from "react-icons/fa";
import { BiHelpCircle } from "react-icons/bi";
import { BsCardChecklist } from "react-icons/bs";
import { OverlayTrigger, Button, Tooltip } from "react-bootstrap";

const Home = () => {

  useEffect(() => {
    document.title = "ProtectBox | Home"
  }, [])

  var styles = {
    mainCarouselItem: {
      minHeight: "80vh",
      background: "no-repeat center center scroll",
      backgroundSize: "cover",
    },
    SliderItem1: {
      minHeight: "80vh",
      backgroundImage: `url("image/f1.jpg")`
    },
    SliderItem2: {
      minHeight: "80vh",
      backgroundImage: `url("image/f3.jpg")`
    },
    SliderItem3: {
      minHeight: "80vh",
      backgroundImage: `url("image/f2.jpg")`
    },
    HowWorkIcon: {
      width: "40px",
      height: "70px"
    }
  };
  
  return (
    <div className="wrapper home">
      <section className="pb-section slider-main">
        <Header />
        <div id="slider-section-main" className="slider-section-main">
          <Carousel interval={5000} indicators={false} controls={false}>
            <Carousel.Item  style={{ ...styles.mainCarouselItem, ...styles.SliderItem1 }}>
            </Carousel.Item>
            <Carousel.Item  style={{ ...styles.mainCarouselItem, ...styles.SliderItem2 }}>
            </Carousel.Item>
            <Carousel.Item  style={{ ...styles.mainCarouselItem, ...styles.SliderItem3 }}>
            </Carousel.Item>
          </Carousel>          
					<div className="slider-text">
            <h1 className="pbh-2 white-color">AI Cybersecurity Comparison Marketplace</h1>
            <div className="banner-descrition">
							<p>Our award-winning responsible AI is making cybersecurity accessible for small and medium businesses everywhere,  whether you are technical or not.</p>
							<p>We're helping Suppliers too, by matching them with actual sales (not just leads) quickly, simply and fairly.</p>
							<p>First and only marketplace in the world that let's buyers buy bundled & matched solutions,<br/> for Security (& more) as a Service.</p>
						</div>
            <Link className="btn btn-primary" to="/Login">Protect my business</Link>
            <Link className="btn btn-secondary ml-1" to="/Login">Feature my business</Link>
          </div>
        </div>
      </section>
      <article className="pb-section mobile-app">
        <div className="container">
          <div className="row">
            <div className="col-12 col-md-6">
              <h1 className="pbh-2">Download the ProtectBox App Now</h1>
            </div>
            <div className="col-12 col-md-6">
              <div className="playstore-applestore">
                <a
                  className="inline"
                  href="https://play.google.com/store/apps/details?id=com.security.protectbox"
                  target="_blank"
                >
                  <img src={google} />
                </a>
                <a
                  className="inline"
                  href="https://apps.apple.com/in/app/protectbox/id1514550015"
                  target="_blank"
                >
                  <img src={apple} />
                </a>
              </div>
            </div>
          </div>
        </div>
      </article>
      <section className="pb-section gray-bg pb-content-section ">
        <div className="container">
          <div className="content-box">
            <h1 className="pbh-3">
              Cybercrime impacts every business at some stage
            </h1>
            <center>
              {" "}
              <p>
                Cybercriminals are getting smarter and with our reliance on
                technologies to run our businesses, these attacks threaten our
                very survival.
              </p>
            </center>
            <p>
              Our award-winning, responsible AI makes it easy for techies and
              non-techies to find the perfect match for all your cybersecurity
              needs, in one place.
            </p>
          </div>
        </div>
      </section>
      <article className="pb-section pb-home-security">
        <div className="container">
          <h1 className="pbh-3">
            Marketplace (Cybersecurity &amp; more) as a service
          </h1>
          <div className="row">
            <div className="col-12 col-md-6 px-1">
              <p>
                ProtectBox is the go-to platform for businesses looking for
                jargon-free cybersecurity bundles to protect every area of their
                business. We have built ProtectBox with your specific
                frustrations and needs in mind.
              </p>
              <p>
                {" "}
                Our pioneering search and comparison engine has been developed
                by a team of cyber and data experts from government and industry
                backgrounds. We also partner with orgnaisations who are the
                voice of small businesses.
              </p>
              <p>
                {" "}
                Our free comparison tool uses our award-winning algorithms to
                find the perfect cybersecurity bundles tailored to your
                business' needs.
              </p>
              <p>
                No bias. No jargon. Just a fast, easy to understand solution to
                your problem.
              </p>
            </div>
            <div className="col-12 col-md-6 px-2">
              <div className="pb-video">
                <div className="video-overlay">
                  <div className="video-icon">
                    <a
                      id="videobtn"
                      target="_blank"
                      className="video-btn white"
                      href="https://youtu.be/vWoNJ3nwzXs"
                    >
                    <i className="fa fa-play" />
                    </a>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-12 col-md-12">
              <div className="pb-btn-box">
                <a className="btn btn-secondary" href="/Login">
                  Protect my business
                </a>
              </div>
            </div>
          </div>
        </div>
      </article>
      <article className="pb-section gray-bg pb-testimonial">
        <div className="container">
          <h1 className="pbh-3">What our customers say about us</h1>
          <div className="row">
            <div className="col-12 col-md-10">
              <div className="home-testimonial">
                <Slider
                  slidesToShow={1}
                  swipeToSlide={true}
                  autoplay={true}
                  dots={false}
                  slidesToScroll={true}
                  infinite={true}
                >
                  <div className="item">
                    <div className="testimonial-box ">
                      <h3>“Winner of Access Stage.. comparisons for SMEs trying to choose security”</h3>
                      <h3 className="content-body">Wired<a href="http://www.wired.co.uk" target="_blank" className="content-body-link" rel="nofollow"> http://www.wired.co.uk </a>- 100th issue</h3>
                    </div>
                  </div>
                  <div className="item">
                    <div className="testimonial-box">
                      <h3>“new simplicity approach, helping businesses become secure“</h3>
                      <h3 className="content-body">Gibrahn Verdult - Managing Partner<a href="https://www.venpropartners.com" target="_blank" rel="nofollow" className="content-body-link" > www.venpropartners.com </a>(Fintech, USA)</h3>
                    </div>
                  </div>
                  <div className="item">
                    <div className="testimonial-box">
                      <h3>“business leaders can take action themselves ...says Mary Portas“</h3>
                      <h3 className="content-body">The Telegraph<a href="https://www.telegraph.co.uk" target="_blank" rel="nofollow" className="content-body-link" > www.telegraph.co.uk </a>Mary Portas puts SMEs under the cybersecurity spotlight</h3>
                    </div>
                  </div>
                  <div className="item">
                    <div className="testimonial-box">
                      <h3>“easy to use and would use this service again“</h3>
                      <h3 className="content-body">Dato’ Dr Nick Boden and Dato’ Arif Abdullah, co-founders,<a href="https://www.klean.asia/" target="_blank" rel="nofollow" className="content-body-link" > www.klean.my </a> (Envirotech, Asia-Pacific)</h3>
                    </div>
                  </div>
                  <div className="item">
                    <div className="testimonial-box">
                      <h3>“far removed from your typical ‘geek’ “ </h3>
                      <h3 className="content-body">The Times<a href="https://www.thetimes.co.uk/" target="_blank" rel="nofollow"  className="content-body-link" > www.thetimes.co.uk </a>in Lloyds Bank National Business Awards 2018 Supplement</h3>
                    </div>
                  </div>
                  <div className="item">
                    <div className="testimonial-box">
                      <h3>“weren't sure what to do, support helped us delegate questions”</h3>
                      <h3 className="content-body">Ifeyinwa Kanu - CEO/Founder,<a href="https://intellidigest.com/" target="_blank" rel="nofollow" className="content-body-link" > https://intellidigest.com </a>(Envirotech, UK)</h3>
                    </div>
                  </div>
                  <div className="item">
                    <div className="testimonial-box">
                      <h3>“quickly found best providers for us to keep our customer's secure”</h3>
                      <h3 className="content-body">Andrew Irvine - CEO/Founder,<a href="https://www.tuskcapital.com/" target="_blank" rel="nofollow" className="content-body-link" > www.tuskcapital.com </a> (Fintech, Europe)</h3>
                    </div>
                  </div>
                  <div className="item">
                    <div className="testimonial-box">
                      <h3>“encourage partners to support...change in ‘game changer’ “</h3>
                      <h3 className="content-body">Omar Al Busaidy - World Economic Forum (WEF) Global Shaper (Middle East)</h3>
                    </div>
                  </div>
                </Slider>
              </div>
            </div>
          </div>
        </div>
      </article>
      <section className="pb-section why-protect-box">
        <div className="container">
          <h1 className="pbh-3">Why use ProtectBox?</h1>
          <div className="pb-row">
            <div className="pb-col-12 pb-col-md-2">
              <div className="pb-block">
                <i className="flaticon-sport-team"></i>
                <p>Perfect match recommendations for your business' needs</p>
              </div>
            </div>
            <div className="pb-col-12 pb-col-md-2">
              <div className="pb-block">
                <i className="flaticon-files"></i>
                <p>Jargon-free and user-friendly</p>
              </div>
            </div>
            <div className="pb-col-12 pb-col-md-2">
              <div className="pb-block">
                <i className="flaticon-sprout"></i>
                <p>
                  Clean growth strategy. We have built our technology in Cloud
                  providers who aim to be net-zero by 2040
                </p>
              </div>
            </div>
            <div className="pb-col-12 pb-col-md-2">
              <div className="pb-block">
                <i className="flaticon-user"></i>
                <p>No bias in recommendations</p>
              </div>
            </div>
            <div className="pb-col-12 pb-col-md-2">
              <div className="pb-block">
                <i className="flaticon-faq"></i>
                <p>"Ask a friend" option for non-techies</p>
              </div>
            </div>
            <div className="pb-col-12 pb-col-md-2">
              <div className="pb-block">
                <i className="flaticon-medal"></i>
                <p>Award-winning AI and data analytics</p>
              </div>
            </div>
            <div className="pb-col-12 pb-col-md-2">
              <div className="pb-block">
                <i className="flaticon-independence"></i>
                <p>
                  Removing barriers to entry. Anyone can access cybersecurity
                </p>
              </div>
            </div>
            <div className="pb-col-12 pb-col-md-2">
              <div className="pb-block">
                <i className="flaticon-free"></i>
                <p>Free to use (first time)</p>
              </div>
            </div>
            <div className="pb-col-12 pb-col-md-2">
              <div className="pb-block">
                <i className="flaticon-help"></i>
                <p>
                  Bringing diversity through inclusivity to the cybersecurity
                  market
                </p>
              </div>
            </div>
            <div className="pb-col-12 pb-col-md-2">
              <div className="pb-block">
                <i className="flaticon-robotic"></i>
                <p>
                  Auto CISO - Subscribe for the cost of a coffee for easy
                  renewal and information management
                </p>
              </div>
            </div>
            <div className="clearfix" />
          </div>
        </div>
      </section>
      <ExistingNews />
      <UseCases />
      <section className="pb-section gray-bg pb-how-work">
        <div className="container">
          <h1 className="pbh-3">How it works</h1>
          <div className="row">
            <div className="col-12 col-md-4 px-3">
              <div className="pc-icon-box">
                <i className="flaticon-edit"></i>
              </div>
              <div className="award-box">
                <span className="box-number">01</span>
                <p>
                  Register &amp; answer our questionnaire. Techies, this'll take
                  minutes. Non-techies, 1 hour maximum &amp; you don't have to
                  answer all the techie questions.{" "}
                </p>
              </div>
            </div>
            <div className="col-12 col-md-4 arrow-col px-3">
              <div className="pc-icon-box">
                <i className="flaticon-coffee-cup"></i>
              </div>
              <div className="award-box">
                <span className="box-number">02</span>
                <p>
                  Techies, tell us your budget breakdown by category &amp; our
                  AI will do rest. Non-techies, you can "ask a friend". Easily
                  import answers from Sage, Xero (&amp; more) or delegate to
                  your team. Our jargon-free ProtectBox team are on-hand for
                  further support.{" "}
                </p>
                <div className="inn-icons">
                  <span>
                    <i className="flaticon-chat-1"></i>
                  </span>
                  <span>
                    <i className="flaticon-envelope-1" />
                  </span>
                  <span>
                    <i className="flaticon-smartphone" />
                  </span>
                </div>
              </div>
            </div>
            <div className="col-12 col-md-4 px-3">
              <div className="pc-icon-box">
                <i className="flaticon-mail"></i>
              </div>
              <div className="award-box">
                <span className="box-number">03</span>
                <p>
                  Receive your six personalised bundles of recommendations for
                  comparison, including product descriptions and reviews from
                  real users (not other suppliers). Personalise bundles further,
                  using the sliders and filters in our tool that let you
                  personalise by risk score, budget or supplier.{" "}
                </p>
              </div>
            </div>
            <div className="col-12 col-md-6 arrow-col2 px-3">
              <div className="pc-icon-box">
                <i className="flaticon-credit-card"></i>
              </div>
              <div className="award-box">
                <span className="box-number">04</span>
                <p>
                  Once happy with your choice, buy in one. click, via credit
                  card (3 months interest-free option available or pay with
                  government grants).
                </p>
                <div className="pc-icon-box2">
                  <i className="flaticon-add-user"></i>
                </div>
                <p>
                  Auto-CISO: Alternatively, for a small subscription fee, save
                  your information, market news, best buys and more, for easy
                  ongoing management.{" "}
                </p>
                <p />
              </div>
            </div>
            <div className="col-12 col-md-6 px-3">
              <div className="pc-icon-box">
                <i className="flaticon-smile"></i>
              </div>
              <div className="award-box">
                <span className="box-number">05</span>
                <p>
                  Be 100% happy with what you've bought. If you're not, then
                  we'll arrange a refund. Or leave a Review about your
                  order/suppliers, to help other customers get better service
                  next time!
                </p>
              </div>
            </div>
          </div>
        </div>
      </section>
      <AwardWinning />
      <article className="pb-section home-different-section">
        <div className="container">
          <h1 className="pbh-3">What makes us different?</h1>
          <div className="row">
            <div className="col-12 col-md-6 pr-3">
              <p>
                We're not only making cybersecurity simple, we're also changing
                hearts and minds.
              </p>
              <p>
                Small and medium businesses trust and engage with us because we
                are one too.
              </p>
              <p>
                We're all about collaboration, empowerment, challenging the
                norms of who can work in cyber, by hiring ex-forces, graduates,
                returners-to-work, etc. and making it fun along the way.
              </p>
              <p>
                This is why our customers, partners, investors and supporters
                are all as much a part of the ProtectBox family as our team.
              </p>
            </div>
            <div className="col-12 col-md-6 pr-3">
              <div className="different-img">
                <img src="./../../../image/video-overview.png" />
              </div>
            </div>
          </div>
        </div>
      </article>
      <article className="pb-section pb-home-security">
        <div className="container">
          <h1 className="pbh-3">Reducing bias and increasing diversity</h1>
          <div className="row">
            <div className="col-12 col-md-6 pr-3">
              <div className="pb-video">
                <div className="video-overlay video-overlay2">
                  <div className="video-icon">
                    <a
                      id="videobtn"
                      target="_blank"
                      className="video-btn white"
                      href="https://www.youtube.com/watch?v=wM1lfbCKtSo&feature=youtu.be"
                    >
                      <i className="fa fa-play" />
                    </a>
                  </div>
                </div>
              </div>
            </div>
            <div style={{ padding: "0 10px" }} className="col-12 col-md-6 px-1">
              <p>
                Reducing bias in the cybersecurity marketplace and increasing
                diversity is at the heart of everything we do.
              </p>
              <p>
                {" "}
                We use data from a broad spectrum of diverse sources, both
                direct and indirect.
              </p>
              <p>
                This includes real customer reviews, from people like you,
                rather than industry experts as this provides a more realistic
                marketplace for everyone.
              </p>
            </div>
          </div>
        </div>
      </article>
      <LastInsidesAndNews />
      <LatestInsides />
      <article className="pb-section security-everyone">
        <div className="container">
          <h1 className="pbh-3">
            Marketplace (Cybersecurity &amp; more) as a service
          </h1>
          <div className="row">
            <div className="col-12 col-md-6 px-3">
              <div className="gray-box">
                <h4>For techies:</h4>
                <ul>
                  <li>
                    Within 1 hour, receive tailored recommendations for all of
                    your business' cybersecurity needs
                  </li>
                  <li> Responsible AI for the perfect match</li>
                  <li>No bias</li>
                  <li>
                    Creating inclusion and bringing diversity to the
                    cybersecurity space
                  </li>
                  <li> Easily import data from your Sage or Xero</li>
                </ul>
              </div>
            </div>
            <div className="col-12 col-md-6 px-3">
              <div className="gray-box">
                <h4>For non-techies:</h4>
                <ul>
                  <li>Jargon-free, easy to complete questionnaire</li>
                  <li> "Ask a friend" option</li>
                  <li>ProtectBox support team on hand to help</li>
                  <li>
                    Responsible AI recommending the perfect match for all of
                    your business' cybersecurity needs
                  </li>
                  <li> Making cybersecurity accessible for everyone</li>
                </ul>
              </div>
            </div>
            <div className="col-12 col-md-12">
              <div className="pb-btn-box">
                <Link className="btn btn-secondary" to="/Login">
                  Get Started
                </Link>
              </div>
            </div>
          </div>
        </div>
      </article>
      <Footer />
    </div>
  );
};

export default Home;
