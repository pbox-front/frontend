import React from "react";
import { FaEnvelope } from "react-icons/fa";

const LatestInsides = () => {

    var EnvelopeStyle = {
        height: "45px",
        width: "20px"
    };
    
    return (
        <div>
            <section className="pb-section newsletter-section">
                <div className="container">
                    <div className="row">
                        <div className="col-12 col-md-6">
                        <h2 className="pbh-2">Sign up for latest insights direct to your inbox</h2>
                        </div>
                        <div className="col-12 col-md-6">
                            <form name="form_latest_insights" className="newsletter-form " action="" method="post" style={{ "marginBottom": "0" }}>
                                <div className="form-group email ">
                                    <input type="text" name="email" id="latest_insights_email" className="form-control " placeholder="Email " />
                                </div>
                                <div className="form-group submit ">
                                    <a type="button " href="#">
                                        <FaEnvelope style={{ ...EnvelopeStyle }}/>
                                    </a>
                                </div>
                                <div className="clearfix "></div>
                            </form>
                        </div>
                    </div>
                </div>
            </section>

            <div className="modal fade" id="LatestInsightsModal" role="dialog" aria-labelledby="LatestInsightsModalLabel" aria-hidden="true">
                <div className="modal-dialog modal-info" role="document">
                    <div className="modal-content">
                        <div className="modal-body">
                            <button className="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                            <div className="row">
                                <p>
                                “Thank you for registering, we will be in touch shortly.” <br />
                                When someone signs up through Exciting News & Latest Insights boxes.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default LatestInsides;
