import React from "react";
import Moment from "moment";
import {Link} from "react-router-dom";

class BlogItem extends React.Component {
  constructor(props) {
    super(props);
  }

  render () {
    const base_url = "https://protectbox.com/";

    const item_category = this.props.blog_category;
    const item_link = this.props.blog_link;
    const item_year = this.props.blog_year;
    const item_month = this.props.blog_month;
    const item_day = this.props.blog_day;
    const item_author = this.props.blog_author;
    const item_body = this.props.blog_body;
    const item_id = this.props.blog_id;
    const item_imageUrl = this.props.blog_imageUrl;
    const item_meta_desc = this.props.blog_meta_desc;
    const item_meta_keywords = this.props.blog_meta_keywords;
    const item_publish_date = this.props.blog_publish_date;
    const item_short_body = this.props.blog_short_body;
    const item_slug = this.props.blog_slug;
    const item_title = this.props.blog_title;
    const item_updated_date = this.props.blog_updated_date;

    const item_date = Moment(item_year+"-"+item_month+"-"+item_day).format("MMM Do YYYY");

    return (
      <div className="pb-col-md-2 blog-item">
        <div className="blog-box" style={{height: "293px"}}>
          {item_category == "Awards" ?
            <p className="blog-item-date text-left" style={{"color": "#4a8522"}}>{item_date}</p> :
            item_category == "Blog" ?
              <p className="blog-item-date text-left" style={{"color": "#000"}}>{item_date}</p> :
              item_category == "Events" ?
                <p className="blog-item-date text-left" style={{"color": "#808080"}}>{item_date}</p> :
                //news
                <p className="blog-item-date text-left" style={{"color": "#e39836"}}>{item_date}</p>
          }
          <p className="blog-item-img">
            <img src={base_url+item_imageUrl}/>
          </p>
          <p className="blog-item-title">
            {item_category == "Awards" ?
              <a href={item_link} target="_blank" style={{"fontWeight": "bold", "color": "#4a8522"}}>{item_title}</a> :
              item_category == "Blog" ?
                <span><Link
                  to={{pathname: "/Blog/Detail/"+item_slug}}
                  target="_blank"
                  style={{fontWeight: "bold", color: "#000"}}>{item_title}</Link></span> :
                item_category == "Events" ?
                  <a href={item_link} target="_blank" style={{"fontWeight": "bold", "color": "#808080"}}>{item_title}</a> :
                  //news
                  item_slug == "" ? <a href={item_link} target="_blank" style={{"fontWeight": "bold", "color": "#e39836"}}>{item_title}</a> :
                    <span><Link to={{pathname: `/News/Detail/${item_slug}`}} target="_blank" style={{"fontWeight": "bold", "color": "#e39836"}}>{item_title}</Link></span>
            }
          </p>
        </div>
      </div>
    )
  };
};

export default BlogItem;
