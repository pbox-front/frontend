import React from "react";
import Slider from "react-slick";

import img1 from "./../../src/images2/xero.png";
import img2 from "./../../src/images2/investor-01.png";
import img3 from "./../../src/images2/oracle-logosm.png";
import img4 from "./../../src/images2/Picture1.png";
import img5 from "./../../src/images2/investor-04.png";
import img6 from "./../../src/images2/investor-06.png";
import img7 from "./../../src/images2/investor-07.png";
import img8 from "./../../src/images2/technation_io.png";
import img9 from "./../../src/images2/london-and-partners2.png";
import img10 from "./../../src/images2/pitch palace.png";
import img11 from "./../../src/images2/investor-09.png";
import img12 from "./../../src/images2/investor-10.png";
import img13 from "./../../src/images2/investor-03.png";
import img14 from "./../../src/images2/investor-13.png";
import img15 from "./../../src/images2/ggen.png";
import img16 from "./../../src/images2/investor-14.png";
import img17 from "./../../src/images2/investor-11.png";
import img18 from "./../../src/images2/investor-12.png";
import img19 from "./../../src/images2/investor-15.png";
import img20 from "./../../src/images2/investor-16.png";
import img21 from "./../../src/images2/investor-17.png";


const PartnerInvestor = () => {
    var InvestorSetting = {
        arrows: false,
        dots: false,
        slidesToShow: 6,
        autoplay: true,
        swipeToSlide: true,
        focusOnSelect: true,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                slidesToShow: 3,
                autoplay: true,
                swipeToSlide: true,
                focusOnSelect: true,
                },
            },
            {
                breakpoint: 600,
                settings: {
                slidesToShow: 2,
                autoplay: true,
                swipeToSlide: true,
                focusOnSelect: true,
                },
            }
        ],
    };

    return (
        <article className="pb-section gray-bg pb-partners-investors">
            <div className="white-overlay">
                <div className="container">
                <h1 className="pbh-3">Our partners and investors</h1>
                <p className="text-center">
                    Working with 10+ (shown below) &amp; on-boarding a further 200+
                    around the world
                </p>
                <div className="row">
                    <div className="col-12">
                    <Slider { ...InvestorSetting }>
                        <div>
                        <div className="item">
                            <a href="https://www.xero.com" target="_blank">
                            {" "}
                            <img alt="xero" src={img1} />
                            </a>
                        </div>
                        </div>
                        <div>
                        <div className="item">
                            <a href="http://www.sage.com" target="_blank">
                            {" "}
                            <img
                                alt="investor"
                                src={img2}
                                style={{ marginTop: 0 }}
                            />
                            </a>
                        </div>
                        </div>
                        <div>
                        <div className="item">
                            <a href="https://www.oracle.com/partners" target="_blank">
                            {" "}
                            <img alt="oracle" src={img3} style={{ marginTop: 0 }} />
                            </a>
                        </div>
                        </div>
                        <div>
                            <div className="item">
                                <a href="https://aws.amazon.com/partners" target="_blank">
                                {" "}
                                <img alt="Picture1" src={img4} style={{ marginTop: 0 }} />
                                </a>
                            </div>
                        </div>
                        <div>
                            <div className="item">
                                <a href="#" target="_blank">
                                {" "}
                                <img alt="Picture1" src={img19} style={{ marginTop: 0 }} />
                                </a>
                            </div>
                        </div>
                        
                        <div>
                            <div className="item">
                                <a href="#" target="_blank">
                                {" "}
                                <img alt="Picture1" src={img20} style={{ marginTop: 0 }} />
                                </a>
                            </div>
                        </div>
                        
                        <div>
                            <div className="item">
                                <a href="#" target="_blank">
                                {" "}
                                <img alt="Picture1" src={img21} style={{ marginTop: 0 }} />
                                </a>
                            </div>
                        </div>
                        <div>
                        <div className="item">
                            <a href="https://cyberexchange.uk.net" target="_blank">
                            {" "}
                            <img alt="investor" src={img5} style={{ marginTop: 0 }} />
                            </a>
                        </div>
                        </div>
                        <div>
                        <div className="item">
                            <a href="https://cyber-center.org" target="_blank">
                            {" "}
                            <img alt="investor" src={img6} style={{ marginTop: 0 }} />
                            </a>
                        </div>
                        </div>
                        <div>
                        <div className="item">
                            <a href="https://www.gan.co" target="_blank">
                            {" "}
                            <img alt="investor" src={img7} style={{ marginTop: 0 }} />
                            </a>
                        </div>
                        </div>
                        <div>
                        <div className="item">
                            <a href="https://technation.io/" target="_blank">
                            {" "}
                            <img alt="technation_io" src={img8} style={{ marginTop: 0 }} />
                            </a>
                        </div>
                        </div>
                        <div>
                        <div className="item">
                            <a href="#" target="_blank">
                            {" "}
                            <img alt="london-and-partners" src={img9} style={{ marginTop: 0 }} />
                            </a>
                        </div>
                        </div>
                        <div>
                        <div className="item">
                            <a href="#" target="_blank">
                            {" "}
                            <img alt="pitch palace" src={img10} style={{ marginTop: 0 }} />
                            </a>
                        </div>
                        </div>
                        <div>
                        <div className="item">
                            <a href="http://www.hutzero.co.uk" target="_blank">
                            {" "}
                            <img alt="investor" src={img11} style={{ marginTop: 0 }} />
                            </a>
                        </div>
                        </div>
                        <div>
                        <div className="item">
                            <a href="https://inogesis.com" target="_blank">
                            {" "}
                            <img alt="investor" src={img12} style={{ marginTop: 0 }} />
                            </a>
                        </div>
                        </div>
                        <div>
                        <div className="item">
                            <a href="https://www.investni.com" target="_blank">
                            {" "}
                            <img alt="investor" src={img13} style={{ marginTop: 0 }} />
                            </a>
                        </div>
                        </div>
                        <div>
                        <div className="item">
                            <a href="http://www.techlondonadvocates.org.uk/" target="_blank">
                            {" "}
                            <img alt="investor" src={img14} style={{ marginTop: 0 }} />
                            </a>
                        </div>
                        </div>
                        <div>
                        <div className="item">
                            <a href="https://www.youtube.com/watch?v=D-DOWCliNrE" target="_blank">
                            {" "}
                            <img alt="ggen" src={img15} style={{ marginTop: 0 }} />
                            </a>
                        </div>
                        </div>
                        <div>
                        <div className="item">
                            <a href="https://fiftyfiftypledge.com/" target="_blank">
                            {" "}
                            <img alt="investor" src={img16} style={{ marginTop: 0 }} />
                            </a>
                        </div>
                        </div>
                        <div>
                        <div className="item">
                            <a href="https://techtalentcharter.co.uk" target="_blank">
                            {" "}
                            <img alt="investor" src={img17} style={{ marginTop: 0 }} />
                            </a>
                        </div>
                        </div>
                        <div>
                        <div className="item">
                            <a href="https://aws-restart.com" target="_blank">
                            {" "}
                            <img alt="investor" src={img18} style={{ marginTop: 0 }} />
                            </a>
                        </div>
                        </div>
                    </Slider>
                    </div>
                </div>
                </div>
            </div>
        </article>
    );
};

export default PartnerInvestor;
