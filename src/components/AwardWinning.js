import React from "react";
import Slider from "react-slick";

import CoggImg from "./../../src/images2/awards4/cogg.png";
import AlgImg from "./../../src/images2/awards4/alg.png";
import Corp2020Img from "./../../src/images2/awards4/corp2020.jpg";
import SecurityFireExcellenceAwardImg from "./../../src/images2/awards4/security-fire-excellence-award.jpg";
import Picture14Img from "./../../src/images2/awards4/Picture14.jpg";
import TechnationRisingStarImg from "./../../src/images2/awards4/technation rising star.png";
import wis1Img from "./../../src/images2/awards4/wis1.png";
import BritishIndianAwardNewImg from "./../../src/images2/awards4/7th british indian award new.png";
import InnovationImg from "./../../src/images2/awards4/innovation.jpg";
import Picture26Img from "./../../src/images2/awards4/Picture26.png";

const AwardWinning = () => {
    
    var AwardSliderSetting = {
        slidesToShow: 6,
        slidesToScroll: 1,
        autoplay: true,
        swipeToSlide: true,
        focusOnSelect: true,
        dots: true,
        infinite: true,
        responsive: [
        {
            breakpoint: 1024,
            settings: {
                slidesToShow: 3,
                autoplay: true,
                swipeToSlide: true,
                focusOnSelect: true,
            },
        },
        {
            breakpoint: 600,
            settings: {
                slidesToShow: 2,
                autoplay: true,
                swipeToSlide: true,
                focusOnSelect: true,
            },
            },
        {
            breakpoint: 480,
            settings: {
                slidesToShow: 1,
                autoplay: true,
                swipeToSlide: true,
                focusOnSelect: true,
            },
        },
        ],
    };    

    var awardItemStyle = {
        marginRight: "10px"
    };

    var awardTextStyle = {
        fontWeight: "bold",
        color: "#4a8522"
    };

    var awardImgStyle = {
        height: "100px"
    };

    return (
        <section className="pb-section award-technology-leadership">
            <div className="black-overlay">
                <div className="container">
                    <h1 className="pbh-3 white-color">Award-winning AI technology and leadership</h1>
                    <p className="white-color text-center">
                        Winner of 10 Awards (scroll through them below) &amp; finalist in
                        40+ other Awards (see Blog for details)
                    </p>       
                    <div className="row">
                        <div className="col-12">
                            <Slider { ...AwardSliderSetting }>
                                <div className="item" style={{ ...awardItemStyle }}>
                                    <div className="award-box">
                                        <div className="award-box-img">
                                            <img alt="awards" src={ CoggImg } style={{ ...awardImgStyle }} />
                                        </div>
                                        <div className="award-box-title">
                                            <a href="https://cogx.co/cogx-awards/awards-2020-winners/" target="_blank" style={{ ...awardTextStyle }}>CogX Best AI in Cybersecurity Award (Winner) - humbled to beat BT & Darktrace</a>
                                        </div>
                                    </div>
                                </div>

                                <div className="item" style={{ ...awardItemStyle }}>
                                    <div className="award-box">
                                        <div className="award-box-img">
                                            <img alt="alg" src={ AlgImg } style={{ ...awardImgStyle }} />
                                        </div>
                                        <div className="award-box-title">
                                            <a href="https://www.acq-intl.com/winners/protectbox-ltd/" target="_blank" style={{ ...awardTextStyle }}>Cyber Security Awards 2020 </a>
                                        </div>
                                    </div>
                                </div> 

                                <div className="item" style={{ ...awardItemStyle }}>
                                    <div className="award-box">
                                        <div className="award-box-img">
                                            <img alt="alg" src={ Corp2020Img } style={{ ...awardImgStyle }} />
                                        </div>
                                        <div className="award-box-title">
                                            <a href="https://corp-today.com/corp2020-vote-now/" target="_blank" style={{ ...awardTextStyle }}>Best Cyber Security & Data Protection Comparison Platform 2020</a>
                                        </div>
                                    </div>
                                </div>

                                <div className="item" style={{ ...awardItemStyle }}>
                                    <div className="award-box">
                                        <div className="award-box-img">
                                            <img alt="security" src={ SecurityFireExcellenceAwardImg } style={{ ...awardImgStyle }} />
                                        </div>
                                        <div className="award-box-title">
                                            <a href="https://www.securityandfireawards.com/previous-years/winners-2018" target="_blank" style={{ ...awardTextStyle }}>Cybersecurity Project of the Year 2018</a>
                                        </div>
                                    </div>
                                </div>	

                                <div className="item" style={{ ...awardItemStyle }}>
                                    <div className="award-box">
                                        <div className="award-box-img">
                                            <img alt="Picture14" src={ Picture14Img } style={{ ...awardImgStyle }} />
                                        </div>
                                        <div className="award-box-title">
                                            <a href="https://www.wired.co.uk/article/wired-security-2017-startup-stage" target="_blank" style={{ ...awardTextStyle }}>Best Security StartUp 2017</a>
                                        </div>
                                    </div>
                                </div>  

                                <div className="item" style={{ ...awardItemStyle }}>
                                    <div className="award-box">
                                        <div className="award-box-img">
                                            <img alt="awards" src={ TechnationRisingStarImg } style={{ ...awardImgStyle }} />
                                        </div>
                                        <div className="award-box-title">
                                            <a href="https://youtu.be/NvNG9Ro74G8" target="_blank" style={{ ...awardTextStyle }}>BDO Drive's Creative Award for Lego Movie "May The ProtectBox Be With You" 2019</a>
                                        </div>
                                    </div>
                                </div>		
                            
                                <div className="item" style={{ ...awardItemStyle }}>
                                    <div className="award-box">
                                        <div className="award-box-img">
                                            <img alt="wis1" src={ wis1Img }  style={{ ...awardImgStyle }} />
                                        </div>
                                        <div className="award-box-title">
                                            <a href="http://www.ifsecglobal.com/uncategorized/women-in-security-awards-2019-winners-revealed/amp/" target="_blank" style={{ ...awardTextStyle }}>Women in Security Awards' Technical Award 2019</a>
                                        </div>
                                    </div>
                                </div> 

                                <div className="item" style={{ ...awardItemStyle }}>
                                    <div className="award-box">
                                        <div className="award-box-img">
                                            <img alt="british" src={ BritishIndianAwardNewImg }  style={{ ...awardImgStyle }} />
                                        </div>
                                        <div className="award-box-title">
                                            <a href="https://oceanicconsultingblog.wordpress.com/2019/07/26/winners-for-the-7th-british-indian-awards-2019-are-revealed/" target="_blank" style={{ ...awardTextStyle }}>7th British Indian Awards' Creative Entrepreneur of Year 2019</a>
                                        </div>
                                    </div>
                                </div>		

                                <div className="item" style={{ ...awardItemStyle }}>
                                    <div className="award-box">
                                        <div className="award-box-img">
                                            <img alt="innovation" src={ InnovationImg }  style={{ ...awardImgStyle }} />
                                        </div>
                                        <div className="award-box-title">
                                                <a href="https://www.corporatelivewire.com/awards.html?award=121#:~:text=The%20Corporate%20LiveWire%20Innovation%20%26%20Excellence,to%20demonstrate%20exceptional%20business%20performance" style={{ ...awardTextStyle }} target="_blank">Cybersecurity Comparison Site of the Year 2019</a>
                                        </div>
                                    </div>
                                </div>		

                                <div className="item" style={{ ...awardItemStyle }}>
                                    <div className="award-box">
                                        <div className="award-box-img">
                                            <img alt="innovation" src={ Picture26Img }  style={{ ...awardImgStyle }} />
                                        </div>
                                        <div className="award-box-title">
                                            <a href="https://cybersecurityawards.com/" style={{ ...awardTextStyle }} target="_blank">Woman of Year 2016 (Commended)</a>
                                        </div>
                                    </div>
                                </div>	
                            </Slider>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    );
};

export default AwardWinning;
