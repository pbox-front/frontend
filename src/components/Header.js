import React from "react";
import { render } from "react-dom";
import { NavLink, Link } from "react-router-dom";
import { BsSearch } from "react-icons/bs";
import LanguageBar from "./LanguageBar";
import $ from 'jquery';
import axios from 'axios';
import { Accordion } from "react-bootstrap";

function ShowSearchBox() {
  if (window.innerWidth <= 768) {
    if ($(".main-menu").is(':visible')) {
      $(".main-menu").toggle();
    }
  }
  $(".search-field").slideToggle(300);
}

function setToggleState() {
  $(".main-menu").toggle();
}

$(document).click(function(e) {
  if (window.innerWidth <= 768) {
    if(!$(e.target).closest('.mobile-menu, .main-menu, .menu-item').length){
      if ($(".main-menu").is(':visible')) {
        $(".main-menu").toggle();
      }
    }
  }
})

class Header extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      keywordList: [],
      headingList: [],
      directURL: ""
    }

    this.refreshPage = this.refreshPage.bind(this);
  }

  GrabKeyword = ()=> {
    axios.get('/www/api/keywordapi')
      .then(
        (res) => {
          if (res.data.message == "Success") {
            // console.log(res.data);
            this.setState({keywordList: res.data.data});
          }
        },
      )
  }

  GrabOutKeyword = ()=> {
    const timer = setTimeout(() => {
      //$('.but').html('');
      this.setState({keywordList: []});
    }, 2000);
    return () => clearTimeout(timer);
  }

  _getHeadingList (keyword, freetext) {
    $("#keyword").val(keyword);

    const url = "/www/api/faqapi/getheadings";
    let formData = new FormData();

    formData.append("keyword", keyword);
    formData.append("freetext", freetext);

    axios.post(url, formData, {
      headers: {
        "Content-Type": `multi-part/form-data`,
        "Access-Control-Allow-Origin": "*"
      },
    }).then((res) => {
      if (res.data.message == "Success") {
        // console.log(res.data.data);
        this.setState({headingList: res.data.data});
      }
    })
  }

  refreshPage(keyword, freetext) {
    //FAQs?keyword=Small&freetext=no
    // this.props.history.push(`/FAQs?keyword=${keyword}&freetext=${freetext}`);
    this.setState({directURL: `/FAQs?keyword=${keyword}&freetext=${freetext}`}, () => this.props.history.push(`/FAQs?keyword=${keyword}&freetext=${freetext}`));
  }


  render() {
    const keywordList = this.state.keywordList;
    const keywordListView = keywordList.map((item, index) => {
      return <button key={index} type="button" className="btn btn-sm btn-success sbtn mt-1" onClick={() => this._getHeadingList(item.heading, "no")}>{item.heading}</button>
    });

    const CardGroupStyle = {
      fontWeight: "bold",
      margin: "2rem auto"
    }

    const CardStyle = {
      marginTop: "10px"
    }

    const CardHeaderStyle = {
      padding: "0.5rem",
      border: "0.5px solid #000"
    }

    const CardBodyStyle = {
      border: "1px solid #000"
    }

    const headingList = this.state.headingList;
    const headingListView = headingList.map((item, index) => {
      if (item.type == "qa") {
        return <div key={index} className="repeatrow" style={{margin: "0px", width: "100%"}}>
          <div className="qheading" style={{border: "0.5px solid black"}}>
            <Accordion.Toggle as={Link} variant="link" eventKey={index + 1} style={{
              "fontSize": 16,
              "color": "#262626",
              backgroundColor: "#fff",
              display: "contents",
              width: "auto",
              height: "auto",
              textAlign: "left"
            }}>
              <div>
                <span data-toggle="collapse" data-parent="#accordion" href="#collapse2" aria-expanded="false"
                   className="collapsed" style={{
                  "fontSize": 16,
                  "color": "#262626",
                  backgroundColor: "#fff",
                  display: "contents",
                  width: "auto",
                  height: "auto",
                  textAlign: "left"
                }}>
                  {item.question}
                </span>
              </div>
            </Accordion.Toggle>
          </div>
          <Accordion.Collapse eventKey={index + 1}>
            <div style={{...CardBodyStyle}}>
              <p className="p3">{item.answer}</p>
            </div>
          </Accordion.Collapse>
        </div>
      } else if (item.type == "keyword") {
        return <div className="col-md-12">
          <p className="readmore" style={{background: "white", textAlign: "center"}}>
            <NavLink to={{pathname: `/FAQs?keyword=${item.keyword}&freetext=no`}}>Click here to see full list...</NavLink>
          </p>
        </div>
      }
    });

    return (
      <header id="header" className="main-header">
        <div className="container-fluid">
          <div className="row">
            <div className="col-6 col-md-3">
              <div className="logo">
                <Link to="/Home">
                  <img src="/image/logo.png" />
                </Link>
              </div>
            </div>
            <div className="col-6 col-md-9 menu-col">
              <div className="mobile-menu" onClick={ setToggleState }>
                <span className="toggle-btn"></span>
              </div>
              <ul className="main-menu">
                <li><Link className="menu-item" to="/SupplierAndPartners">Suppliers & Partners</Link></li>
                <li><Link className="menu-item" to="/About">About</Link></li>
                <li><Link className="menu-item" to="/Blog">Blog</Link></li>
                <li><Link className="menu-item" to="/ContactUs">Contact Us</Link></li>
                <li><Link className="menu-item" to="/FAQs">FAQs</Link></li>
                <li><Link className="menu-item btn btn-primary mr-2" to="/Login">Login</Link></li>
                <li><Link className="menu-item btn btn-primary mr-2" to="/Registration">Registration</Link></li>
                <li><button className="menu-item btn search-btn mr-2 pt-1" onClick={ () => this.setState({keywordList: [], headingList: []}, () => ShowSearchBox())}><BsSearch style={{ "width": "20px", "height": "30px" }}/></button></li>
                <li>
                  <LanguageBar />
                </li>
              </ul>
              <div className="search-bar">
                <div className="search-field">
                  <form className="pb-form" action="">
                    <div className="form-group email">
                      <input type="text" name="keyword" id="keyword" className="form-control" placeholder="Search: click here for keywords, or type (/click button on right)" onFocus={this.GrabKeyword} onBlur={this.GrabOutKeyword}/>
                    </div>
                    <div className="form-group submit">
                      <a type="button" href="#">Search</a>
                    </div>
                    <div className="but">{keywordListView}</div>


                    <Accordion className="row mainsearchdiv" >                      
                      {headingListView}
                    </Accordion>
                    <div className="clearfix"></div>
                  </form>
                </div>
              </div>
              <div>

              </div>
            </div>
          </div>
        </div>
      </header>
    );
  }
}

export default Header;
