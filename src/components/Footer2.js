import React from "react";
import { AiOutlineMail } from "react-icons/ai";
import { FaLinkedinIn, FaTwitter } from "react-icons/fa";
import { FaFacebookF } from "react-icons/fa";
import { Link } from "react-router-dom";

import AppleStoreImg from './../images2/apple_store.png';
import GooglePlayImg from './../images2/google-play.png';
import PCIImg from './../images2/pci.png';
import SSLcertificateImg from './../images2/SSLcertificate.png';


const Footer2 = () => {
    const footerTitle = {
        borderBottom: "1px solid #000",
        marginTop: 20,
        marginBottom: 10,
        paddingBottom: 5,
        color: "#292828",
        fontSize: 22
    }

    const iconStyle = {
        color: "#000",
        background: "#CCC",
        padding: 10,
        borderRadius: "50%",
        marginRight: 5,
        height: 40,
        width: 40,
        display: "inline-block",
        textAlign: "center"
    }
    const copyRightTextStyle = {
        background: "rgb(133, 198, 44)",
        padding: "10px 0",
        color: "#fff",
        fontSize: 12,
        textAlign: "center"
    }
    return (<footer style={{ background: "none", color: "#000", padding: 0 }}>
        <div className="container" style={{ background: "#e6e7e9", width: "100%", maxWidth: "100%", borderTop: "2px solid #ccc", height: "auto" }}>
            <div className="container">
                <div className="row ">
                    <div className="col-md-4 col-sm-12">
                        <h3 style={footerTitle}>Contact Information</h3>
                        <p style={{ fontSize: "1rem" }}></p>
                        <p style={{ color: "#010101" }}>Chat to us using the blue chat icon in the bottom right corner. Or email us, as shown below (or call us on +44 (0)207 993 3037), our customer service team is available Mon-Fri 9am-5pm (UK time).</p>
                        <AiOutlineMail style={{ "color": "#000" }} /> <a href="mailto:team@protectbox.com" style={{ color: "#010101" }}>team@protectbox.com (Businesses) </a><br />
                        <AiOutlineMail style={{ "color": "#000" }} /> <a href="mailto:supplier@protectbox.com" style={{ color: "#010101" }}>supplier@protectbox.com (Suppliers)</a><br />
                        <AiOutlineMail style={{ "color": "#000" }} /> <a href="mailto:kiran@protectbox.com" style={{ color: "#010101" }}>kiran@protectbox.com (Investors &amp; Media) </a>
                        <p></p>

                        <p style={{ fontSize: "1em" }}>
                            <a rel="nofollow" href="https://www.facebook.com/protectbox/" target="_BLANK" style={iconStyle}>
                                <span>
                                    <FaFacebookF style={{ "color": "#000" }} />
                                </span>
                            </a>
                            <a rel="nofollow" href="https://www.linkedin.com/company/protectbox-ltd" target="_BLANK" style={iconStyle}>
                                <span>
                                    <FaLinkedinIn style={{ "color": "#000" }} />
                                </span>
                            </a>
                            <a rel="nofollow" href="https://twitter.com/ProtectBoxLtd/" target="_BLANK" style={iconStyle}>
                                <span>
                                    <FaTwitter style={{ "color": "#000" }} />
                                </span>
                            </a>
                            <a rel="nofollow" href="mailto:team@protectbox.com" target="_BLANK" style={iconStyle}>
                                <span>
                                    <AiOutlineMail style={{ "color": "#000" }} />
                                </span>
                            </a>

                        </p>
                        <p style={{ fontSize: "1em" }}>
                            <a rel="nofollow" href="https://apps.apple.com/in/app/protectbox/id1514550015" className="iosonly" target="_blank">
                                <img src={AppleStoreImg} alt="apple_store" style={{ height: 30 }} />
                            </a>

                            <a rel="nofollow" href="https://play.google.com/store/apps/details?id=com.security.protectbox" className="gplayfooter" style={{ marginLeft: 15 }} target="_blank">
                                <img src={GooglePlayImg} alt="google-play" style={{ height: 30 }} />
                            </a>

                        </p>
                    </div>
                    <div className="col-md-4 col-sm-12">
                        <h3 style={footerTitle}>What are you looking for</h3>
                        <p style={{ fontSize: "1em" }}><Link to={'/About'} style={{ color: "#010101" }} target="_blank">About</Link></p>
                        <p style={{ fontSize: "1em" }}><Link to={'/Registration'} style={{ color: "#010101" }} target="_blank">Register</Link></p>
                        <p style={{ fontSize: "1em" }}><Link to={'/Login'} style={{ color: "#010101" }} target="_blank">About</Link></p>
                        <p style={{ fontSize: "1em" }}><Link to={'/ContactUs'} style={{ color: "#010101" }} target="_blank">Contact Us</Link></p>

                    </div>

                    <div className="col-md-4 col-sm-12" id="contact_bg">
                        <h3 style={footerTitle}>Useful Links</h3>
                        <p style={{ fontSize: "1em" }}><Link to={'/Termsandconditions'} style={{ color: "#010101" }} target="_blank">Terms and Conditions</Link></p>
                        <p style={{ fontSize: "1em" }}><Link to={'/Privacypolicy'} style={{ color: "#010101" }} target="_blank">Privacy Policy</Link></p>
                        <p style={{ fontSize: "1em" }}><Link to={'/Supplierpolicy'} style={{ color: "#010101" }} target="_blank">Supplier Policy</Link></p>
                    </div>
                </div>
            </div>
            <div className="container">
                <div className="row ">
                    <div className="col-md-4 col-sm-12">
                        <p style={{ fontSize: "1rem", paddingTop: 20 }}>
                            <a href="#" style={{ textAlign: "center" }} rel="nofollow">
                                <img alt="SSLcertificate" src={SSLcertificateImg} style={{ height: 56, padding: "5px 0px 0px 0px" }} />
                            </a>
                            <a href="#" style={{ textAlign: "center" }} rel="nofollow">
                                <img alt="PCI" src={PCIImg} style={{ marginLeft: "2.4rem", height: 56, padding: "5px 0px 0px 0px" }} />
                            </a>

                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div style={copyRightTextStyle}>
            <div className="container">
                Copyright © 2018 ProtectBox Ltd. Company number: NI643316 - VAT registration number: 297 5082 62- All rights reserved.
		</div>
        </div>
    </footer >
    );
}

export default Footer2;