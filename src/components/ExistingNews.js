import React from "react";
import { FaEnvelope } from "react-icons/fa";

const ExistingNews = () => {

    var EnvelopeStyle = {
        height: "45px",
        width: "20px"
    };

    return (
        <section className="pb-section theme-bg2 pb-exciting-news">
            <div className="container">
                <div className="content-box">
                    <div className="row">
                        <div className="col-12 col-md-6 pr-3">
                            <h1 className="pbh-2">Exciting News</h1>
                            <div className="item">
                                <p>We have exciting plans at ProtectBox to extend our offering further.
                                If you want to buy or can sell B2C, Infrastructure (B2I e.g. hospitals, schools, universities) or Government (B2G) products, please register your interest and we'll get in touch to discuss.</p>
                            </div>
                        </div>
                        <div className="col-12 col-md-6">
                            <form name="form_exciting_news" className="newsletter-form mt-5" action="/" method="post">
                                <div className="form-group email">
                                    <input type="hidden" name="page_type" value="home" />
                                    <input type="hidden" name="type" value="exciting-news" />
                                    <input type="text" name="email" className="form-control" placeholder="Email" />
                                </div>
                                <div className="form-group submit">
                                    <a href="#">
                                        <FaEnvelope style={{ ...EnvelopeStyle }}/>
                                    </a>
                                </div>
                                <div className="clearfix"></div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    );
};

export default ExistingNews;
