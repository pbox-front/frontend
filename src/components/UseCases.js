import React from "react";
import { Link } from "react-router-dom";
import { OverlayTrigger, Button, Tooltip, Popover } from "react-bootstrap";
import Slider from "react-slick";

const UseCases = () => {

    var Font13Style = {
        fontSize: 13
    };

    var popoverStyle = {
        maxWidth: "500px",
        position: "absolute",
        fontSize: 13,
        zIndex: 9999,
        padding: "0px",
        width: "98%"
    };

    var useCaseSliderSetting = {
        slidesToShow: 4,
        slidesToScroll: 1,
        autoplay: true,
        swipeToSlide: true,
        focusOnSelect: true,
        dots: false,
        infinite: true,
        arrows: false,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    autoplay: true,
                    swipeToSlide: true,
                    focusOnSelect: true
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2,
                    autoplay: true,
                    swipeToSlide: true,
                    focusOnSelect: true
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    autoplay: true,
                    swipeToSlide: true,
                    focusOnSelect: true
                }
            }
        ],
    };

    const popoverSage = (
        <Popover id="popover-save" style={{ ...popoverStyle }}>
            <Popover.Content>
                <p style={{ ...Font13Style }}>Extended their product offering to include a security marketplace by showing link to our website. Integrated with their API to auto-pull customer data<br/><br/>-Listing for us on Sage&apos;s Marketplace. After Sage customer clicks on &quot;Contact Us&quot; on this page (on Sage&apos;s website) the customer gets sent to our website<br/><br/>-Integration with Sage’s API software that auto-pulls Sage customers&apos; data (from customer invoices on Sage e.g. if and when anyone in customer’s firm bought cybersecurity, without customer having to be technical) into our questionnaire to help customers answer our questionnaire. Customer’s consent (/data privacy satisfaction), is auto-built into Sage’s API integration</p>
            </Popover.Content>
        </Popover>
    );

    const popoverOracle = (
        <Popover id="popover-oracle" style={{ ...popoverStyle }}>
            <Popover.Content>
                <p style={{ ...Font13Style }}>Extended their product offering to include a cybersecurity comparison marketplace App while keeping data localised<br/><br/>-Oracle customer portal contains a ProtectBox App that deploys a lite version of ProtectBox's user interface<br/>- All data stays within Oracle's systems which is important for GDPR requirements as they are handling sensitive data including personal data.- APIs to ProtectBox's backend process the data.<br/>- Creates value for Oracle's customers by providing a new product service (cyvbersecurity comparison marketplace) without the customer needing to source a provider themselves and undertake due diligence.</p>
            </Popover.Content>
        </Popover>
    );

    const popoverWellbeingApp = (
        <Popover id="popover-wellbeingapp" style={{ ...popoverStyle }}>
            <Popover.Content>
                <p style={{ ...Font13Style }}>Award-winning algorithm repurposed for non-cybersecurity applications<br/><br/>- Uses bundling and matchmaking elements of ProtectBox's award-winning algorithm for TeleHealth application<br/>-Covid-19 has put the NHS and its staff under immense strain with NHS professionals required to put their jobs first and put their own families at risk.<br/>-Hospitals can now get team members to complete a ProtectBox generated wellbeing questionnaire which ProtectBox's algorithm can then assess and flag any team members who need further support during this time of immense stress.<br/>-Hospitals can then work with the individuals to ensure they get the support they need<br/>- Hospitals are paying for this as they recognise the importance of the wellbeing of their team members.</p>
            </Popover.Content>
        </Popover>
    );

    const popoverGenieShares = (
        <Popover id="popover-genieshares" style={{ ...popoverStyle }}>
            <Popover.Content>
                <p style={{ ...Font13Style }}>Co-promotion of ProtectBox’s values, as part of Corporate Social Responsibility (CSR) of backers such as Salesforce & Google</p>
                <p style={{ ...Font13Style }}> ProtectBox pledged 1% of its equity to a frontline staff / keyworker through https://genieshares.com because these are the new faces of entrepreneurship. Purposeful, ambitious, but also compassionate and humble. A growing group of business men and women who are determined to succeed - and to bring others along for the ride. We have all pledged a share of our businesses in a way that’s designed to give everyone a chance of owning a stake.</p>
                <p style={{ ...Font13Style }}> GenieShares is backed by Salesforce, the world's leading Customer Relationship Management tool for small, medium and enterprise businesses. Salesforce is also a leader in corporate purpose with its 1-1-1 model - a pledge to give one percent of employee time, equity and profits to non-profit organisations. Aligned to this, they are exploring working with GenieShares’ pledgers on a series of videos to pick those receiving 1% of these inclusive, visionary-led businesses.</p>
            </Popover.Content>
        </Popover>
    );

    var usecaseTitleStyle = {
        height: "80px"
    };

    const popoverFinancialServices = (
      <Popover id="financial-services" style={{ ...popoverStyle}}>
          <Popover.Content>
              <p style={{ ...Font13Style}}>Extend their policies &/or services. Repurpose our AI for their services</p>
              <p style={{ ...Font13Style}}>- By adding security marketplace to Partner’s existing offerings</p>
              <p style={{ ...Font13Style}}>- Partner’s customer portal can contain a ProtectBox App that deploys a lite version of ProtectBox’s user interface, to keep data localised</p>
              <p style={{ ...Font13Style}}>- APIs to ProtectBox’s backend process the data</p>
              <p style={{ ...Font13Style}}>- Re-purpose our ethical, award-winning AI-powered e-commerce for Partner’s existing professional services</p>
          </Popover.Content>
      </Popover>
    );

    const popoverProfessionalServices = (
      <Popover id="professional-services" style={{ ...popoverStyle}}>
          <Popover.Content>
          <p style={{ ...Font13Style}}>Bundle their existing products / services into our security marketplace. Repurpose our AI for their services</p>
          <p style={{ ...Font13Style}}>- By combining security marketplace into Partner’s existing services</p>
          <p style={{ ...Font13Style}}>- Partner’s customer portal can contain a ProtectBox App that deploys a lite version of ProtectBox’s user interface, to keep data localised</p>
          <p style={{ ...Font13Style}}>- APIs to ProtectBox’s backend process the data</p>
          <p style={{ ...Font13Style}}>- Re-purpose our ethical, award-winning AI-powered e-commerce for Partner’s existing professional services</p>
          </Popover.Content>
      </Popover>
    );

    const popoverGovTech = (
      <Popover id="govtech" style={{ ...popoverStyle}}>
          <Popover.Content>
          <p style={{ ...Font13Style}}>Repurpose our AI to automate Partner’s processes, policy / regulation & enhance existing marketplaces</p>
          <p style={{ ...Font13Style}}>- Add our ethical AI’s capabilities to existing marketplaces of hospitals, schools, energy, defence, OEMs, physical security vendors and all manner of infrastructure assets, to enhance their functionality</p>
          <p style={{ ...Font13Style}}>- Re-purpose our AI to make analysis / prediction from multiple surveillance systems, easier & smarter. To make assets or vendors’ existing solutions easier to use for their end-users</p>
          <p style={{ ...Font13Style}}>- Re-purpose our Recommendations dashboard to local/regional/national policy (decision-making/ supervisory) dashboards</p>
          </Popover.Content>
      </Popover>
    );

    const popoverCircularEconomy = (
      <Popover id="circular-economy" style={{ ...popoverStyle}}>
          <Popover.Content>
          <p style={{ ...Font13Style}}>Repurpose our AI to automate Partner’s processes, policy / regulation & enhance existing marketplaces</p>
          <p style={{ ...Font13Style}}>- Automate (/ enhance) Partner’s existing capabilities (e.g. distribution of farmed goods, credit financing / insurance for farming, training/staffing) by combining it with our ethical, award-winning AI-powered e-commerce engine</p>
          <p style={{ ...Font13Style}}>- Add our AI’s predictive capabilities to expedite Partner’s international development impacts</p>
          <p style={{ ...Font13Style}}>- Re-purpose our Recommendations dashboard to local/regional/national policy (decision-making/ supervisory) dashboards</p>
          </Popover.Content>
      </Popover>
    );

    const popoverTech4Good = (
      <Popover id="tech4good" style={{ ...popoverStyle}}>
          <Popover.Content>
          <p style={{ ...Font13Style}}>Repurpose our AI to automate Partner’s processes, policy / regulation & enhance existing marketplaces</p>
          <p style={{ ...Font13Style}}>- Automate (/ enhance) existing capabilities (e.g. disease diagnosis, treatment, training/staffing) by combining it with our ethical, award-winning AI-powered e-commerce engine</p>
          <p style={{ ...Font13Style}}>- Add our AI’s predictive capabilities for supply chain management</p>
          <p style={{ ...Font13Style}}>- Re-purpose our Recommendations dashboard to local/regional/national policy (decision-making/ supervisory) dashboards</p>
          </Popover.Content>
      </Popover>
    );

    return (
        <article className="pb-section pb-cases">
            <div className="black-overlay">
                <div className="container">
                    <h1 className="pbh-3 white-color">Use Cases</h1>
                    <p>We're building trust and confidence with our responsible AI across the following use cases and industries</p>		
                    <div className="row">
                        <div className="col-12">
                        <Slider {...useCaseSliderSetting}>
                            <div className="item">
                                <div className="award-box">
                                    {/*<img src="/assets/image/icon/loan.png" alt="" />*/}
                                    <p style={{...usecaseTitleStyle}}>Sage</p>
                                    <div className="cases-award-text" style= {{ ...Font13Style }}>Extended their product offering to include a security marketplace by showing link to our website. Integrated with their API to auto-pull customer data</div>
                                    <OverlayTrigger trigger={["hover", "hover"]}  placement="bottom" overlay={ popoverSage } >
                                        <Link className="btn btn-secondary btn-find-out-how" data-name="sage" to="#">Find out how</Link>
                                    </OverlayTrigger>
                                </div>
                            </div>
                            <div className="item">
                                <div className="award-box">
                                    <img src="/assets/image/icon/loan.png" alt="" />
                                    <p style={{...usecaseTitleStyle}}>Oracle</p>
                                    <div className="cases-award-text" style= {{ ...Font13Style }}>Extended their product offering to include a cybersecurity comparison marketplace App while keeping data localised</div>
                                    <OverlayTrigger trigger={["hover", "hover"]}  placement="bottom" overlay={ popoverOracle } >
                                        <Link className="btn btn-secondary btn-find-out-how" data-name="oracle" to="#">Find out how</Link>
                                    </OverlayTrigger>
                                </div>
                            </div>
                            <div className="item">
                                <div className="award-box">
                                    <img src="</assets/image/icon/loan.png" alt="" />
                                    <p style={{...usecaseTitleStyle}}>Wellbeing App</p>
                                    <div className="cases-award-text" style= {{ ...Font13Style }}>Award-winning algorithm repurposed for non-cybersecurity applications</div>
                                    <OverlayTrigger trigger={["hover", "hover"]}  placement="bottom" overlay={ popoverWellbeingApp } >
                                        <Link className="btn btn-secondary btn-find-out-how" data-name="wellbing" to="#">Find out how</Link>
                                    </OverlayTrigger>
                                </div>
                            </div>
                            <div className="item">
                                <div className="award-box">
                                    <img src="/assets/image/icon/loan.png" alt="" />
                                    <p style={{...usecaseTitleStyle}}>GenieShares</p>
                                    <div className="cases-award-text" style= {{ ...Font13Style }}>Co-promotion of ProtectBox’s values, as part of Corporate Social Responsibility (CSR) of backers such as Salesforce & Google</div>
                                    <OverlayTrigger trigger={["hover", "hover"]}  placement="bottom" overlay={ popoverGenieShares } >
                                        <Link className="btn btn-secondary  btn-find-out-how" data-name="gen" to="#">Find out how</Link>
                                    </OverlayTrigger>
                                </div>
                            </div>
                            <div className="item">
                                <div className="award-box" style={{}}>
                                    <img src="/assets/image/icon/loan.png" alt="" />
                                    <p style={{...usecaseTitleStyle}}>Financial Services / Insurer / Bank / Investor</p>
                                    <div className="cases-award-text" style= {{ ...Font13Style }}>Extended their product offering to include a security marketplace by showing link to our website. Integrated with their API to auto-pull customer data</div>
                                    <OverlayTrigger trigger={["hover", "hover"]}  placement="bottom" overlay={ popoverFinancialServices } >
                                        <Link className="btn btn-secondary  btn-find-out-how" data-name="financial-services" to="#">Find out how</Link>
                                    </OverlayTrigger>
                                </div>
                            </div>
                            <div className="item">
                                <div className="award-box">
                                    <img src="/assets/image/icon/loan.png" alt="" />
                                    <p style={{...usecaseTitleStyle}}>Professional Services / Telecom / Lawyer / Accountant</p>
                                    <div className="cases-award-text" style= {{ ...Font13Style }}>Bundle their existing products / services into our security marketplace. Repurpose our AI for their services</div>
                                    <OverlayTrigger trigger={["hover", "hover"]}  placement="bottom" overlay={ popoverProfessionalServices } >
                                        <Link className="btn btn-secondary  btn-find-out-how" data-name="professional-services" to="#">Find out how</Link>
                                    </OverlayTrigger>
                                </div>
                            </div>
                            <div className="item">
                                <div className="award-box">
                                    <img src="/assets/image/icon/loan.png" alt="" />
                                    <p style={{...usecaseTitleStyle}}>GovTech / Smart Cities / Industrial IoT / Physical Security / OEMs</p>
                                    <div className="cases-award-text" style= {{ ...Font13Style }}>Repurpose our AI to automate Partner’s processes, policy / regulation & enhance existing marketplaces</div>
                                    <OverlayTrigger trigger={["hover", "hover"]}  placement="bottom" overlay={ popoverGovTech } >
                                        <Link className="btn btn-secondary  btn-find-out-how" data-name="govtech" to="#">Find out how</Link>
                                    </OverlayTrigger>
                                </div>
                            </div>
                            <div className="item">
                                <div className="award-box">
                                    <img src="/assets/image/icon/loan.png" alt="" />
                                    <p style={{...usecaseTitleStyle}}>Tech4Good / Health</p>
                                    <div className="cases-award-text" style= {{ ...Font13Style }}>Repurpose our AI to automate Partner’s processes, policy / regulation & enhance existing marketplaces</div>
                                    <OverlayTrigger trigger={["hover", "hover"]}  placement="bottom" overlay={ popoverTech4Good } >
                                        <Link className="btn btn-secondary  btn-find-out-how" data-name="tech4good" to="#">Find out how</Link>
                                    </OverlayTrigger>
                                </div>
                            </div>
                            <div className="item">
                                <div className="award-box">
                                    <img src="/assets/image/icon/loan.png" alt="" />
                                    <p style={{...usecaseTitleStyle}}>Circular Economy / Sustainability / AgriTech</p>
                                    <div className="cases-award-text" style= {{ ...Font13Style }}>Repurpose our AI to automate Partner’s processes, policy / regulation & enhance existing marketplaces</div>
                                    <OverlayTrigger trigger={["hover", "hover"]}  placement="bottom" overlay={ popoverCircularEconomy } >
                                        <Link className="btn btn-secondary  btn-find-out-how" data-name="circular-economy" to="#">Find out how</Link>
                                    </OverlayTrigger>
                                </div>
                            </div>
                        </Slider>
                        </div>
                    </div>	
                </div>
            </div>
        </article>
    );
};

export default UseCases;
