import React from "react";

import PwiredImg from "./../../src/images2/P_wired.png";
import TheTimes2Img from "./../../src/images2/the_times-2.png";
import Telegraph1Img from "./../../src/images2/Telegraph-1.png";

const LastInsidesAndNews = () => {

    return (
        <div className="pb-section latest-news">
            <div className="container">
                <h1 className="pbh-3 mt-3">Latest insights and news</h1>
                <div>
                    <div className="container">
                        <div className="row">
                            <div className="col-md-12" style={{"textAlign":"center", "paddingTop":"10px", "paddingBottom":"20px", "margin":"auto"}} className="lanch_bar">
                                <p style={{ "fontWeight":"normal"}}>"Winner of Access Stage pitch" <img src={ PwiredImg }  style={{ "width":"100px" }}/>   </p>
                                <p style={{ "fontWeight":"normal"}}><img src={ TheTimes2Img }  style={{ "width":"155px" }}/> "far removed from your typical 'geek' "</p>
                                <p style={{ "fontWeight":"normal"}}> "business leaders can take action themselves...says   Mary Portas"  <img src={ Telegraph1Img }  style={{ "width":"145px" }}/></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-12 col-md-3 px-3">
                        <div className="info-box">
                            <a href="https://www.gov.uk/government/news/smart-city-firms-announced-for-export-academy-to-asia-pacific" target="_blank">
                                <div className="news-img"><img src="https://protectbox.com/images/media/gov_uk.png" /></div>
                                <div className="text">
                                    <p className="latest-blog-title">ProtectBox in Smart-City firms for UK govt Export Academy to Asia-Pac - LondonTechWeek launch</p>
                                </div>
                            </a>
                        </div>
                    </div>

                    <div className="col-12 col-md-3 px-3">
                        <div className="info-box">
                            <a href="https://www.eurafricanforum.org/kiran-bhagotra/" target="_blank">
                                <div className="news-img"><img src="https://protectbox.com/images/media/WhatsApp Image 2020-09-13 at 1.27.16 PM.jpeg" /></div>
                                <div className="text">
                                    <p className="latest-blog-title">Speaker at EurAfrican Forum Digital 2020</p>
                                </div>
                            </a>
                        </div>
                    </div>

                    <div className="col-12 col-md-3 px-3">
                        <div className="info-box">
                            <a href="https://protectbox.com/blog/fintech-data-protection" target="_blank">
                                <div className="news-img"><img src="https://protectbox.com/assets/uploads/90060fea4513352d4e4fe05261149c0b.png"  /></div>
                                <div className="text">
                                    <p className="latest-blog-title">Fintech data protection - Quantum-proofing blockchain</p>
                                </div>
                            </a>
                        </div>
                    </div>

                    <div className="col-12 col-md-3 px-3">
                        <div className="info-box">
                            <a href="https://corp-today.com/corp2020-vote-now/" target="_blank">
                                <div className="news-img"><img src="https://protectbox.com/images/media/WhatsApp Image 2020-09-09 at 7.09.19 PM.jpeg"  /></div>
                                <div className="text">
                                    <p className="latest-blog-title">Best Cyber Security &amp; Data Protection Comparison Platform 2020</p>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div className="col-12 col-md-12">
                        <div className="pb-btn-box">
                            <a className="btn" style={{ "background": "#e39836" }} href="/Blog">View All</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default LastInsidesAndNews;
