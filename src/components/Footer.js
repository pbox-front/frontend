import React, { Fragment } from "react";
import { AiFillWechat } from "react-icons/ai";
import { FiPhone } from "react-icons/fi";
import { BiEnvelope } from "react-icons/bi";
import { FaLinkedinIn } from "react-icons/fa";
import { FaTwitter } from "react-icons/fa";
import { FaFacebookF } from "react-icons/fa";
import { Link } from "react-router-dom";

const Footer = () => {

  var FooterMainStyle = {
    width: "25px",
    height: "25px"
  };

  return (
    <Fragment>
      <footer className="footer-main">
        <div className="footer-top">
          <div className="container">
            <div className="row">
              <div className="col-12 col-md-6 pr-1">
                <h3 className="foot-heading">Contact Us</h3>
                <p className="info-bar chat align">
                  <label className="footer-label">
                    <AiFillWechat style={{ ...FooterMainStyle }}/>
                  </label>{" "}
                  <label className="pl-2">Chat to us using the blue chat icon in the bottom right
                  corner.</label>
                </p>
                <p className="info-bar chat align">
                  <label className="footer-label">
                    {" "}
                    <FiPhone  style={{ ...FooterMainStyle }}/>
                  </label>{" "}
                  <label className="pl-2">+44 (0)207 993 3037. Our customer service team is available
                  Mon-Fri 9am-5pm (UK time).</label>
                </p>
                <p className="info-bar chat align">
                  <label className="footer-label">
                    <BiEnvelope  style={{ ...FooterMainStyle }}/>
                  </label>{" "}
                  <label className="pl-2">Support and businesses: team@protectbox.com<br />
                  Suppliers and partners: supplier@protectbox.com<br />
                  Investors and media: kiran@protectbox.com</label>
                </p>
              </div>
              <div className="col-12 col-md-6">
                <div className="social-contact">
                  <h3 className="foot-heading">Follow Us</h3>
                  <ul className="social">
                    <li className="background">
                      <a
                        href="https://www.linkedin.com/in/juliapaynestartupmarketing/"
                        target="_blank"
                      >
                        <span className="footer-icon">
                          <FaLinkedinIn  style={{ "width": "20px", "height": "30px", "color": "#000" }}/>
                        </span>
                      </a>
                    </li>
                    <li>
                      <a
                        href="https://twitter.com/ProtectBoxLtd/"
                        target="_blank"
                      >
                        <span className="footer-icon">
                          {" "}
                          <FaTwitter  style={{ "width": "20px", "height": "30px", "color": "#000" }}/>
                        </span>
                      </a>
                    </li>
                    <li>
                      <a
                        href="https://www.facebook.com/protectbox/"
                        target="_blank"
                      >
                        {" "}
                        <span className="footer-icon">
                          <FaFacebookF style={{ "width": "13px", "height": "30px", "color": "#000" }}/>
                        </span>
                      </a>
                    </li>
                  </ul>
                </div>
                <div className="playstores">
                  <a
                    href="https://play.google.com/store/apps/details?id=com.security.protectbox"
                    target="_blank" className="pr-2"
                  >
                    <img src="/image/google-play.png" />
                  </a>
                  <a
                    href="https://apps.apple.com/in/app/protectbox/id1514550015"
                    target="_blank"
                  >
                    <img src="/image/apple-store.png" />
                  </a>
                </div>
                <div className="payment-method">
                  <h3 className="foot-heading">Your Data is Safe</h3>
                  <img src="/image/ssl-certificate.png" />
                  <img src="/image/pci-dss.png" />
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="footer-bottom">
          <div className="container">
            <div className="row">
              <div className="col-12 col-sm-4 col-md-6">
                <p>© Copyright ProtectBox <br/>Company number: NI643316&nbsp;&nbsp;VAT Registration number 297 5082 62</p>
              </div>
              <div className="col-12 col-sm-8 col-md-6 social-media right">
                <ul className="footer-menu">
                  <li>
                    <Link
                      to="/SupplierPolicy"
                      target="_blank"
                    >
                      Supplier policy
                    </Link>
                  </li>
                  <li>
                    <Link
                      to="/Privacypolicy"
                      target="_blank"
                    >
                      Privacy policy
                    </Link>
                  </li>
                  <li>
                    <Link to="/Termsandconditions" target="_blank">
                      Terms and Conditions
                    </Link>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </footer>
      {/* <div id="back-top">
        <a title="Go to Top" href="#">
          <i class="fa fa-level-up"></i>
        </a>
      </div> */}
    </Fragment>
  );
};

export default Footer;
