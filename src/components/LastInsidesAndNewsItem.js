import React from "react";

const BlogItem = () => {

    return (
        <div class="pb-col-md-2 blog-item">
            <div class="blog-box">
                <p class="blog-item-date text-left" style={{ "color": "#e39836" }}>Sep 8th, 2020</p>
                <p class="blog-item-img">
                    <img alt="awards" src={ "https://protectbox.com/images/media/gov_uk.png" } />
                </p>
                <p class="blog-item-title">
                    <a href="#" target="_blank" style={{ "fontWeight":"bold", "color": "#e39836"  }}>
                    ProtectBox in Smart-City firms for UK govt Export Academy to Asia-Pac - LondonTechWeek launch
                    </a>
                </p>
            </div>
        </div>
    );
};

export default BlogItem;
