import React from "react";
import { FaAngleDown } from "react-icons/fa";
import { GiHamburgerMenu } from "react-icons/gi";
import { Link } from "react-router-dom";
import '../styles/header.css'
import LanguageBar from "./LanguageBar";
import logo from '../images2/logo.png';
import closeIcon from '../images2/close.png';


class Header2 extends React.Component {

    constructor() {
        super();
        this.state = {};
        this.toggleOnMobileView = this.toggleOnMobileView.bind(this);
    }

    toggleOnMobileView() {
        this.setState({ showMobileViewMenu: !this.state.showMobileViewMenu });
    }


    render() {
        const { showMobileViewMenu } = this.state;
        return (
            <header>
                <div className="container">
                    <div className="row" style={{ marginTop: 10, marginBottom: 10 }}>
                        <div className="col-md-3">
                            <div id="logo">
                                <Link to="/Home">
                                    <img src={logo} height="50" />
                                </Link>
                            </div>
                        </div>
                        <nav className="col-md-9">
                            <a className="cmn-toggle-switch" onClick={this.toggleOnMobileView} >
                                <GiHamburgerMenu />
                            </a>
                            <div className={`login-after-main-menu ${showMobileViewMenu ? 'show' : ''}`}>

                                <div id="header_menu">
                                    <img src={logo} style={{ width: "50%" }} alt="Logo-Mobile" data-retina="true" />
                                </div>

                                <a className="open_close" id="close_in" onClick={this.toggleOnMobileView}>
                                    <img src={closeIcon} style={{ height: 50 }} alt="Close" />
                                </a>

                                <ul style={{ textAlign: "right", zIndex: 99 }}>
                                    <li><Link to="/Home">Home</Link></li>
                                    <li><Link to="/About">About</Link></li>
                                    <li><Link to="/ContactUs">Contact Us</Link></li>
                                    <li><Link to="/FAQs">FAQs</Link></li>

                                    <li className="hidden-sm hidden-md hidden-lg"><Link to="/sales">Sales</Link></li>
                                    <li className="hidden-sm hidden-md hidden-lg"><Link to="/edit_solution">Solutions</Link></li>
                                    <li className="hidden-sm hidden-md hidden-lg"><Link to="/payments">Payments</Link></li>
                                    <li className="hidden-sm hidden-md hidden-lg">
                                        <a href="javascript:void(0);" className="dropdown-toggle" data-toggle="dropdown" title="Delegates"> Delegates</a>
                                        <ul className="dropdown-menu" style={{ marginLeft: 50 }}>
                                            <li><Link to="/be_delegate">Be a Delegate</Link></li>
                                        </ul>
                                    </li>
                                    <li className="hidden-sm hidden-md hidden-lg"><Link to="/profile">Settings</Link></li>


                                    <li className="submenu hidden-xs">
                                        <span className="btn btn-warning" style={{ height: 38, padding: 8, paddingRight: 0, marginRight: 5, borderRadius: 4 }}>
                                            <a href="javascript:void(0);" className="show-submenu" style={{ color: "#fff", marginTop: -12 }}>Account
                                               <FaAngleDown />
                                            </a></span>
                                        <ul>
                                            <li><Link to="/sales" style={{ color: "#73859B" }}>Sales</Link></li>
                                            <li><Link to="/edit_solution" style={{ color: "#73859B" }}>Solutions</Link></li>
                                            <li><Link to="/payments" style={{ color: "#73859B" }}>Payments</Link></li>
                                            <li>
                                                <a href="javascript:void(0);" className="dropdown-toggle" data-toggle="dropdown"> Delegates</a>
                                                <ul className="dropdown-menu" style={{ marginLeft: 50 }}>
                                                    <li><Link to="/be_delegate" style={{ fontSize: 15 }}>Be a Delegate</Link></li>
                                                </ul>
                                            </li>
                                            <li><Link to="/profile" style={{ color: "#73859B" }}>Settings</Link></li>
                                        </ul>
                                    </li>
                                    <li><Link to="/logout" className="btn btn-success" style={{ color: "#fff", fontSize: 15, padding: "7px 10px", height: "auto", borderRadius: 4, marginRight: 5 }}>Logout</Link></li>
                                    <li><LanguageBar /></li>
                                </ul>

                                <div className="input-group">
                                    <input type="text" name="keyword" id="keyword" className="form-control" placeholder="Search: click here for keywords, or type (/click button on right)" style={{ fontSize: 12 }} aria-describedby="basic-addon2" />
                                    <span className="input-group-addon sbtns" id="basic-addon2" style={{ cursor: "pointer" }}>Search</span>
                                </div>
                            </div>


                        </nav>

                    </div>
                </div>
            </header >
        );
    }
}

export default Header2;
