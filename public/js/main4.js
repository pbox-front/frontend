(function($) {

    "use strict";



    if ($.fn.magnificPopup) {

        $('#videobtn').magnificPopup({

            type: 'iframe'

        });

        $('.gallery_img').magnificPopup({

            type: 'image',

            gallery: {

                enabled: true

            },

            removalDelay: 300,

            mainClass: 'mfp-fade',

            preloader: true

        });

    }



    if ($.fn.magnificPopup) {

        $('#videobtn2').magnificPopup({

            type: 'iframe'

        });

    }



})(jQuery);





/* 2. sticky And Scroll UP */

$(window).on('scroll', function() {

    var scroll = $(window).scrollTop();

    if (scroll < 100) {

        $(".main-header").removeClass("stickyheader");

        $('#back-top').fadeOut(500);

    } else {

        $(".main-header").addClass("stickyheader");

        $('#back-top').fadeIn(500);

    }

});

// Scroll Up

$('#back-top a').on("click", function() {

    $('body,html').animate({

        scrollTop: 0

    }, 800);

    return false;

});



// *****BX SLIDER *** 



$(document).ready(function() {

    $('.slider').bxSlider();

});





// *****TESTIMONIAL OWL CAROUSEL *** 



var owl = $('.home-testimonial');

owl.owlCarousel({

    loop: true,

    nav: true,

    dots: false,

    margin: 10,

    autoplay: true,

    autoplayTimeout: 5000,

    autoplayHoverPause: true,

    responsive: {

        0: {

            items: 1,

        },

        600: {

            items: 1,

        },

        1000: {

            items: 1,

        }

    }

});





// *****CASES OWL CAROUSEL *** 



var owl = $('.home-cases');

owl.owlCarousel({

    loop: false,

    margin: 10,

    autoplayTimeout: 5000,

    autoplayHoverPause: true,

    responsive: {

        0: {

            items: 1,

        },

        600: {

            items: 2,

        },

        1000: {

            items: 3,

        }

    }



});





// *****CATEGORY OWL CAROUSEL *** 



var owl = $('.home-category');

owl.owlCarousel({

    loop: true,

    nav: true,

    dots: false,

    margin: 10,

    autoplay: true,

    autoplayTimeout: 5000,

    autoplayHoverPause: true,

    responsive: {

        0: {

            items: 1,

        },

        600: {

            items: 3,

        },

        1000: {

            items: 5,

        }

    }



});



// *****PARTNER OWL CAROUSEL *** 


var owl = $('.home-partner');

owl.owlCarousel({

    loop: true,

    dots: false,

    margin: 10,

    autoplay: true,

    autoplayTimeout: 5000,

    autoplayHoverPause: true,

    responsive: {

        0: {

            items: 2,

        },

        600: {

            items: 3,

        },

        1000: {

            items: 6,

        }

    }



});



// *****EXISTING NEWS OWL CAROUSEL *** 



var owl = $('.excisting-news');

owl.owlCarousel({

    loop: true,

    margin: 10,

    autoplay: true,

    autoplayTimeout: 5000,

    autoplayHoverPause: true,

    responsive: {

        0: {

            items: 1,

        },

        600: {

            items: 1,

        },

        1000: {

            items: 1,

        }

    }



});



// *****LATEST NEWS OWL CAROUSEL *** 



var owl = $('.latest-innews');

owl.owlCarousel({

    loop: true,

    margin: 10,

    autoplay: true,

    autoplayTimeout: 5000,

    autoplayHoverPause: true,

    responsive: {

        0: {

            items: 1,

        },

        600: {

            items: 2,

        },

        1000: {

            items: 3,

        }

    }



});



// *****AWARD WINNING OWL CAROUSEL *** 


/*
var owl = $('.award-winning-tech');

owl.owlCarousel({

    loop: true,

    margin: 10,

    autoplay: true,

    autoplayTimeout: 5000,

    autoplayHoverPause: true,

    responsive: {

        0: {

            items: 2,

        },

        600: {

            items: 3,

        },

        1000: {

            items: 6,

        }

    }



});

*/



$(document).ready(function() {

    $('.btn.language-btn').click(function() {

        $('.drop-menu').toggle();

    });

});



$(document).ready(function() {

    $(".main-menu ul li").click(function() {

        $(this).addClass("active").siblings().removeClass("active");

    });

});



$(document).ready(function() {

    $(".main-menu li .btn.search-btn").click(function() {

        $(".search-field").slideToggle(300);

    });

});







$(document).ready(function() {

    $(".mobile-menu").click(function() {

        $(".main-menu").slideToggle();

    });

});


$(document).ready(function() {
    $("#blog-opt").change(function() {
        var opt = $(this).val();
        console.log(">>>>>" + opt);
        if (opt == 'Blog') {
            $('.post-blog').show();
            $('.post-news').hide();
            $('.post-awards').hide();
        } else if (opt == 'News') {
            $('.post-blog').hide();
            $('.post-news').show();
            $('.post-awards').hide();
        } else if (opt == 'Awards') {
            $('.post-blog').hide();
            $('.post-news').hide();
            $('.post-awards').show();
        } else if (opt == 'All') {
            $('.post-blog').show();
            $('.post-news').show();
            $('.post-awards').show();
        }
    });
})